import './App.css';
import React, { useState, useEffect } from 'react';
import { Routes, Route, Navigate, useLocation } from 'react-router-dom';
import Layout from './components/Main/Layout';
import Login from './components/Main/Login';
import ForgotPassword from './components/Main/ForgotPassword';
import Header from './components/Header/Header';
import SideBar from './components/sidebar/SideBar';
import Footer from './components/Footer/Footer';
import Sales from './components/Sales/Sales';
import SalesReturn from './components/Sales/SalesReturn';
import WaterBottleManufacture from './components/Water Bottles/WaterBottleManufacture';
import Purchase from './components/Purchase/Purchase';
import PurchaseReturn from './components/Purchase/PurchaseReturn';
import BottleManufacturing from './components/Bottle Manufacture/BottleManufacturing';
import EmploymentManagement from './components/Employment/EmploymentManagement';
import Inventory from './components/Inventory/Inventory';
import Expenditure from './components/Expenditure/Expenditure';
import Leave from './components/Leave/Leave';
import Category from './components/Masters/Category';
import Product from './components/Masters/Product';
import Supplier from './components/Masters/Supplier';
import Rawmaterial from './components/Masters/Rawmaterial';
import Discount from './components/Masters/Discount';
import ExpenditureType from "./components/Masters/ExpenditureType";
import Customers from './components/Customers/Customers';
import Payments from './components/Payments/Payments';
import Dashboard from './components/Dashboard/Dashboard';
import Notification from './components/Notification/Notification';
import BulkNotification from "./components/Notification/BulkNotification";
import Calendar from './components/Calendar/Calendar';
import TimeTable from './components/TimeTable/TimeTable';
import Users from './components/Users/Users';
import Report from './components/Report/Report';
import Contact from './components/Contact/Contact';
import Settings from './components/Settings/Settings';
import Rawmaterialstock from './components/Inventory/RawMaterialStock';
import WaterBottleStock from "./components/Inventory/WaterBottleStock"

function RequireAuth({ children }) {
  const user = JSON.parse(sessionStorage.getItem('user'));
  let location = useLocation();
  if (!user) {
    return <Navigate to="/login" state={{ from: location }} replace />;
  } else if (user) {
  }
  return children;
}

function App() {
  const [authenticated, setAuthenticated] = useState(null);

  useEffect(() => {
    const user = JSON.parse(sessionStorage.getItem('user'));
    user ? setAuthenticated(true) : setAuthenticated(false);
  }, []);

  return (
    <div>
      <Routes>
        {!authenticated && <Route path="/login" element={<Login />} />}
        <Route path="/forgotpassword" element={<ForgotPassword />} />

        {/* {authenticated && ( */}
        <Route
          path="/"
          element={
            <RequireAuth>
              <Layout />
            </RequireAuth>
          }
        >
          <Route path="/" element={<Dashboard />} />
          <Route path="/sales" element={<Sales />} />
          <Route path="/salesreturn" element={<SalesReturn />} />
          <Route path="/water" element={<WaterBottleManufacture />} />
          <Route path="/purchase" element={<Purchase />} />
          <Route path="/purchasereturn" element={<PurchaseReturn />} />
          <Route path="/bottle" element={<BottleManufacturing />} />
          <Route path="/employee" element={<EmploymentManagement />} />
          <Route path="/inventory" element={<Inventory />} />
          <Route path="/rawmaterialStock" element={<Rawmaterialstock/>}/>
          <Route path="/waterbottleStock" element={<WaterBottleStock/>}/>
          <Route path="/expenditure" element={<Expenditure />} />
          <Route path="/leave" element={<Leave />} />
          <Route path="/category" element={<Category />} />
          <Route path="/product" element={<Product />} />
          <Route path="/supplier" element={<Supplier />} />
          <Route path="/rawmaterial" element={<Rawmaterial />} />
          <Route path="/discount" element={<Discount />} />
          <Route path='/expendituretype' element={<ExpenditureType/>} />
          <Route path="/customers" element={<Customers />} />
          <Route path="/payment" element={<Payments />} />
         
          <Route path="/notification" element={<Notification />} />
          <Route path="/bulknotification" element={<BulkNotification/>}/>
          <Route path="/calendar" element={<Calendar />} />
          <Route path="/timetable" element={<TimeTable />} />
          <Route path="/users" element={<Users />} />
          <Route path="/report" element={<Report />} />
          <Route path="/contact" element={<Contact />} />
          <Route path="/settings" element={<Settings />} />
        </Route>
        {/* )} */}
        <Route
          path="*"
          element={<Navigate to={authenticated ? '/' : '/login'} />}
        />
      </Routes>
    </div>
  );
}

export default App;

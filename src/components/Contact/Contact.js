import React, { useState, useEffect } from "react";
import {
  NavLink,
  NavItem,
  Nav,
  DropdownButton,
  DropdownItem,
  Dropdown,
  Row,
  Col,
  FormGroup,
  Form,
  FormLabel,
  FormControl,
  InputGroup,
  InputGroupText,
  Button,
} from "react-bootstrap";
import { TabContent, TabPane } from "reactstrap";
import Pagination from "react-bootstrap/Pagination";
import classnames from "classnames";
import Moment from "moment";
import axios from "axios";
import ReactPaginate from "react-paginate";
import DatePicker from "react-datepicker";
import PhoneInput from "react-phone-number-input";

export default function EmploymentManagement() {
  const [activeTab, setActiveTab] = useState(1);

  const [contact, setContact] = useState([]);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [contactInfoId, setContactInfoId] = useState("");
  const [contactRemark, setContactRemark] = useState("");
  const [email, setEmail] = useState("");
  const [altermobileNumber, setAltermobileNumber] = useState("");
  const [landLine, setLandLine] = useState("");
  const [mobileNumber, setMobileNumber] = useState("");

  const [selectedPhoto, setSelectedPhoto] = useState(null);
  const [pageSize, setPageSize] = useState(10);
  const [onEdit, setOnEdit] = useState(false);
  const [pageNumber, setPageNumber] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [value, setValue] = useState(0);
  const [searchTerm, setSearchTerm] = useState("");

  const BarStyling = {
    width: "20rem",
    background: "#F2F1F9",
    border: "#0d6efd",
    padding: "0.5rem",
    marginBottom: 10,
  };
  const user = JSON.parse(sessionStorage.getItem("user"));
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };
  let userId = user.userId;
  const baseUrl = "https://executivetracking.cloudjiffy.net/WaterPlantWeb";

  const postData = () => {
    axios({
      method: "post",
      url: `${baseUrl}/contactInfo/v1/createContactInfo`,
      headers,
      data: {
        altermobileNumber,
        contactInfoId,
        contactRemark,
        createdBy: {
          userId,
        },
        email,
        firstName,
        landLine,
        lastName,
        mobileNumber,
      },
    })
      .then(function (res) {
        if (res.data.responseCode === 201) {
          alert(res.data.message);
          window.location.reload(false);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setFirstName("");
    setLastName("");
    setContactInfoId("");
    setContactRemark("");
    setEmail("");
    setAltermobileNumber("");
    setLandLine("");
    setMobileNumber("");
    setActiveTab(1);
    NewData();
  };

  const NewData = async () => {
    await axios({
      method: "get",
      url: `${baseUrl}/contactInfo/v1/getAllContactInfoByPagination/{pageNumber}/{pageSize}?pageNumber=${pageNumber}&pageSize=${pageSize}`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setPageCount(data.totalPages);
        setContact(data.content);
        console.log("data==", data.content);
      })

      .catch(function (error) {
        console.log(error);
      });
  };

  const handlePageClick = (data) => {
    setPageNumber(data.selected);
    NewData(pageNumber);
  };
  const handleChange = (e) => {
    setPageSize(e.target.value);
    setValue(e.target.value);
    NewData(pageSize);
  };

  useEffect(() => {
    NewData();
  }, [
    altermobileNumber,
    contactInfoId,
    contactRemark,
    email,
    firstName,
    landLine,
    lastName,
    mobileNumber,
    pageSize,
    pageNumber,
  ]);

  const handleRemove = (contactInfoId) => {
    axios({
      method: "delete",
      url: `${baseUrl}/contactInfo/v1/deleteContactInfoById/{contactInfoId}${contactInfoId}`,
      headers,
    })
      .then((res) => {
        if (res.data.responseCode === 200) {
          alert(res.data.message);
          window.location.reload(false);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
        NewData();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const UpdateData = (contactInfoId) => {
    axios({
      method: "get",
      url: `${baseUrl}/contactInfo/v1/getContactInfoByContactInfoId/{contactInfoId}?contactInfoPersonName=${contactInfoId}`,
      headers,
      data: {
        altermobileNumber,
        contactInfoId,
        contactRemark,
        createdBy: {
          userId,
        },
        email,
        firstName,
        landLine,
        lastName,
        mobileNumber,
      },
    })
      .then(function (res) {
        let data = res.data;
        if (data.responseCode === 201) {
          alert(data.message);
          window.location.reload(false);
        } else if (data.responseCode === 400) {
          alert(data.errorMessage);
        }
        setFirstName(res.data.firstName);
        setLastName(res.data.lastName);
        setContactRemark(res.data.contactRemark);
        setEmail(res.data.email);
        setAltermobileNumber(res.data.altermobileNumber);
        setLandLine(res.data.landLine);
        setMobileNumber(res.data.mobileNumber);
        setOnEdit(true);
        setActiveTab(2);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const userData = (e) => {
    axios({
      method: "put",
      url: `${baseUrl}/contactInfo/v1/updateContactInfo`,
      headers,
      data: {
        altermobileNumber,
        contactInfoId,
        contactRemark,
        createdBy: {
          userId,
        },
        email,
        firstName,
        landLine,
        lastName,
        mobileNumber,
        updatedBy: {
          userId,
        },
      },
    })
      .then(function (res) {
        let data = res.data;
        if (data.responseCode === 201) {
          alert(data.message);
          window.location.reload(false);
        } else if (data.responseCode === 400) {
          alert(data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setFirstName("");
    setLastName("");
    setContactRemark("");
    setEmail("");
    setAltermobileNumber("");
    setLandLine("");
    setMobileNumber("");
    setOnEdit(false);
    setActiveTab(1);
    NewData();
  };

  return (
    <>
      <main id="main" class="main">
        <div class="pagetitle">
          <h1>Contact</h1>
          <nav>
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="/">Home</a>
              </li>
              <li class="breadcrumb-item active">Contact</li>
            </ol>
          </nav>
        </div>
        <div>
          <hr />
        </div>

        <div className="container-fluid">
          <div className="d-flex justify-content-between align-items-center ">
            <Nav
              tabs
              className="page-header-tab"
              variant="pills"
              style={{ marginBottom: 30 }}
            >
              <Nav.Item>
                <Nav.Link
                  className={classnames({ active: activeTab === 1 })}
                  onClick={() => setActiveTab(1)}
                >
                  List View
                </Nav.Link>
              </Nav.Item>
              <NavItem>
                {onEdit ? (
                  <NavLink
                    className={classnames({ active: activeTab === 2 })}
                    onClick={() => setActiveTab(2)}
                  >
                    Edit
                  </NavLink>
                ) : (
                  <NavLink
                    className={classnames({ active: activeTab === 2 })}
                    onClick={() => setActiveTab(2)}
                  >
                    Add
                  </NavLink>
                )}
              </NavItem>
            </Nav>
          </div>
        </div>

        <div class="container-fluid">
          <div class="row justify-content-between">
            <div class="col-4">
              <select
                value={value}
                onChange={handleChange}
                style={{ border: "none", color: "bg-primary" }}
              >
                <option value="10">10 / Pages</option>
                <option value="25">25 / Pages</option>
                <option value="50">50 / Pages</option>
                <option value="100">100 / Pages</option>
              </select>
            </div>
            <div class="col-4">
              <div class="input-group">
                <input
                  style={BarStyling}
                  type="text"
                  placeholder="Search"
                  className="prompt"
                  onChange={(e) => setSearchTerm(e.target.value)}
                />
              </div>
            </div>
          </div>
        </div>

        <div className="container-fluid">
          <TabContent activeTab={activeTab}>
            <TabPane tabId={1} className={classnames(["fade show"])}>
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-body ">
                    <table class="table table-hover bg-gradient-info text-black">
                      <thead class="table-section">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">First Name</th>
                          <th scope="col">Last Name</th>
                          <th scope="col">Email</th>
                          <th scope="col">Mobile Number</th>
                          <th scope="col">Alternate Mobile Number</th>
                          <th scope="col">LandLine</th>
                          <th scope="col">Remark</th>
                          <th className="text-center" scope="col">
                            Created By
                          </th>
                          <th className="text-center" scope="col">
                            Updated By
                          </th>
                          <th scope="col">Inserted Date</th>
                          <th scope="col">Updated Date</th>
                          <th scope="col">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        {contact
                          .filter((val) => {
                            if (searchTerm == "") {
                              return val;
                            } else if (
                              val.description
                                .toLowerCase()
                                .includes(searchTerm.toLowerCase())
                            ) {
                              return val;
                            }
                          })
                          .map((data, index) => {
                            return (
                              <tr key={data.contactInfoId}>
                                <th scope="row">{index + 1}</th>
                                <td>{data.firstName}</td>
                                <td>{data.lastName}</td>
                                <td>{data.email}</td>
                                <td>{data.mobileNumber}</td>
                                <td>{data.altermobileNumber}</td>
                                <td>{data.landLine}</td>
                                <td>{data.contactRemark}</td>
                                <td>{data.createdBy.userName}</td>
                                <td>{data.updatedBy.userName}</td>
                                <td>
                                  {Moment(data.insertedDate).format(
                                    "DD-MM-YYYY"
                                  )}
                                </td>
                                <td>
                                  {Moment(data.updatedDate).format(
                                    "DD-MM-YYYY"
                                  )}
                                </td>
                                <td>
                                  <button
                                    type="button"
                                    className="btn btn-icon btn-sm"
                                    title="Edit"
                                    onClick={(e) =>
                                      UpdateData(data.contactInfoId, e)
                                    }
                                  >
                                    <i className="fa fa-edit"></i>
                                  </button>
                                  <button
                                    type="button"
                                    className="btn btn-icon btn-sm js-sweetalert"
                                    title="Delete"
                                    data-type="confirm"
                                    onClick={(e) =>
                                      handleRemove(data.contactInfoId, e)
                                    }
                                  >
                                    <i className="fas fa-trash-alt text-danger"></i>
                                  </button>
                                </td>
                              </tr>
                            );
                          })}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div>
                <ReactPaginate
                  previousLabel="Previous"
                  nextLabel="Next"
                  breakLabel={"..."}
                  pageCount={pageCount}
                  marginPagesDisplayed={2}
                  pageRangeDisplayed={2}
                  onPageChange={handlePageClick}
                  containerClassName={"pagination justify-content-center"}
                  pageClassName={"page-item"}
                  pageLinkClassName={"page-link"}
                  previousClassName={"page-item"}
                  previousLinkClassName={"page-link"}
                  nextClassName={"page-item"}
                  nextLinkClassName={"page-link"}
                  breakClassName={"page-item"}
                  breakLinkClassName={"page-link"}
                  activeClassName={"active"}
                />
              </div>
            </TabPane>
            <TabPane tabId={2} className={classnames(["fade show"])}>
              <div className="col-lg-12 col-md-12">
                <div class="card">
                  <div class="card-body">
                    <div className="card-header table-section">
                      {onEdit ? (
                        <h3 className="card-title">Update Contact</h3>
                      ) : (
                        <h3 className="card-title">Add Contact</h3>
                      )}
                    </div>
                    <Form noValidate>
                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="6"
                          controlId="validationCustom01"
                        >
                          <Form.Label>First Name</Form.Label>
                          <Form.Control
                            required
                            type="text"
                            value={firstName}
                            className="form-control"
                            placeholder="Enter First Name"
                            onChange={(e) => setFirstName(e.target.value)}
                          />
                        </Form.Group>
                        <Form.Group
                          as={Col}
                          md="6"
                          controlId="validationCustom01"
                        >
                          <Form.Label>Last Name</Form.Label>
                          <Form.Control
                            required
                            type="text"
                            value={lastName}
                            className="form-control"
                            placeholder="Enter Last Name"
                            onChange={(e) => setLastName(e.target.value)}
                          />
                        </Form.Group>
                      </Row>
                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="6"
                          controlId="validationCustom02"
                        >
                          <Form.Label>Email</Form.Label>
                          <Form.Control
                            required
                            value={email}
                            className="form-control"
                            placeholder="Enter Email "
                            onChange={(e) => setEmail(e.target.value)}
                          />
                        </Form.Group>

                        <Form.Group
                          as={Col}
                          md="6"
                          controlId="validationCustom02"
                        >
                          <Form.Label>LandLine Number</Form.Label>
                          <Form.Control
                            required
                            value={landLine}
                            className="form-control no-resize"
                            placeholder="Add LandLine Number"
                            onChange={(e) => setLandLine(e.target.value)}
                          />
                        </Form.Group>
                      </Row>

                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="6"
                          controlId="validationCustomUsername"
                        >
                          <Form.Label>Mobile Number</Form.Label>
                          <InputGroup.Text id="inputGroupPrepend">
                            <PhoneInput
                              placeholder="Enter Mobile number"
                              value={mobileNumber}
                              onChange={setMobileNumber}
                            />
                          </InputGroup.Text>
                        </Form.Group>

                        <Form.Group
                          as={Col}
                          md="6"
                          controlId="validationCustomUsername"
                        >
                          <Form.Label>Alternate Mobile Number</Form.Label>
                          <InputGroup.Text id="inputGroupPrepend">
                            <PhoneInput
                              placeholder="Enter Alternate Mobile number"
                              value={altermobileNumber}
                              onChange={setAltermobileNumber}
                            />
                          </InputGroup.Text>
                        </Form.Group>
                      </Row>

                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="12"
                          controlId="validationCustom03"
                        >
                          <Form.Label>Remark</Form.Label>
                          <Form.Control
                            as="textarea"
                            rows={3}
                            value={contactRemark}
                            className="form-control no-resize"
                            placeholder="Add Remark"
                            onChange={(e) => setContactRemark(e.target.value)}
                          />
                        </Form.Group>
                      </Row>
                    </Form>
                    <div className="col-sm-12">
                      {onEdit ? (
                        <button
                          onClick={userData}
                          style={{ marginRight: 20 }}
                          type="submit"
                          className="mr-1 btn btn-primary"
                        >
                          Update
                        </button>
                      ) : (
                        <button
                          onClick={postData}
                          style={{ marginRight: 20 }}
                          type="submit"
                          className="mr-1 btn btn-primary"
                        >
                          Submit
                        </button>
                      )}

                      <button
                        type="submit"
                        className="btn btn-outline-secondary btn-default"
                      >
                        Cancel
                      </button>
                    </div>
                  </div>
                </div>
                <div></div>
              </div>
            </TabPane>
          </TabContent>
        </div>
      </main>
    </>
  );
}

import React, { useEffect, useState } from "react";
import { Link, Outlet, useNavigate } from "react-router-dom";

import invoices from "../../assets/invoices.png";
import axios from "axios";

export default function Header() {
  const [userProfile, setUserProfile] = useState({});
  const [toggleSidebar, setToggleSideBar] = useState(false);
  const [navbarlinksActive, setNavbarlinksActive] = useState(false);

  const select = (el, all = false) => {
    el = el.trim();
    if (all) {
      return [document.querySelectorAll(el)];
    } else {
      return document.querySelector(el);
    }
  };

  const on = (type, el, listener, all = false) => {
    if (all) {
      select(el, all).forEach((e) => e.addEventListener(type, listener));
    } else {
      select(el, all).addEventListener(type, listener);
    }
  };
  const onscroll = (el, listener) => {
    el.addEventListener("scroll", listener);
  };

  const BarStyling = {
    background: "#F2F1F9",
    border: "2px solid #0d6efd",
    borderradius: 12,
    padding: 5,
    marginBottom: 10,
  };
  let date = new Date();
  let year = date.getFullYear();
  let navigate = useNavigate();
  const Logout = () => {
    navigate("/login");
    sessionStorage.removeItem("user");
    window.location.reload(false);
  };
  const user = JSON.parse(sessionStorage.getItem("user"));
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };
  const baseUrl = "https://executivetracking.cloudjiffy.net/WaterPlantWeb";
  const userLogin = () => {
    axios({
      method: "get",
      url: `${baseUrl}/login/login/v1/queryUserProfileByUserName/{userName}?userName=${user.userName}`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setUserProfile(data);
        // console.log(data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  useEffect(() => {
    userLogin();
    if (select(".toggle-sidebar-btn")) {
      on("click", ".toggle-sidebar-btn", function (e) {
        select("body").classList.toggle("toggle-sidebar");
      });
    }
    let navbarlinks = select("#navbar .scrollto", true);
    const navbarlinksActive = () => {
      let position = window.scrollY + 200;
      navbarlinks.forEach((navbarlink) => {
        if (!navbarlink.hash) return;
        let section = select(navbarlink.hash);
        if (!section) return;
        if (
          position >= section.offsetTop &&
          position <= section.offsetTop + section.offsetHeight
        ) {
          navbarlink.classList.add("active");
        } else {
          navbarlink.classList.remove("active");
        }
      });
    };
    window.addEventListener("load", navbarlinksActive);
    onscroll(document, navbarlinksActive);
  }, []);

  return (
    <div>
      <header
        id="header"
        className="header fixed-top d-flex align-items-center"
      >
        <div className="d-flex align-items-center justify-content-between">
          <Link to="/" className="logo d-flex align-items-center">
            <img src={invoices} alt="" />
            <span className="d-none d-lg-block">Invoice4U</span>
          </Link>

          <i
            className="bi bi-list toggle-sidebar-btn"
            onClick={(e) => setToggleSideBar(!toggleSidebar)}
          ></i>
        </div>

        {/* <div className="search-bar">
          <form
            className="search-form d-flex align-items-center"
            method="POST"
            action="#"
          >
            <input
              type="text"
              name="query"
              placeholder="Search"
              title="Enter search keyword"
            />
            <button type="submit" title="Search">
              <i className="bi bi-search"></i>
            </button>
          </form>
        </div> */}

        <nav className="header-nav ms-auto">
          <ul className="d-flex align-items-center">
            <li className="nav-item d-block d-lg-none">
              <a className="nav-link nav-icon search-bar-toggle" href="">
                <i className="bi bi-search"></i>
              </a>
            </li>

            <li className="nav-item dropdown">
              <a
                className="nav-link nav-icon"
                href="/"
                data-bs-toggle="dropdown"
                style={{ textDecoration: "none" }}
              >
                <i className="bi bi-bell"></i>
                <span className="badge bg-primary badge-number">4</span>
              </a>

              {/* <ul className="dropdown-menu dropdown-menu-end dropdown-menu-arrow notifications">
                <li className="dropdown-header">
                  You have 4 new notifications
                  <a href="#">
                    <span className="badge rounded-pill bg-primary p-2 ms-2">
                      View all
                    </span>
                  </a>
                </li>
                <li>
                  <hr className="dropdown-divider" />
                </li>

                <li className="notification-item">
                  <i className="bi bi-exclamation-circle text-warning"></i>
                  <div>
                    <h4>Lorem Ipsum</h4>
                    <p>Quae dolorem earum veritatis oditseno</p>
                    <p>30 min. ago</p>
                  </div>
                </li>

                <li>
                  <hr className="dropdown-divider" />
                </li>

                <li className="notification-item">
                  <i className="bi bi-x-circle text-danger"></i>
                  <div>
                    <h4>Atque rerum nesciunt</h4>
                    <p>Quae dolorem earum veritatis oditseno</p>
                    <p>1 hr. ago</p>
                  </div>
                </li>

                <li>
                  <hr className="dropdown-divider" />
                </li>

                <li className="notification-item">
                  <i className="bi bi-check-circle text-success"></i>
                  <div>
                    <h4>Sit rerum fuga</h4>
                    <p>Quae dolorem earum veritatis oditseno</p>
                    <p>2 hrs. ago</p>
                  </div>
                </li>

                <li>
                  <hr className="dropdown-divider" />
                </li>

                <li className="notification-item">
                  <i className="bi bi-info-circle text-primary"></i>
                  <div>
                    <h4>Dicta reprehenderit</h4>
                    <p>Quae dolorem earum veritatis oditseno</p>
                    <p>4 hrs. ago</p>
                  </div>
                </li>

                <li>
                  <hr className="dropdown-divider" />
                </li>
                <li className="dropdown-footer">
                  <a href="#">Show all notifications</a>
                </li>
              </ul> */}
            </li>

            <li className="nav-item dropdown">
              <a
                className="nav-link nav-icon"
                href="#"
                data-bs-toggle="dropdown"
              >
                <i className="bi bi-chat-left-text"></i>
                <span className="badge bg-success badge-number">3</span>
              </a>

              {/* <ul className="dropdown-menu dropdown-menu-end dropdown-menu-arrow messages">
                <li className="dropdown-header">
                  You have 3 new messages
                  <a href="#">
                    <span className="badge rounded-pill bg-primary p-2 ms-2">
                      View all
                    </span>
                  </a>
                </li>
                <li>
                  <hr className="dropdown-divider" />
                </li>

                <li className="message-item">
                  <a href="#">
                    <img
                      src="assets/img/messages-1.jpg"
                      alt=""
                      className="rounded-circle"
                    />
                    <div>
                      <h4>Maria Hudson</h4>
                      <p>
                        Velit asperiores et ducimus soluta repudiandae labore
                        officia est ut...
                      </p>
                      <p>4 hrs. ago</p>
                    </div>
                  </a>
                </li>
                <li>
                  <hr className="dropdown-divider" />
                </li>

                <li className="message-item">
                  <a href="#">
                    <img
                      src="assets/img/messages-2.jpg"
                      alt=""
                      className="rounded-circle"
                    />
                    <div>
                      <h4>Anna Nelson</h4>
                      <p>
                        Velit asperiores et ducimus soluta repudiandae labore
                        officia est ut...
                      </p>
                      <p>6 hrs. ago</p>
                    </div>
                  </a>
                </li>
                <li>
                  <hr className="dropdown-divider" />
                </li>

                <li className="message-item">
                  <a href="#">
                    <img
                      src="assets/img/messages-3.jpg"
                      alt=""
                      className="rounded-circle"
                    />
                    <div>
                      <h4>David Muldon</h4>
                      <p>
                        Velit asperiores et ducimus soluta repudiandae labore
                        officia est ut...
                      </p>
                      <p>8 hrs. ago</p>
                    </div>
                  </a>
                </li>
                <li>
                  <hr className="dropdown-divider" />
                </li>

                <li className="dropdown-footer">
                  <a href="#">Show all messages</a>
                </li>
              </ul> */}
            </li>

            <li className="nav-item dropdown pe-3">
              <a
                className="nav-link nav-profile d-flex align-items-center pe-0"
                href="#"
                data-bs-toggle="dropdown"
              >
                <span
                  className="d-none d-md-block dropdown-toggle"
                  // style={BarStyling}
                >
                  {userProfile.userName}
                </span>
              </a>

              <ul className="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">
                <li className="dropdown-header">
                  <h6>{userProfile.userName}</h6>
                  <span>Web Designer</span>
                </li>
                <li>
                  <hr className="dropdown-divider" />
                </li>

                <li>
                  <a
                    className="dropdown-item d-flex align-items-center"
                    href="users-profile.html"
                  >
                    <i className="bi bi-person"></i>
                    <span>My Profile</span>
                  </a>
                </li>
                <li>
                  <hr className="dropdown-divider" />
                </li>

                <li>
                  <a
                    className="dropdown-item d-flex align-items-center"
                    href="users-profile.html"
                  >
                    <i className="bi bi-gear"></i>
                    <span>Account Settings</span>
                  </a>
                </li>
                <li>
                  <hr className="dropdown-divider" />
                </li>

                <li>
                  <a
                    className="dropdown-item d-flex align-items-center"
                    href="pages-faq.html"
                  >
                    <i className="bi bi-question-circle"></i>
                    <span>Need Help?</span>
                  </a>
                </li>
                <li>
                  <hr className="dropdown-divider" />
                </li>

                <li>
                  <a
                    className="dropdown-item d-flex align-items-center"
                    onClick={(e) => Logout(e)}
                  >
                    <i className="bi bi-box-arrow-right"></i>
                    <span>Sign Out</span>
                  </a>
                </li>
                <li>
                  <hr className="dropdown-divider" />
                </li>
                {/* <li >
                <a
                  to="/login"
                  onClick={(e) => Logout(e)}
                  className="nav-link collapsed"
                >
                  <i class="bi bi-box-arrow-right"></i>
                  Sign Out
                </a>
              </li> */}
              </ul>
            </li>
          </ul>
        </nav>
      </header>
      <div className={"toggle-sidebar" ? " " : toggleSidebar}>
        <aside id="sidebar" className="sidebar">
          <nav>
            <ul className="sidebar-nav" id="sidebar-nav">
              <li className="nav-item">
                <Link className="nav-link " to="/">
                  Dashboard
                </Link>
              </li>

              <li className="nav-item">
                <Link
                  to="/sales"
                  className="nav-link collapsed"
                  data-bs-target="#sales-nav"
                  data-bs-toggle="collapse"
                >
                  <i className="bi bi-menu-button-wide"></i>
                  Sales
                  <i className="bi bi-chevron-down ms-auto"></i>
                </Link>
                <ul
                  id="sales-nav"
                  className="nav-content collapse "
                  data-bs-parent="#sidebar-nav"
                >
                  <li>
                    <Link to="/sales">
                      <i className="bi bi-circle"></i>
                      <span>Sales</span>
                    </Link>
                  </li>
                  <li>
                    <Link to="/salesreturn">
                      <i className="bi bi-circle"></i>
                      <span>Sales Return</span>
                    </Link>
                  </li>
                </ul>
              </li>

              <li className="nav-item">
                <Link
                  to="/water"
                  className="nav-link collapsed"
                  data-bs-target="#water-nav"
                  data-bs-toggle="collapse"
                >
                  <i class="bi bi-water"></i>
                  <span>Water Bottles</span>
                  <i className="bi bi-chevron-down ms-auto"></i>
                </Link>
                <ul
                  id="water-nav"
                  className="nav-content collapse "
                  data-bs-parent="#sidebar-nav"
                >
                  <li>
                    <Link to="water">
                      <i className="bi bi-circle"></i>
                      <span>Water Bottles Manufacturing</span>
                    </Link>
                  </li>
                </ul>
              </li>

              <li className="nav-item">
                <Link
                  to="/purchase"
                  className="nav-link collapsed"
                  data-bs-target="#purchase-nav"
                  data-bs-toggle="collapse"
                >
                  <i class="bi bi-basket-fill"></i>
                  <span>Purchase</span>
                  <i className="bi bi-chevron-down ms-auto"></i>
                </Link>
                <ul
                  id="purchase-nav"
                  className="nav-content collapse "
                  data-bs-parent="#sidebar-nav"
                >
                  <li>
                    <Link to="purchase">
                      <i className="bi bi-circle"></i>
                      <span>Purchase</span>
                    </Link>
                  </li>
                  <li>
                    <Link to="purchasereturn">
                      <i className="bi bi-circle"></i>
                      <span>Purchase Return</span>
                    </Link>
                  </li>
                </ul>
              </li>

              <li className="nav-item">
                <Link
                  to="/bottle"
                  className="nav-link collapsed"
                  data-bs-target="#bottle-nav"
                  data-bs-toggle="collapse"
                >
                  <i class="bi bi-bar-chart-line"></i>
                  <span>Bottle Manufacture</span>
                  <i className="bi bi-chevron-down ms-auto"></i>
                </Link>
                <ul
                  id="bottle-nav"
                  className="nav-content collapse "
                  data-bs-parent="#sidebar-nav"
                >
                  <li>
                    <Link to="bottle">
                      <i className="bi bi-circle"></i>
                      <span>Bottle Manufacturing</span>
                    </Link>
                  </li>
                </ul>
              </li>

              <li className="nav-item">
                <Link
                  to="/inventory"
                  className="nav-link collapsed"
                  data-bs-target="#inventory-nav"
                  data-bs-toggle="collapse"
                >
                  <i class="bi bi-hdd-stack"></i>
                  <span>Inventory</span>
                  <i className="bi bi-chevron-down ms-auto"></i>
                </Link>
                <ul
                  id="inventory-nav"
                  className="nav-content collapse "
                  data-bs-parent="#sidebar-nav"
                >
                  <li>
                    <Link to="rawmaterialStock">
                      <i className="bi bi-circle"></i>
                      <span>Raw Material Stock</span>
                    </Link>
                  </li>
                  <li>
                    <Link to="waterbottleStock">
                      <i className="bi bi-circle"></i>
                      <span>Water Bottle Stock</span>
                    </Link>
                  </li>
                </ul>
              </li>

              <li className="nav-item">
                <Link
                  to="/expenditure"
                  className="nav-link collapsed"
                  // data-bs-target="#expenditure-nav"
                  // data-bs-toggle="collapse"
                >
                  <i class="bi bi-bank"></i>
                  <span>Expenditure</span>
                  {/* <i className="bi bi-chevron-down ms-auto"></i> */}
                </Link>
                {/* <ul
                id="bottle-nav"
                className="nav-content collapse "
                data-bs-parent="#sidebar-nav"
              > 
                <li>
                  <Link to="bottle">
                    <i className="bi bi-circle"></i>
                    <span>Bottle Manufacturing</span>
                  </Link>
                </li> 
              </ul> */}
              </li>

              <li className="nav-item">
                <Link
                  to="/leave"
                  className="nav-link collapsed"
                  data-bs-target="#leave-nav"
                  data-bs-toggle="collapse"
                >
                  <i class="bi bi-megaphone"></i>
                  <span>Leave</span>
                  {/* <i className="bi bi-chevron-down ms-auto"></i> */}
                </Link>
                {/* <ul
                id="bottle-nav"
                className="nav-content collapse "
                data-bs-parent="#sidebar-nav"
              > 
                <li>
                  <Link to="bottle">
                    <i className="bi bi-circle"></i>
                    <span>Bottle Manufacturing</span>
                  </Link>
                </li>
                
              </ul> */}
              </li>

              <li className="nav-item">
                <Link
                  to="/employee"
                  className="nav-link collapsed"
                  data-bs-target="#employee-nav"
                  data-bs-toggle="collapse"
                >
                  <i class="bi bi-wallet-fill"></i>
                  <span>Employment</span>
                  <i className="bi bi-chevron-down ms-auto"></i>
                </Link>
                <ul
                  id="employee-nav"
                  className="nav-content collapse "
                  data-bs-parent="#sidebar-nav"
                >
                  <li>
                    <Link to="employee">
                      <i className="bi bi-circle"></i>
                      <span>Employment Management</span>
                    </Link>
                  </li>
                </ul>
              </li>
              <li className="nav-item">
                <Link
                  to="/masters"
                  className="nav-link collapsed"
                  data-bs-target="#masters-nav"
                  data-bs-toggle="collapse"
                >
                  <i class="bi bi-table"></i>
                  <span>Masters</span>
                  <i className="bi bi-chevron-down ms-auto"></i>
                </Link>
                <ul
                  id="masters-nav"
                  className="nav-content collapse "
                  data-bs-parent="#sidebar-nav"
                >
                  <li>
                    <Link to="category">
                      <i className="bi bi-circle"></i>
                      <span>Category</span>
                    </Link>
                  </li>
                  <li>
                    <Link to="product">
                      <i className="bi bi-circle"></i>
                      <span>Product</span>
                    </Link>
                  </li>
                  <li>
                    <Link to="supplier">
                      <i className="bi bi-circle"></i>
                      <span>Material Supplier</span>
                    </Link>
                  </li>
                  <li>
                    <Link to="rawmaterial">
                      <i className="bi bi-circle"></i>
                      <span>Raw Material</span>
                    </Link>
                  </li>
                  <li>
                    <Link to="discount">
                      <i className="bi bi-circle"></i>
                      <span>Discount</span>
                    </Link>
                  </li>
                  <li>
                    <Link to="expendituretype">
                      <i className="bi bi-circle"></i>
                      <span>Expenditure Type</span>
                    </Link>
                  </li>
                </ul>
              </li>

              <li className="nav-item">
                <Link
                  to="/customers"
                  className="nav-link collapsed"
                  data-bs-target="#customers-nav"
                  data-bs-toggle="collapse"
                >
                  <i class="bi bi-cash-stack"></i>
                  <span>Customers</span>
                  <i className="bi bi-chevron-down ms-auto"></i>
                </Link>
                <ul
                  id="customers-nav"
                  className="nav-content collapse "
                  data-bs-parent="#sidebar-nav"
                >
                  <li>
                    <Link to="customers">
                      <i className="bi bi-circle"></i>
                      <span>Customers</span>
                    </Link>
                  </li>
                </ul>
              </li>

              <li className="nav-item">
                <Link to="/payment" className="nav-link collapsed">
                  <i class="bi bi-cash-stack"></i>
                  Payments
                  {/* <i className="bi bi-chevron-down ms-auto"></i> */}
                </Link>
                {/* <ul
                id="bottle-nav"
                className="nav-content collapse "
                data-bs-parent="#sidebar-nav"
              > 
                <li>
                  <Link to="bottle">
                    <i className="bi bi-circle"></i>
                    <span>Bottle Manufacturing</span>
                  </Link>
                </li>
                
              </ul> */}
              </li>
              <li className="nav-item">
                <Link
                  to="/notification"
                  className="nav-link collapsed"
                  data-bs-target="#notification-nav"
                  data-bs-toggle="collapse"
                >
                  <i class="bi bi-cash-stack"></i>
                  <span>Notification</span>
                  <i className="bi bi-chevron-down ms-auto"></i>
                </Link>
                <ul
                  id="notification-nav"
                  className="nav-content collapse "
                  data-bs-parent="#sidebar-nav"
                >
                  <li>
                    <Link to="bulknotification">
                      <i className="bi bi-circle"></i>
                      <span>Bulk Notification</span>
                    </Link>
                  </li>
                </ul>
              </li>

              <li className="nav-item">
                <Link to="/users" className="nav-link collapsed">
                  <i class="bi bi-people-fill"></i>
                  <span>Users</span>
                  {/* <i className="bi bi-chevron-down ms-auto"></i> */}
                </Link>
                {/* <ul
                id="bottle-nav"
                className="nav-content collapse "
                data-bs-parent="#sidebar-nav"
              > 
                <li>
                  <Link to="bottle">
                    <i className="bi bi-circle"></i>
                    <span>Bottle Manufacturing</span>
                  </Link>
                </li>
                
              </ul> */}
              </li>

              <li className="nav-item">
                <Link
                  to="/report"
                  className="nav-link collapsed"
                  // data-bs-target="#bottle-nav"
                  // data-bs-toggle="collapse"
                >
                  <i class="bi bi-flag"></i>
                  <span>Report</span>
                  {/* <i className="bi bi-chevron-down ms-auto"></i> */}
                </Link>
                {/* <ul
                id="bottle-nav"
                className="nav-content collapse "
                data-bs-parent="#sidebar-nav"
              > 
                <li>
                  <Link to="bottle">
                    <i className="bi bi-circle"></i>
                    <span>Bottle Manufacturing</span>
                  </Link>
                </li>
                
              </ul> */}
              </li>

              <li className="nav-item">
                <Link
                  to="/contact"
                  className="nav-link collapsed"
                  // data-bs-target="#bottle-nav"
                  // data-bs-toggle="collapse"
                >
                  <i class="bi bi-person-rolodex"></i>
                  <span>Contact</span>
                  {/* <i className="bi bi-chevron-down ms-auto"></i> */}
                </Link>
                {/* <ul
                id="bottle-nav"
                className="nav-content collapse "
                data-bs-parent="#sidebar-nav"
              > 
                <li>
                  <Link to="bottle">
                    <i className="bi bi-circle"></i>
                    <span>Bottle Manufacturing</span>
                  </Link>
                </li>
                
              </ul> */}
              </li>
              <li className="nav-item">
                <Link
                  to="/settings"
                  className="nav-link collapsed"
                  // data-bs-target="#bottle-nav"
                  // data-bs-toggle="collapse"
                >
                  <i class="bi bi-gear-fill"></i>
                  <span>Settings</span>
                  {/* <i className="bi bi-chevron-down ms-auto"></i> */}
                </Link>
                {/* <ul
                id="bottle-nav"
                className="nav-content collapse "
                data-bs-parent="#sidebar-nav"
              > 
                <li>
                  <Link to="bottle">
                    <i className="bi bi-circle"></i>
                    <span>Bottle Manufacturing</span>
                  </Link>
                </li>
                
              </ul> */}
              </li>
            </ul>
          </nav>
        </aside>
      </div>

      <Outlet />

      <footer id="footer" class="footer">
        <div class="copyright">
          &copy; Copyright{" "}
          <strong>
            <span>Invoice4U</span>
          </strong>
          . All Rights Reserved
        </div>
        <div class="credits">
          Designed by{" "}
          <a href="https://www.walkinsoftwares.com/">
            Walkin Softwares Technologies
          </a>
        </div>
      </footer>
    </div>
  );
}

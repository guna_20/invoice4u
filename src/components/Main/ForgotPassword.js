import React, { useState } from 'react';
import axios from 'axios';
import '../../components/Main/forgotPassword.css';
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import invoices from '../../assets/invoices.png';

export default function ForgotPassword() {
  let date = new Date();
  let year = date.getFullYear();
  const [otpSent, setOtpSent] = useState(false);
  const [mobileNumber, setMobileNumber] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [otp, setOtp] = useState('');
  const [userErr, setUserErr] = useState(false);

  const baseUrl =
    'https://executivetracking.cloudjiffy.net/WaterPlantWeb/login/v1';

  const handleOtp = (mobileNumber) => {
    axios({
      method: 'get',
      url: `${baseUrl}/forgotPassword/${mobileNumber}`,
      data: {
        mobileNumber,
      },
    })
      .then((res) => {
        console.log(res);
        const data = res.data;
        if (data.responseCode === 1000) {
          alert(data.message);
        } else if (data.responseCode === 400) {
          alert(data.errorMessage);
        }
        setOtpSent(true);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const navigate = useNavigate();

  const handleForgotPassword = (mobileNumber, newPassword, otp) => {
    axios({
      method: 'post',
      url: `${baseUrl}/resetPassword`,
      data: {
        mobileNumber,
        newPassword,
        otp,
      },
    })
      .then((res) => {
        const data = res.data;
        if (data.responseCode === 200) {
          alert(data.message);
        } else if (data.responseCode === 403) {
          alert(data.message);
        }
        setOtpSent(false);
        navigate('/login');
      })
      .catch((error) => {
        console.log(error);
      });
  };
  
  return (
    <div>
      <section>
        <header
          id="header"
          className="header fixed-top d-flex align-items-center"
          style={{ backgroundColor: '#fff' }}
        >
          <div className="container-fluid container-xl d-flex align-items-center justify-content-between">
            <a href="index.html" className="logo d-flex align-items-center">
              <img src={invoices} alt="" style={{ marginRight: 20 }} />
              <span>Invoice4U</span>
            </a>

            <nav id="navbar" className="navbar">
              <a
                class="getstarted scrollto"
                href="http://www.walkinsoftwares.com/contact.html"
              >
                Contact Us
              </a>
            </nav>
          </div>
        </header>
      </section>
      <div>
        <section id="hero" class="hero d-flex align-items-center">
          <div class="container">
            <div class="row">
              <div class="col-lg-6 d-flex flex-column justify-content-center">
                <h1 data-aos="fade-up">Welcome to Invoice4U</h1>
                <h2 data-aos="fade-up" data-aos-delay="400">
                  Water Plant Management System for tracking, monitoring etc.,{' '}
                </h2>
                <div data-aos="fade-up" data-aos-delay="600">
                  <div class="text-center text-lg-start">
                    <a
                      href="/login"
                      class="btn-get-started scrollto d-inline-flex align-items-center justify-content-center align-self-center"
                    >
                      <span>Get Started</span>
                      <i class="bi bi-arrow-right"></i>
                    </a>
                  </div>
                </div>
              </div>
              <div class="col-lg-6 hero-img">
                <div class="card mb-3" style={{ width: 444 }}>
                  <div class="card-body">
                    <div class="pt-4 pb-2">
                      <h5 class="card-title text-center pb-0 fs-4">
                        Forgot Password
                      </h5>
                      <p class="text-center small">
                        Enter your Phone number to generate OTP, your password
                        will be reset.
                      </p>
                    </div>

                    <div className="form-group">
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Enter Phone Number"
                        maxLength={10}
                        value={mobileNumber}
                        onChange={(e) => setMobileNumber(e.target.value)}
                        required
                      />
                      {userErr ? (
                        <span style={{ color: 'red' }}>
                          Phone Number is required!!
                        </span>
                      ) : null}
                    </div>

                    <div className={otpSent ? 'displayForm' : 'displayNull'}>
                      <div className="form-group">
                        <label className="card-title">New Password</label>
                        <input
                          type="password"
                          className="form-control"
                          placeholder="Enter New Password"
                          minLength={6}
                          value={newPassword}
                          onChange={(e) => setNewPassword(e.target.value)}
                        />
                      </div>
                      <div className="form-group">
                        <label className="card-title">OTP</label>
                        <input
                          type="password"
                          className="form-control"
                          placeholder="Enter OTP"
                          maxLength={6}
                          value={otp}
                          onChange={(e) => setOtp(e.target.value)}
                        />
                      </div>
                    </div>

                    <div className="text-center">
                      {otpSent ? (
                        <button
                          type="submit"
                          className="btn btn-primary btn-block"
                          onClick={(e) =>
                            handleForgotPassword(mobileNumber, newPassword, otp)
                          }
                        >
                          Reset
                        </button>
                      ) : (
                        <button
                          type="submit"
                          className="btn btn-primary btn-block"
                          onClick={(e) => handleOtp(mobileNumber)}
                        >
                          Send OTP
                        </button>
                      )}

                      <div className="text-muted mt-4">
                        Forget it, <Link to="/">Send me Back</Link> to the Sign
                        in screen.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>

      <footer id="footerlogin" class="footerlogin">
        <div>
          <div class="container">
            <div class="copyright">
              &copy; {year} Copyright{' '}
              <strong>
                <span>Invoice4U</span>
              </strong>
              . All Rights Reserved
            </div>
            <div class="credits">
              Designed by{' '}
              <a href="https://walkinsoftwares.com/" target="blank">
                Walkin Software Technologies
              </a>
            </div>
          </div>
        </div>
      </footer>
      {/* </div> */}
    </div>
  );
}

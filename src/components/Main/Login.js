import React, { useState } from "react";
import { Link, useNavigate, useLocation } from "react-router-dom";
import axios from "axios";
import invoices from "../../assets/invoices.png";

export default function Login(props) {
  let date = new Date();
  let year = date.getFullYear();
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");

  const [userErr, setUserErr] = useState(false);
  const [passErr, setPassErr] = useState(false);

  function userHandler(e) {
    let item = e.target.value;
    if (item.length <= 0) {
      setUserErr(true);
    } else {
      setUserErr(false);
    }
    setUserName(item);
    // console.log(e.target.value)
  }
  function passHandler(e) {
    let item = e.target.value;
    if (item.length <= 0) {
      setPassErr(true);
    } else {
      setPassErr(false);
    }
    setPassword(item);
    // console.log(e.target.value)
  }
  const handleSubmit = (e) => {
    let name = userName.trim();
    // console.log(name);
    if (userName.length > 0) {
      setUserErr(false);
      if (password.length > 0) {
        setUserErr(false);
        setPassErr(false);
        handleLogin(e);
      } else {
        setPassErr(true);
      }
    } else {
      setUserErr(true);
    }
  };

  const handleLogin = (e) => {
    axios({
      method: "post",
      url: `https://executivetracking.cloudjiffy.net/WaterPlantWeb/login/v1/userLogin`,
      data: {
        password,
        userName,
      },
      headers: {
        "Content-type": "application/json",
      },
    })
      .then((response) => {
        if (response.data.accessToken) {
          sessionStorage.setItem("user", JSON.stringify(response.data));
          authUser();
        } else {
          alert(response.data.errorMessage);
        }
        return response.data;
      })
      .catch(function (error) {
        console.log(error);
      });
    setUserName("");
    setPassword("");
  };

  const navigate = useNavigate();

  function authUser() {
    const user = JSON.parse(sessionStorage.getItem("user"));
    navigate("/");
    return { Authorization: "Bearer " + user.accessToken };
  }

  return (
    <div>
      <section>
        <header
          id="header"
          className="header fixed-top d-flex align-items-center"
          style={{ backgroundColor: "#fff" }}
        >
          <div className="container-fluid container-xl d-flex align-items-center justify-content-between">
            <a href="index.html" className="logo d-flex align-items-center">
              <img src={invoices} alt="" style={{ marginRight: 20 }} />
              <span>Invoice4U</span>
            </a>

            <nav id="navbar" className="navbar">
              <a
                class="getstarted scrollto"
                href="http://www.walkinsoftwares.com/contact.html"
              >
                Contact Us
              </a>
            </nav>
          </div>
        </header>
      </section>
      <div>
        <section id="hero" class="hero d-flex align-items-center">
          <div class="container">
            <div class="row">
              <div class="col-lg-6 d-flex flex-column justify-content-center">
                <h1 data-aos="fade-up">Welcome to Invoice4U</h1>
                <h2 data-aos="fade-up" data-aos-delay="400">
                  Water Plant Management System for tracking, monitoring etc.,{" "}
                </h2>
                <div data-aos="fade-up" data-aos-delay="600">
                  <div class="text-center text-lg-start">
                    <a
                      href="/login"
                      class="btn-get-started scrollto d-inline-flex align-items-center justify-content-center align-self-center"
                    >
                      <span>Get Started</span>
                      <i class="bi bi-arrow-right"></i>
                    </a>
                  </div>
                </div>
              </div>
              <div class="col-lg-6 hero-img">
                <div class="card mb-3" style={{ width: 444 }}>
                  <div class="card-body">
                    <div class="pt-4 pb-2">
                      <h5 class="card-title text-center pb-0 fs-4">
                        Login to Your Account
                      </h5>
                      <p class="text-center small">
                        Enter your username & password to login
                      </p>
                    </div>

                    <div className="form-group" style={{ marginBottom: 20 }}>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Enter your Username"
                        value={userName}
                        // onChange={(e) => setUserName(e.target.value)}
                        onChange={userHandler}
                        required
                      />
                      {userErr ? (
                        <span style={{ color: "red" }}>
                          User Name is required!!
                        </span>
                      ) : null}
                    </div>

                    <div className="form-group">
                      <input
                        type="password"
                        className="form-control"
                        placeholder="Enter Password"
                        value={password}
                        // onChange={(e) => setPassword(e.target.value)}
                        onChange={passHandler}
                      />
                      {passErr ? (
                        <span style={{ color: "red" }}>
                          Password is required!!
                        </span>
                      ) : null}
                      <label className="form-label">
                        <Link
                          to="/forgotpassword"
                          className="float-right small"
                        >
                          I forgot password
                        </Link>
                      </label>
                    </div>
                    <div className="form-group-checkbox">
                      <label
                        className="custom-control custom-checkbox"
                        style={{ marginRight: 270 }}
                      >
                        <input
                          type="checkbox"
                          className="custom-control-input"
                        />
                        <span className="custom-control-label">
                          Remember me
                        </span>
                      </label>
                    </div>
                    <div className="text-center">
                      <Link
                        to="/"
                        className="btn btn-primary btn-block btn-login"
                        // onClick={(e) => handleLogin(e)}
                        onClick={(e) => handleSubmit(e)}
                      >
                        Login
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>

      <div>
        <footer id="footer" class="footer">
          <div class="copyright">
            &copy; Copyright{" "}
            <strong>
              <span>Invoice4U</span>
            </strong>
            . All Rights Reserved
          </div>
          <div class="credits">
            Designed by{" "}
            <a href="https://www.walkinsoftwares.com/">
              Walkin Softwares Technologies
            </a>
          </div>
        </footer>
      </div>
    </div>
  );
}

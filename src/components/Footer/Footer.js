import React from "react";

export default function Footer() {
  let date = new Date();
  let year = date.getFullYear();
  return (
    <div style={{ marginTop: 50 }}>
      <footer id="footer" class="footer">
        <div class="copyright">
          &copy; {year} Copyright{" "}
          <strong>
            <span>Invoice4U</span>
          </strong>
          . All Rights Reserved
        </div>
        <div class="credits">
          Designed by{" "}
          <a href="https://walkinsoftwares.com/" target="blank">
            Walkin Software Technologies
          </a>
        </div>
      </footer>
    </div>
  );
}

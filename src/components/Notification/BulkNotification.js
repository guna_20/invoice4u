import React, { useState, useEffect } from "react";
import {
  NavLink,
  NavItem,
  Nav,
  DropdownButton,
  DropdownItem,
  Dropdown,
  Form,
  FormGroup,
  FormLabel,
  FormControl,
  FormControlFeedback,
  InputGroup,
  Row,
} from "react-bootstrap";
import { TabContent, TabPane } from "reactstrap";
import Pagination from "react-bootstrap/Pagination";
import classnames from "classnames";
import Moment from "moment";
import axios from "axios";
import ReactPaginate from "react-paginate";

// import SweetAlert from 'sweetalert-react';

export default function BulkNotification() {
  const [activeTab, setActiveTab] = useState(1);

  const [bulkNotification, setBulkNotification] = useState([]);
  const [bulkNotificationId, setBulkNotificationId] = useState("");
  const [message, setMessage] = useState("");
  const [result, setResult] = useState("");
  const [status, setStatus] = useState("");
  const [title, setTitle] = useState("");
  const [topic, setTopic] = useState("");

  const [pageSize, setPageSize] = useState(10);
  const [onEdit, setOnEdit] = useState(false);
  const [pageNumber, setPageNumber] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [value, setValue] = useState(0);
  const [searchTerm, setSearchTerm] = useState("");
  const [validated, setValidated] = useState(false);

  const BarStyling = {
    width: "20rem",
    background: "#F2F1F9",
    border: "#0d6efd",
    padding: "0.5rem",
    marginBottom: 10,
  };
  const user = JSON.parse(sessionStorage.getItem("user"));

  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };

  let userId = user.userId;

  const data = {
    bulkNotificationId,
    createdBy: {
      userId,
    },
    message,
    result,
    status,
    title,
    topic,
  };

  const baseUrl = "https://executivetracking.cloudjiffy.net/WaterPlantWeb";

  const postData = (e) => {
    axios({
      method: "post",
      url: `${baseUrl}/bulknotification/v1/createBulkNotification`,
      headers,
      data,
    })
      .then(function (res) {
        let data = res.data;

        if (data.responseCode === 201) {
          alert(data.message);
          window.location.reload(false);
        } else if (data.responseCode === 400) {
          alert(data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setBulkNotificationId("");
    setMessage("");
    setResult("");
    setStatus("");
    setTitle("");
    setTopic("");
    setActiveTab(1);
    NewData();
  };

  const NewData = async () => {
    await axios({
      method: "get",
      url: `${baseUrl}/bulknotification/v1/getAllBulkNotificationByPagination/{pageNumber}/{pageSize}?pageNumber=${pageNumber}&pageSize=${pageSize}`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setPageCount(data.totalPages);
        setBulkNotification(data.content);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => {
    NewData();
  }, [
    bulkNotificationId,
    message,
    result,
    status,
    title,
    topic,
    pageSize,
    pageNumber,
  ]);

  const handlePageClick = (data) => {
    // console.log(data)
    setPageNumber(data.selected);
    NewData(pageNumber);
  };
  const handleChange = (e) => {
    // console.log(e.target.value);
    setPageSize(e.target.value);
    setValue(e.target.value);
    NewData(pageSize);
  };

  const handleRemove = (bulkNotificationId, e) => {
    axios({
      method: "delete",
      url: `${baseUrl}/bulknotification/v1/deleteBulkNotificationById/${bulkNotificationId}`,
      headers,
    })
      .then(function (res) {
        let data = res.data;
        // console.log(data.responseCode)
        if (data.responseCode === 200) {
          alert(data.message);
          window.location.reload(false);
        } else if (data.responseCode === 400) {
          alert(data.errorMessage);
        }
        NewData();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <>
      <main id="main" class="main">
        <div class="pagetitle">
          <h1>Bulk Notification</h1>
          <nav>
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="/">Home</a>
              </li>

              <li class="breadcrumb-item active">Bulk Notification</li>
            </ol>
          </nav>
        </div>
        <div>
          <hr />
        </div>

        <div className="container-fluid">
          <div className="d-flex justify-content-between align-items-center ">
            <Nav
              tabs
              className="page-header-tab"
              variant="pills"
              style={{ marginBottom: 30 }}
            >
              <Nav.Item>
                <Nav.Link
                  className={classnames({ active: activeTab === 1 })}
                  onClick={() => setActiveTab(1)}
                >
                  List View
                </Nav.Link>
              </Nav.Item>
              <NavItem>
                {onEdit ? (
                  <NavLink
                    className={classnames({ active: activeTab === 2 })}
                    onClick={() => setActiveTab(2)}
                  >
                    Edit
                  </NavLink>
                ) : (
                  <NavLink
                    className={classnames({ active: activeTab === 2 })}
                    onClick={() => setActiveTab(2)}
                  >
                    Add
                  </NavLink>
                )}
              </NavItem>
            </Nav>
          </div>
        </div>

        <div class="container-fluid">
          <div class="row justify-content-between">
            <div class="col-4">
              <select
                value={value}
                onChange={handleChange}
                style={{ border: "none", color: "bg-primary" }}
              >
                <option value="10">10 / Pages</option>
                <option value="25">25 / Pages</option>
                <option value="50">50 / Pages</option>
                <option value="100">100 / Pages</option>
              </select>
            </div>

            <div class="col-4">
              <div class="input-group">
                <input
                  style={BarStyling}
                  type="text"
                  placeholder="Search"
                  className="prompt"
                  // value={searchTerm}
                  onChange={(e) => setSearchTerm(e.target.value)}
                />
              </div>
            </div>
          </div>
        </div>

        <div className="container-fluid">
          <TabContent activeTab={activeTab}>
            <TabPane tabId={1} className={classnames(["fade show"])}>
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-body ">
                    <table class="table table-hover bg-gradient-info text-black">
                      <thead class="table-section">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Title</th>
                          <th scope="col">Topic</th>
                          <th scope="col">Status</th>
                          <th scope="col">Result</th>
                          <th scope="col">Message</th>
                          <th scope="col">Created By</th>
                          <th scope="col">Updated By</th>
                          <th scope="col">Inserted Date</th>
                          <th scope="col">Updated Date</th>
                          {/* <th scope="col">Actions</th> */}
                        </tr>
                      </thead>
                      <tbody>
                        {bulkNotification
                          .filter((val) => {
                            if (searchTerm == "") {
                              return val;
                            } else if (
                              val.message
                                .toLowerCase()
                                .includes(searchTerm.toLowerCase())
                            ) {
                              return val;
                            }
                          })
                          .map((data, index) => {
                            // {console.log((data,index) => {
                            return (
                              <tr key={data.bulkNotificationId}>
                                <th scope="row">{index + 1}</th>
                                <td>{data.message}</td>
                                <td>{data.result}</td>
                                <td>{data.status}</td>
                                <td>{data.title}</td>
                                <td>{data.topic}</td>
                                <td>{data.createdBy.userName}</td>
                                <td>{data.updatedBy.userName}</td>
                                <td>
                                  {Moment(data.insertedDate).format(
                                    "DD-MM-YYYY"
                                  )}
                                </td>
                                <td>
                                  {Moment(data.updatedDate).format(
                                    "DD-MM-YYYY"
                                  )}
                                </td>
                              </tr>
                            );
                          })}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div>
                <ReactPaginate
                  previousLabel="Previous"
                  nextLabel="Next"
                  breakLabel={"..."}
                  pageCount={pageCount}
                  marginPagesDisplayed={2}
                  pageRangeDisplayed={2}
                  onPageChange={handlePageClick}
                  containerClassName={"pagination justify-content-center"}
                  pageClassName={"page-item"}
                  pageLinkClassName={"page-link"}
                  previousClassName={"page-item"}
                  previousLinkClassName={"page-link"}
                  nextClassName={"page-item"}
                  nextLinkClassName={"page-link"}
                  breakClassName={"page-item"}
                  breakLinkClassName={"page-link"}
                  activeClassName={"active"}
                />
              </div>
            </TabPane>
            <TabPane tabId={2} className={classnames(["fade show"])}>
              <div className="col-lg-12 col-md-12">
                <div class="card">
                  <div class="card-body">
                    <div className="card-header table-section">
                      <h3 className="card-title">Add Bulk Notification</h3>
                    </div>
                    <Form style={{ marginbottom: 8 }}>
                      <Row>
                        <Form.Group
                          className="mb-3 col-sm-6"
                          controlId="validationCustom01"
                        >
                          <Form.Label>Title</Form.Label>
                          <InputGroup>
                            <Form.Control
                              type="text"
                              value={title}
                              placeholder="Enter Title"
                              onChange={(e) => setTitle(e.target.value)}
                              // onChange={userHandler}
                              required
                            />
                          </InputGroup>
                        </Form.Group>

                        <Form.Group
                          className="mb-3 col-sm-6"
                          controlId="validationCustom01"
                        >
                          <Form.Label>Topic</Form.Label>
                          <InputGroup>
                            <Form.Control
                              type="text"
                              value={topic}
                              placeholder="Enter Topic"
                              onChange={(e) => setTopic(e.target.value)}
                              // onChange={userHandler}
                              required
                            />
                          </InputGroup>
                        </Form.Group>
                      </Row>
                      <Row>
                        <Form.Group
                          className="mb-3 col-sm-6"
                          controlId="validationCustom02"
                        >
                          <Form.Label>Status</Form.Label>
                          <Form.Control
                            as="textarea"
                            rows={2}
                            type="text"
                            value={status}
                            placeholder="Enter Status"
                            onChange={(e) => setStatus(e.target.value)}
                            //   onChange={DesHandler}
                            required
                          />
                        </Form.Group>

                        <Form.Group
                          className="mb-3 col-sm-6"
                          controlId="validationCustom02"
                        >
                          <Form.Label>Result</Form.Label>
                          <Form.Control
                            as="textarea"
                            rows={2}
                            type="text"
                            value={result}
                            placeholder="Enter Result"
                            onChange={(e) => setResult(e.target.value)}
                            //   onChange={DesHandler}
                            required
                          />
                        </Form.Group>
                      </Row>
                      <Row>
                        <Form.Group
                          className="mb-3"
                          controlId="validationCustom02"
                        >
                          <Form.Label>Message</Form.Label>
                          <Form.Control
                            as="textarea"
                            rows={3}
                            type="text"
                            value={message}
                            placeholder="Enter Message"
                            onChange={(e) => setMessage(e.target.value)}
                            //   onChange={DesHandler}
                            required
                          />
                        </Form.Group>
                      </Row>
                    </Form>
                    <div className="col-sm-12">
                      <button
                        //   onClick={(e) => handleSubmit(e.target.value, 0)}
                        onClick={postData}
                        style={{ marginRight: 20 }}
                        type="submit"
                        className="mr-1 btn btn-primary"
                      >
                        Submit
                      </button>

                      <button
                        type="submit"
                        className="btn btn-outline-secondary btn-default"
                      >
                        Cancel
                      </button>
                    </div>
                  </div>
                  <div></div>
                </div>
              </div>
            </TabPane>
          </TabContent>
        </div>
      </main>
    </>
  );
}

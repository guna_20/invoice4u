import React, { useState, useEffect } from "react";
import {
  NavLink,
  NavItem,
  Nav,
  Row,
  Col,
  Form,
  InputGroup,
  Button,
} from "react-bootstrap";
import { TabContent, TabPane } from "reactstrap";
import classnames from "classnames";
import Moment from "moment";
import axios from "axios";
import ReactPaginate from "react-paginate";
import PhoneInput from "react-phone-number-input";

export default function EmploymentManagement() {
  const [activeTab, setActiveTab] = useState(1);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [customer, setCustomer] = useState([]);
  const [customerId, setCustomerId] = useState("");
  const [description, setDescription] = useState("");
  const [address, setAddress] = useState("");
  const [altermobileNumber, setAltermobileNumber] = useState("");
  const [email, setEmail] = useState("");
  const [customerPic, setCustomerPic] = useState("");
  const [customerPicPath, setCustomerPicPath] = useState("");
  const [mobileNumber, setMobileNumber] = useState("");
  const [gstNo, setGstNo] = useState("");

  const [selectedPhoto, setSelectedPhoto] = useState(null);
  const [pageSize, setPageSize] = useState(10);
  const [onEdit, setOnEdit] = useState(false);
  const [pageNumber, setPageNumber] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [value, setValue] = useState(0);
  const [searchTerm, setSearchTerm] = useState("");

  const [firstNameErr, setFirstNameErr] = useState(false);
  const [desErr, setDesErr] = useState(false);

  function userHandler(e) {
    let item = e.target.value;
    if (item.length <= 0) {
      setFirstNameErr(true);
    } else {
      setFirstNameErr(false);
    }
    setFirstName(item);
    // setLastName(item);

    console.log(e.target.value);
  }

  function desHandler(e) {
    let item = e.target.value;
    if (item.length > 30 || item.length <= 0) {
      setDesErr(true);
    } else {
      setDesErr(false);
    }
    setDescription(item);

    console.log(e.target.value);
  }
  const handleSubmit = (e, val) => {
    let first = firstName.trim();
    let desc = description.trim();
    if (first.length > 0) {
      setFirstNameErr(false);
      if (desc.length > 0) {
        setFirstNameErr(false);
        setDesErr(false);
        if (val == 0) {
          postData(e);
        } else {
          userData(e);
        }
      } else {
        setDesErr(true);
      }
    } else {
      setFirstNameErr(true);
    }
  };

  const BarStyling = {
    width: "20rem",
    background: "#F2F1F9",
    border: "#0d6efd",
    padding: "0.5rem",
    marginBottom: 10,
  };
  const user = JSON.parse(sessionStorage.getItem("user"));
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };
  let userId = user.userId;
  const baseUrl = "https://executivetracking.cloudjiffy.net/WaterPlantWeb";
  const ImageUrl =
    "https://executivetracking.cloudjiffy.net/WaterPlantWeb/file/downloadFile/?filePath=";

  const postData = () => {
    axios({
      method: "post",
      url: `${baseUrl}/customer/v1/createCustomer`,
      headers,
      data: {
        address,
        altermobileNumber,
        createdBy: {
          userId,
        },
        customerPic,
        description,
        email,
        gstNo,
        firstName,
        lastName,
        mobileNumber,
      },
    })
      .then(function (res) {
        if (res.data.responseCode === 201) {
          alert(res.data.message);
          window.location.reload(false);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setFirstName("");
    setLastName("");
    setDescription("");
    setAddress("");
    setAltermobileNumber("");
    setEmail("");
    setGstNo("");
    setCustomerPic("");
    setMobileNumber("");
    setActiveTab(1);
    NewData();
  };

  const NewData = async () => {
    await axios({
      method: "get",
      url: `${baseUrl}/customer/v1/getAllCustomerByPagination/{pageNumber}/{pageSize}?pageNumber=${pageNumber}&pageSize=${pageSize}`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setPageCount(data.totalPages);
        setCustomer(data.content);
      })

      .catch(function (error) {
        console.log(error);
      });
  };

  const handlePageClick = (data) => {
    setPageNumber(data.selected);
    NewData(pageNumber);
  };
  const handleChange = (e) => {
    setPageSize(e.target.value);
    setValue(e.target.value);
    NewData(pageSize);
  };

  useEffect(() => {
    NewData();
  }, [
    address,
    altermobileNumber,
    customerPic,
    customerPicPath,
    description,
    email,
    firstName,
    lastName,
    gstNo,
    mobileNumber,
    pageSize,
    pageNumber,
  ]);

  const handleRemove = (customerId) => {
    axios({
      method: "delete",
      url: `${baseUrl}/customer/v1/deleteCustomerById/${customerId}`,
      headers,
    })
      .then((res) => {
        if (res.data.responseCode === 200) {
          alert(res.data.message);
          window.location.reload(false);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
        NewData();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const UpdateData = (customerId) => {
    axios({
      method: "get",
      url: `${baseUrl}/customer/v1/getCustomerByCustomerId/{customerId}?customerId=${customerId}`,
      headers,
      data: {
        address,
        altermobileNumber,
        createdBy: {
          userId,
        },
        customerPic,
        customerPicPath,
        description,
        email,
        gstNo,
        firstName,
        lastName,
        mobileNumber,
      },
    })
      .then(function (res) {
        let data = res.data;
        if (data.responseCode === 201) {
          alert(data.message);
          window.location.reload(false);
        } else if (data.responseCode === 400) {
          alert(data.errorMessage);
        }
        setFirstName(res.data.firstName);
        setLastName(res.data.lastName);
        setDescription(res.data.description);
        setAddress(res.data.address);
        setAltermobileNumber(res.data.altermobileNumber);
        setEmail(res.data.email);
        setGstNo(res.data.gstNo);
        setCustomerPic(res.data.customerPic);
        setMobileNumber(res.data.mobileNumber);
        setOnEdit(true);
        setActiveTab(2);
        setCustomerId(res.data.customerId);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const userData = (e) => {
    e.preventDefault();
    axios({
      method: "put",
      url: `${baseUrl}/customer/v1/updateCustomer`,
      headers,
      data: {
        address,
        altermobileNumber,
        createdBy: {
          userId,
        },
        customerPic,
        customerPicPath,
        description,
        email,
        gstNo,
        firstName,
        lastName,
        mobileNumber,
      },
    })
      .then(function (res) {
        let data = res.data;
        if (data.responseCode === 201) {
          alert(data.message);
          window.location.reload(false);
        } else if (data.responseCode === 400) {
          alert(data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setFirstName("");
    setLastName("");
    setDescription("");
    setAddress("");
    setAltermobileNumber("");
    setEmail("");
    setGstNo("");
    setCustomerPic("");
    setMobileNumber("");
    setOnEdit(false);
    setActiveTab(1);
    NewData();
  };

  const onFileChange = (e) => {
    setSelectedPhoto(e.target.files[0]);
    if (!selectedPhoto) {
      // alert("image is required");
      return false;
    }
    if (!selectedPhoto.name.match(/\.(jpg|png)$/)) {
      alert("select valid image (jpg or png)");
      return false;
    }
  };

  const onFileUpload = () => {
    const data = new FormData();
    data.append("file", selectedPhoto);

    axios({
      method: "post",
      url: `https://executivetracking.cloudjiffy.net/WaterPlantWeb/file/uploadFile`,
      headers: {
        "content-type": "multipart/form-data",
        Authorization: "Bearer " + user.accessToken,
      },
      data,
    })
      .then(function (res) {
        console.log(res.data);
        setCustomerPic(res.data.fileName);
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  return (
    <>
      <main id="main" class="main">
        <div class="pagetitle">
          <h1>Customers</h1>
          <nav>
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="/">Home</a>
              </li>
              <li class="breadcrumb-item active">Customers</li>
            </ol>
          </nav>
        </div>
        <div>
          <hr />
        </div>

        <div className="container-fluid">
          <div className="d-flex justify-content-between align-items-center ">
            <Nav
              tabs
              className="page-header-tab"
              variant="pills"
              style={{ marginBottom: 30 }}
            >
              <Nav.Item>
                <Nav.Link
                  className={classnames({ active: activeTab === 1 })}
                  onClick={() => setActiveTab(1)}
                >
                  List View
                </Nav.Link>
              </Nav.Item>
              <NavItem>
                {onEdit ? (
                  <NavLink
                    className={classnames({ active: activeTab === 2 })}
                    onClick={() => setActiveTab(2)}
                  >
                    Edit
                  </NavLink>
                ) : (
                  <NavLink
                    className={classnames({ active: activeTab === 2 })}
                    onClick={() => setActiveTab(2)}
                  >
                    Add
                  </NavLink>
                )}
              </NavItem>
            </Nav>
          </div>
        </div>

        <div class="container-fluid">
          <div class="row justify-content-between">
            <div class="col-4">
              <select
                value={value}
                onChange={handleChange}
                style={{ border: "none", color: "bg-primary" }}
              >
                <option value="10">10 / Pages</option>
                <option value="25">25 / Pages</option>
                <option value="50">50 / Pages</option>
                <option value="100">100 / Pages</option>
              </select>
            </div>
            <div class="col-4">
              <div class="input-group">
                <input
                  style={BarStyling}
                  type="text"
                  placeholder="Search"
                  className="prompt"
                  onChange={(e) => setSearchTerm(e.target.value)}
                />
              </div>
            </div>
          </div>
        </div>

        <div className="container-fluid">
          <TabContent activeTab={activeTab}>
            <TabPane tabId={1} className={classnames(["fade show"])}>
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-body ">
                    <table class="table table-hover bg-gradient-info text-black">
                      <thead class="table-section">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">First Name</th>
                          <th scope="col">Last Name</th>
                          <th scope="col">Customer Pic</th>
                          <th scope="col">Email Id</th>
                          <th scope="col">Gst Number</th>
                          <th scope="col">Mobile Number</th>
                          <th scope="col">Address</th>
                          <th scope="col">Alternate Mobile Number</th>
                          <th scope="col">Description</th>
                          <th className="text-center" scope="col">
                            Created By
                          </th>
                          <th className="text-center" scope="col">
                            Updated By
                          </th>
                          <th scope="col">Inserted Date</th>
                          <th scope="col">Updated Date</th>
                          <th scope="col">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        {customer
                          .filter((val) => {
                            if (searchTerm == "") {
                              return val;
                            } else if (
                              val.firstName
                                .toLowerCase()
                                .includes(searchTerm.toLowerCase())
                            ) {
                              return val;
                            }
                          })
                          .map((data, index) => {
                            return (
                              <tr key={data.customerId}>
                                <th scope="row">{index + 1}</th>
                                <td>{data.firstName}</td>
                                <td>{data.lastName}</td>
                                <td>
                                  <img
                                    src={ImageUrl + data.customerPicPath}
                                    alt={data.customerPic}
                                    style={{ width: 50, height: 50 }}
                                  />
                                </td>

                                <td>{data.mobileNumber}</td>
                                <td>{data.email}</td>
                                <td>{data.gstNo}</td>
                                <td>{data.altermobileNumber}</td>
                                <td>{data.description}</td>
                                <td>{data.address}</td>
                                <td>{data.createdBy.userName}</td>
                                <td>{data.updatedBy.userName}</td>
                                <td>
                                  {Moment(data.insertedDate).format(
                                    "DD-MM-YYYY"
                                  )}
                                </td>
                                <td>
                                  {Moment(data.updatedDate).format(
                                    "DD-MM-YYYY"
                                  )}
                                </td>
                                <td>
                                  <button
                                    type="button"
                                    className="btn btn-icon btn-sm"
                                    title="Edit"
                                    onClick={(e) =>
                                      UpdateData(data.customerId, e)
                                    }
                                  >
                                    <i className="fa fa-edit"></i>
                                  </button>
                                  <button
                                    type="button"
                                    className="btn btn-icon btn-sm js-sweetalert"
                                    title="Delete"
                                    data-type="confirm"
                                    onClick={(e) =>
                                      handleRemove(data.customerId, e)
                                    }
                                  >
                                    <i className="fas fa-trash-alt text-danger"></i>
                                  </button>
                                </td>
                              </tr>
                            );
                          })}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div>
                <ReactPaginate
                  previousLabel="Previous"
                  nextLabel="Next"
                  breakLabel={"..."}
                  pageCount={pageCount}
                  marginPagesDisplayed={2}
                  pageRangeDisplayed={2}
                  onPageChange={handlePageClick}
                  containerClassName={"pagination justify-content-center"}
                  pageClassName={"page-item"}
                  pageLinkClassName={"page-link"}
                  previousClassName={"page-item"}
                  previousLinkClassName={"page-link"}
                  nextClassName={"page-item"}
                  nextLinkClassName={"page-link"}
                  breakClassName={"page-item"}
                  breakLinkClassName={"page-link"}
                  activeClassName={"active"}
                />
              </div>
            </TabPane>
            <TabPane tabId={2} className={classnames(["fade show"])}>
              <div className="col-lg-12 col-md-12">
                <div class="card">
                  <div class="card-body">
                    <div className="card-header table-section">
                      {onEdit ? (
                        <h3 className="card-title">Update Customers</h3>
                      ) : (
                        <h3 className="card-title">Add Customers</h3>
                      )}
                    </div>
                    <Form noValidate>
                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="6"
                          controlId="validationCustom01"
                        >
                          <Form.Label>Customer First Name</Form.Label>
                          <Form.Control
                            required
                            type="text"
                            value={firstName}
                            className="form-control"
                            placeholder="Enter First Name"
                            // onChange={(e) => setFirstName(e.target.value)}
                            onChange={userHandler}
                          />
                          {firstNameErr ? (
                            <span style={{ color: "red", marginLeft: 20 }}>
                              Customer Name is required!!
                            </span>
                          ) : null}
                        </Form.Group>
                        <Form.Group
                          as={Col}
                          md="6"
                          controlId="validationCustom01"
                        >
                          <Form.Label>Customer Last Name</Form.Label>
                          <Form.Control
                            required
                            type="text"
                            value={lastName}
                            className="form-control"
                            placeholder="Enter Last Name"
                            onChange={(e) => setLastName(e.target.value)}
                            // onChange={userHandler}
                          />
                        </Form.Group>
                      </Row>
                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="4"
                          controlId="validationCustomUsername"
                        >
                          <Form.Label>Email Id</Form.Label>
                          <InputGroup hasValidation>
                            <Form.Control
                              value={email}
                              className="form-control"
                              placeholder="Enter Email id"
                              onChange={(e) => setEmail(e.target.value)}
                              required
                            />
                          </InputGroup>
                        </Form.Group>

                        <Form.Group
                          as={Col}
                          md="4"
                          controlId="validationCustomUsername"
                        >
                          <Form.Label>GST Number</Form.Label>
                          <InputGroup hasValidation>
                            <Form.Control
                              value={gstNo}
                              className="form-control"
                              placeholder="Enter GST Number"
                              onChange={(e) => setGstNo(e.target.value)}
                              required
                            />
                          </InputGroup>
                        </Form.Group>
                      </Row>

                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="4"
                          controlId="validationCustomUsername"
                        >
                          <Form.Label>Mobile Number</Form.Label>
                          <InputGroup.Text id="inputGroupPrepend">
                            <PhoneInput
                              placeholder="Enter Mobile number"
                              value={mobileNumber}
                              onChange={setMobileNumber}
                            />
                          </InputGroup.Text>
                        </Form.Group>

                        <Form.Group
                          as={Col}
                          md="4"
                          controlId="validationCustomUsername"
                        >
                          <Form.Label>Alternate Mobile Number</Form.Label>
                          <InputGroup.Text id="inputGroupPrepend">
                            <PhoneInput
                              placeholder="Enter Alternate Mobile number"
                              value={altermobileNumber}
                              onChange={setAltermobileNumber}
                            />
                          </InputGroup.Text>
                        </Form.Group>
                      </Row>
                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="12"
                          controlId="validationCustom01"
                        >
                          <Form.Label>Address</Form.Label>
                          <Form.Control
                            as="textarea"
                            rows={3}
                            required
                            value={address}
                            className="form-control no-resize"
                            placeholder="Add Address"
                            onChange={(e) => setAddress(e.target.value)}
                          />
                        </Form.Group>
                      </Row>

                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="12"
                          controlId="validationCustom03"
                        >
                          <Form.Label>Description</Form.Label>
                          <Form.Control
                            as="textarea"
                            rows={3}
                            value={description}
                            className="form-control no-resize"
                            placeholder="Add description"
                            // onChange={(e) => setDescription(e.target.value)}
                            onChange={desHandler}
                            required
                          />
                          {desErr ? (
                            <span style={{ color: "red", marginLeft: 20 }}>
                              Description is required!!
                            </span>
                          ) : null}
                        </Form.Group>
                      </Row>

                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="6"
                          controlId="validationCustom03"
                        >
                          <Form.Label>File</Form.Label>
                          <Form.Control
                            type="file"
                            required
                            name="file"
                            className="form-control"
                            placeholder="Upload Photo"
                            onChange={(e) => onFileChange(e)}
                          />
                        </Form.Group>
                        <div className="col-sm-6">
                          <Button
                            style={{ marginbottom: 20 }}
                            className="mr-1 p-2 btn btn-primary"
                            onClick={onFileUpload}
                          >
                            Upload
                          </Button>
                        </div>
                      </Row>
                    </Form>
                    <div className="col-sm-12">
                      {onEdit ? (
                        <button
                          onClick={(e) => handleSubmit(e.target.value, 1)}
                          // onClick={userData}
                          style={{ marginRight: 20 }}
                          type="submit"
                          className="mr-1 btn btn-primary"
                        >
                          Update
                        </button>
                      ) : (
                        <button
                          onClick={(e) => handleSubmit(e.target.value, 0)}
                          // onClick={postData}
                          style={{ marginRight: 20 }}
                          type="submit"
                          className="mr-1 btn btn-primary"
                        >
                          Submit
                        </button>
                      )}

                      <button
                        type="submit"
                        className="btn btn-outline-secondary btn-default"
                      >
                        Cancel
                      </button>
                    </div>
                  </div>
                </div>
                <div></div>
              </div>
            </TabPane>
          </TabContent>
        </div>
      </main>
    </>
  );
}

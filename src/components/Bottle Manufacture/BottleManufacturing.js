import React, { useState, useEffect } from "react";
import {
  NavLink,
  NavItem,
  Nav,
  Row,
  Col,
  Form,
  InputGroup,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import { TabContent, TabPane } from "reactstrap";
import classnames from "classnames";
import Moment from "moment";
import axios from "axios";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import ReactPaginate from "react-paginate";

export default function BottleManufacturing() {
  const [activeTab, setActiveTab] = useState(1);

  const [bottlemaking, setBottleMaking] = useState([]);
  const [bottlemakingId, setBottlemakingId] = useState("");
  const [failedBottles, setFailedBottles] = useState("");
  const [makingDate, setMakingDate] = useState("");
  const [remark, setRemark] = useState("");
  const [successBottles, setSuccessBottles] = useState("");
  const [totalRawBottles, setTotalRawBottles] = useState("");

  const [rawmaterial, setRawmaterial] = useState([]);
  const [rawmaterialId, setRawmaterialId] = useState("");
  const [rawMaterialName, setRawmaterialName] = useState("");
  const [rawValue, setRawValue] = useState("");

  const [pageSize, setPageSize] = useState(10);
  const [onEdit, setOnEdit] = useState(false);
  const [pageNumber, setPageNumber] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [value, setValue] = useState(0);
  const [searchTerm, setSearchTerm] = useState("");

  const BarStyling = {
    width: "20rem",
    background: "#F2F1F9",
    border: "#0d6efd",
    padding: "0.5rem",
    marginBottom: 10,
  };

  const user = JSON.parse(sessionStorage.getItem("user"));
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };

  let userId = user.userId;

  const data = {
    bottlemakingId,
    createdBy: {
      userId,
    },
    failedBottles,
    makingDate,
    rawMaterialDtoList: {
      rawMaterialName,
      rawmaterialId,
    },
    remark,
    successBottles,
    totalRawBottles,
  };
  const baseUrl = "https://executivetracking.cloudjiffy.net/WaterPlantWeb";

  const postData = (e) => {
    e.preventDefault();
    axios({
      method: "post",
      url: `${baseUrl}/bottle/v1/createBottlesManufacturing`,
      headers,
      data,
    })
      .then(function (res) {
        let data = res.data;
        if (data.responseCode === 201) {
          alert(data.message);
          window.location.reload(false);
        } else if (data.responseCode === 400) {
          alert(data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setBottlemakingId("");
    setFailedBottles("");
    setMakingDate("");
    setRemark("");
    setSuccessBottles("");
    setTotalRawBottles("");
    setRawmaterialId("");
    setRawmaterialName("");
    setActiveTab(1);
    NewData("");
  };

  const NewData = () => {
    axios({
      method: "get",
      url: `${baseUrl}/bottle/v1/getAllBottlesManufacturingByPagination/{pageNumber}/{pageSize}?pageNumber=${pageNumber}&pageSize=${pageSize}`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setPageCount(data.totalPages);
        setBottleMaking(data.content);
      })

      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => {
    NewData();
    RawMaterialData();
  }, [data, pageSize, pageNumber]);

  const handlePageClick = (data) => {
    setPageNumber(data.selected);
    NewData(pageNumber);
  };
  const handleChange = (e) => {
    setPageSize(e.target.value);
    setValue(e.target.value);
    NewData(pageSize);
  };

  const UpdateData = (bottlemakingId) => {
    axios({
      method: "get",
      url: `${baseUrl}/bottle/v1/getBottlesManufacturingById/{bottlemakingId}?bottlemakingId=${bottlemakingId}`,
      headers,
      data: {
        bottlemakingId,
        createdBy: {
          userId,
        },
        failedBottles,
        makingDate,
        rawMaterialDtoList: {
          rawMaterialName,
          rawmaterialId,
        },
        remark,
        successBottles,
        totalRawBottles,
      },
    })
      .then(function (res) {
        let data = res.data;
        if (data.responseCode === 201) {
          alert(data.message);
          window.location.reload(false);
        } else if (data.responseCode === 400) {
          alert(data.errorMessage);
        }
        setBottlemakingId(bottlemakingId);
        setFailedBottles(data.failedBottles);
        setRemark(data.remark);
        setSuccessBottles(data.successBottles);
        setTotalRawBottles(data.totalRawBottles);
        setMakingDate(data.makingDate);
        setRawmaterialId(data.rawmaterialId);
        setRawmaterialName(data.rawMaterialName);
        setOnEdit(true);
        setActiveTab(2);
      })
      .catch(function (err) {
        console.log(err);
      });
  };
  const RawMaterialData = () => {
    axios({
      method: "get",
      url: `${baseUrl}/rawmaterial/v1/getAllRawMaterials`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setRawmaterial(data);
      })

      .catch(function (error) {
        console.log(error);
      });
  };
  const handleTypeChange = (e) => {
    setRawmaterialId(e.target.value);
    setRawValue(e.target.value);
    console.log(rawmaterialId);
  };

  return (
    <>
      <main id="main" class="main">
        <div class="pagetitle">
          <h1>Bottle Manufacturing</h1>
          <nav>
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <Link to="/">Home</Link>
              </li>

              <li class="breadcrumb-item active">Bottle Manufacturing</li>
            </ol>
          </nav>
        </div>
        <div>
          <hr />
        </div>

        <div className="container-fluid">
          <div className="d-flex justify-content-between align-items-center ">
            <Nav
              tabs
              className="page-header-tab"
              variant="pills"
              style={{ marginBottom: 30 }}
            >
              <Nav.Item>
                <Nav.Link
                  className={classnames({ active: activeTab === 1 })}
                  onClick={() => setActiveTab(1)}
                >
                  List View
                </Nav.Link>
              </Nav.Item>
              <NavItem>
                {onEdit ? (
                  <NavLink
                    className={classnames({ active: activeTab === 2 })}
                    onClick={() => setActiveTab(2)}
                  >
                    Edit
                  </NavLink>
                ) : (
                  <NavLink
                    className={classnames({ active: activeTab === 2 })}
                    onClick={() => setActiveTab(2)}
                  >
                    Add
                  </NavLink>
                )}
              </NavItem>
            </Nav>
          </div>
        </div>

        <div class="container-fluid">
          <div class="row justify-content-between">
            <div class="col-4">
              <select
                value={value}
                onChange={handleChange}
                style={{ border: "none", color: "bg-primary" }}
              >
                <option value="10">10 / Pages</option>
                <option value="25">25 / Pages</option>
                <option value="50">50 / Pages</option>
                <option value="100">100 / Pages</option>
              </select>
            </div>
            <div class="col-4">
              <div class="input-group">
                <input
                  style={BarStyling}
                  type="text"
                  placeholder="Search"
                  className="prompt"
                  onChange={(e) => setSearchTerm(e.target.value)}
                />
              </div>
            </div>
          </div>
        </div>

        <div className="container-fluid">
          <TabContent activeTab={activeTab}>
            <TabPane tabId={1} className={classnames(["fade show"])}>
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-body ">
                    <table class="table table-hover bg-gradient-info text-black">
                      <thead class="table-section">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Total Raw Bottles</th>
                          <th scope="col">Success Bottles</th>
                          <th scope="col">Failed Bottles</th>
                          <th scope="col">Making Date</th>
                          <th scope="col">remark</th>
                          <th className="text-center" scope="col">
                            Created By
                          </th>
                          <th className="text-center" scope="col">
                            Updated By
                          </th>
                          {/* <th scope="col">Actions</th> */}
                          {/* <th scope="col">Inserted Date</th> */}
                          {/* <th scope="col">Updated Date</th> */}
                        </tr>
                      </thead>
                      <tbody>
                        {bottlemaking
                          .filter((val) => {
                            if (searchTerm == "") {
                              return val;
                            } else if (
                              val.remark
                                .toLowerCase()
                                .includes(searchTerm.toLowerCase())
                            ) {
                              return val;
                            }
                          })
                          .map((data, index) => {
                            return (
                              <tr key={data.bottlemakingId}>
                                <th scope="row">{index + 1}</th>
                                <td>{data.totalRawBottles}</td>
                                <td>{data.successBottles}</td>
                                <td>{data.failedBottles}</td>
                                <td>
                                  {Moment(data.makingDate).format("DD-MM-YYYY")}
                                </td>
                                <td>{data.remark}</td>
                                {/* <td>{data.createdBy.userName}</td>
                                    <td>{data.createdBy.userName}</td> */}
                                <td>
                                  {Moment(data.insertedDate).format(
                                    "DD-MM-YYYY"
                                  )}
                                </td>
                                <td>
                                  {Moment(data.updatedDate).format(
                                    "DD-MM-YYYY"
                                  )}
                                </td>
                                <td>
                                  <button
                                    type="button"
                                    className="btn btn-icon btn-sm"
                                    title="Edit"
                                    onClick={(e) =>
                                      UpdateData(data.bottlemakingId, e)
                                    }
                                  >
                                    {/* <i className="fa fa-edit"></i> */}
                                  </button>
                                </td>
                              </tr>
                            );
                          })}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div>
                <ReactPaginate
                  previousLabel="Previous"
                  nextLabel="Next"
                  breakLabel={"..."}
                  pageCount={pageCount}
                  marginPagesDisplayed={2}
                  pageRangeDisplayed={2}
                  onPageChange={handlePageClick}
                  containerClassName={"pagination justify-content-center"}
                  pageClassName={"page-item"}
                  pageLinkClassName={"page-link"}
                  previousClassName={"page-item"}
                  previousLinkClassName={"page-link"}
                  nextClassName={"page-item"}
                  nextLinkClassName={"page-link"}
                  breakClassName={"page-item"}
                  breakLinkClassName={"page-link"}
                  activeClassName={"active"}
                />
              </div>
            </TabPane>
            <TabPane tabId={2} className={classnames(["fade show"])}>
              <div className="col-lg-12 col-md-12">
                <div class="card">
                  <div class="card-body">
                    <div className="card-header table-section">
                      <h3 className="card-title">Add Bottle Manufacturing</h3>
                    </div>
                    <Form noValidate>
                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="4"
                          controlId="validationCustom01"
                        >
                          <Form.Label>Total Raw Bottles</Form.Label>
                          <Form.Control
                            required
                            value={totalRawBottles}
                            type="number"
                            className="form-control "
                            placeholder="Add Total Raw Bottles"
                            onChange={(e) => setTotalRawBottles(e.target.value)}
                          />
                        </Form.Group>
                        <Form.Group
                          as={Col}
                          md="4"
                          controlId="validationCustom01"
                        >
                          <Form.Label>Success Bottles</Form.Label>
                          <Form.Control
                            required
                            value={successBottles}
                            type="number"
                            className="form-control "
                            placeholder="Add Success Bottles"
                            onChange={(e) => setSuccessBottles(e.target.value)}
                          />
                        </Form.Group>

                        <Form.Group
                          as={Col}
                          md="4"
                          controlId="validationCustom01"
                        >
                          <Form.Label>Failed Bottles</Form.Label>
                          <Form.Control
                            required
                            value={failedBottles}
                            type="number"
                            className="form-control "
                            placeholder="Add Failed Bottles"
                            onChange={(e) => setFailedBottles(e.target.value)}
                          />
                        </Form.Group>
                      </Row>
                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="4"
                          controlId="validationCustom01"
                        >
                          <Form.Label>Choose making Date</Form.Label>
                          <InputGroup.Text id="inputGroupPrepend">
                            <i class="bi bi-calendar-check"></i>
                            <DatePicker
                              selected={makingDate}
                              placeholder="Choosedate"
                              onChange={(date) => setMakingDate(date)}
                            />
                          </InputGroup.Text>
                        </Form.Group>
                      </Row>
                      <Row>
                        <Form.Group as={Col} md="4" controlId="formGridState">
                          <Form.Label>Raw Material</Form.Label>
                          <Form.Select
                            rawValue={value}
                            onChange={handleTypeChange}
                          >
                            <option>Select a Raw Material</option>
                            {rawmaterial.map((data) => {
                              return (
                                <option value={data.rawmaterialId}>
                                  {data.rawMaterialName}
                                </option>
                              );
                            })}
                          </Form.Select>
                        </Form.Group>
                      </Row>
                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="12"
                          controlId="validationCustom01"
                        >
                          <Form.Label>Remark</Form.Label>
                          <Form.Control
                            as="textarea"
                            rows={3}
                            required
                            value={remark}
                            className="form-control no-resize"
                            placeholder="Add Remark"
                            onChange={(e) => setRemark(e.target.value)}
                          />
                        </Form.Group>
                      </Row>
                    </Form>
                    <div className="col-sm-12">
                      <button
                        onClick={postData}
                        style={{ marginRight: 20 }}
                        type="submit"
                        className="mr-1 btn btn-primary"
                      >
                        Submit
                      </button>
                      <button
                        type="submit"
                        className="btn btn-outline-secondary btn-default"
                      >
                        Cancel
                      </button>
                    </div>
                  </div>
                </div>
                <div></div>
              </div>
            </TabPane>
          </TabContent>
        </div>
      </main>
    </>
  );
}

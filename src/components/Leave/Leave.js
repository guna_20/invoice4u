import React from "react";

export default function Expenditure() {
  return (
    <>
      <main id="main" className="main d-flex float-left">
        <div className="pagetitle">
          <h1>Leave</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <a href="">Leave</a>
              </li>
              <li className="breadcrumb-item active">Leave</li>
            </ol>
          </nav>
        </div>
      </main>
    </>
  );
}

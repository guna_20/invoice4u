import React from "react";

export default function TimeTable() {
  return (
    <>
      <main id="main" className="main d-flex float-left">
        <div className="pagetitle">
          <h1>Time Table</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <a href="/">Home </a>
              </li>
              <li className="breadcrumb-item active">Time Table</li>
            </ol>
          </nav>
        </div>
      </main>
    </>
  );
}

import React from "react";
import { Link, Outlet } from "react-router-dom";

export default function SideBar() {
  return (
    <div>
      <aside id="sidebar" className="sidebar">
        <nav>
          <ul className="sidebar-nav" id="sidebar-nav">
            <li className="nav-item">
              <Link className="nav-link " to="/">
                Dashboard
              </Link>
            </li>

            <li className="nav-item">
              <Link
                to="/sales"
                className="nav-link collapsed"
                data-bs-target="#sales-nav"
                data-bs-toggle="collapse"
              >
                <i className="bi bi-menu-button-wide"></i>
                Sales
                <i className="bi bi-chevron-down ms-auto"></i>
              </Link>
              <ul
                id="sales-nav"
                className="nav-content collapse "
                data-bs-parent="#sidebar-nav"
              > 
              <li>
                <Link to="/sales">
                  <i className="bi bi-circle"></i>
                  <span>Sales</span>
                </Link>
              </li>
              <li>
                <Link to="/salesreturn">
                  <i className="bi bi-circle"></i>
                  <span>Sales Return</span>
                </Link>
              </li>
              </ul>
            </li>

            <li className="nav-item">
              <Link
                to="/water"
                className="nav-link collapsed"
                data-bs-target="#water-nav"
                data-bs-toggle="collapse"
              >
                <i class="bi bi-water"></i>
                <span>Water Bottles</span>
                <i className="bi bi-chevron-down ms-auto"></i>
              </Link>
              <ul
                id="water-nav"
                className="nav-content collapse "
                data-bs-parent="#sidebar-nav"
              > 
                <li>
                  <Link to="water">
                    <i className="bi bi-circle"></i>
                    <span>Water Bottles Manufacturing</span>
                  </Link>
                </li>
              </ul>
            </li>

            <li className="nav-item">
              <Link
                to="/purchase"
                className="nav-link collapsed"
                data-bs-target="#purchase-nav"
                data-bs-toggle="collapse"
              >
                <i class="bi bi-basket-fill"></i>
                <span>Purchase</span>
                <i className="bi bi-chevron-down ms-auto"></i>
              </Link>
              <ul
                id="purchase-nav"
                className="nav-content collapse "
                data-bs-parent="#sidebar-nav"
              > 
                <li>
                  <Link to="purchase">
                    <i className="bi bi-circle"></i>
                    <span>Purchase</span>
                  </Link>
                </li>
                <li>
                  <Link to="purchasereturn">
                    <i className="bi bi-circle"></i>
                    <span>Purchase Return</span>
                  </Link>
                </li>
              </ul>
            </li>

            <li className="nav-item">
              <Link
                to="/bottle"
                className="nav-link collapsed"
                data-bs-target="#bottle-nav"
                data-bs-toggle="collapse"
              >
                <i class="bi bi-bar-chart-line"></i>
                <span>Bottle Manufacture</span>
                <i className="bi bi-chevron-down ms-auto"></i>
              </Link>
              <ul
                id="bottle-nav"
                className="nav-content collapse "
                data-bs-parent="#sidebar-nav"
              > 
                <li>
                  <Link to="bottle">
                    <i className="bi bi-circle"></i>
                    <span>Bottle Manufacturing</span>
                  </Link>
                </li>
                
              </ul>
            </li>

            <li className="nav-item">
              <Link
                to="/inventory"
                className="nav-link collapsed"
                data-bs-target="#inventory-nav"
                data-bs-toggle="collapse"
              >
                <i class="bi bi-hdd-stack"></i>
                <span>Inventory</span>
                {/* <i className="bi bi-chevron-down ms-auto"></i> */}
              </Link>
              {/* <ul
                id="inventory-nav"
                className="nav-content collapse "
                data-bs-parent="#sidebar-nav"
              > 
                <li>
                  <Link to="inventory">
                    <i className="bi bi-circle"></i>
                    <span>Bottle Manufacturing</span>
                  </Link>
                </li>
                
              </ul> */}
            </li>

            <li className="nav-item">
              <Link
                to="/expenditure"
                className="nav-link collapsed"
                data-bs-target="#expenditure-nav"
                data-bs-toggle="collapse"
              >
               <i class="bi bi-bank"></i>
                <span>Expenditure</span>
                {/* <i className="bi bi-chevron-down ms-auto"></i> */}
              </Link>
              {/* <ul
                id="bottle-nav"
                className="nav-content collapse "
                data-bs-parent="#sidebar-nav"
              > 
                <li>
                  <Link to="bottle">
                    <i className="bi bi-circle"></i>
                    <span>Bottle Manufacturing</span>
                  </Link>
                </li> 
              </ul> */}
            </li>

            <li className="nav-item">
              <Link
                to="/leave"
                className="nav-link collapsed"
                data-bs-target="#leave-nav"
                data-bs-toggle="collapse"
              >
                <i class="bi bi-megaphone"></i>
                <span>Leave</span>
                {/* <i className="bi bi-chevron-down ms-auto"></i> */}
              </Link>
              {/* <ul
                id="bottle-nav"
                className="nav-content collapse "
                data-bs-parent="#sidebar-nav"
              > 
                <li>
                  <Link to="bottle">
                    <i className="bi bi-circle"></i>
                    <span>Bottle Manufacturing</span>
                  </Link>
                </li>
                
              </ul> */}
            </li>

            <li className="nav-item">
              <Link
                to="/employee"
                className="nav-link collapsed"
                data-bs-target="#employee-nav"
                data-bs-toggle="collapse"
              >
                <i class="bi bi-wallet-fill"></i>
                <span>Employment</span>
                <i className="bi bi-chevron-down ms-auto"></i>
              </Link>
              <ul
                id="employee-nav"
                className="nav-content collapse "
                data-bs-parent="#sidebar-nav"
              > 
                <li>
                  <Link to="employee">
                    <i className="bi bi-circle"></i>
                    <span>Employment Management</span>
                  </Link>
                </li>
                
              </ul>
            </li>
            <li className="nav-item">
              <Link
                to="/masters"
                className="nav-link collapsed"
                data-bs-target="#masters-nav"
                data-bs-toggle="collapse"
              >
                <i class="bi bi-table"></i>
                <span>Masters</span>
                <i className="bi bi-chevron-down ms-auto"></i>
              </Link>
              <ul
                id="masters-nav"
                className="nav-content collapse "
                data-bs-parent="#sidebar-nav"
              > 
                <li>
                  <Link to="category">
                    <i className="bi bi-circle"></i>
                    <span>Category</span>
                  </Link>
                </li>
                <li>
                  <Link to="product">
                    <i className="bi bi-circle"></i>
                    <span>Product</span>
                  </Link>
                </li>
                <li>
                  <Link to="supplier">
                    <i className="bi bi-circle"></i>
                    <span>Material Supplier</span>
                  </Link>
                </li>
                <li>
                  <Link to="rawmaterial">
                    <i className="bi bi-circle"></i>
                    <span>Raw Material</span>
                  </Link>
                </li>
                <li>
                  <Link to="discount">
                    <i className="bi bi-circle"></i>
                    <span>Discount</span>
                  </Link>
                </li>
                
              </ul>
            </li>
            {/* <li className="nav-item">
              <Link  to="/customers">
                Customers
              </Link>
            </li> */}
            <li className="nav-item">
              <Link
                to="/customers"
                className="nav-link collapsed"
                data-bs-target="#customers-nav"
                data-bs-toggle="collapse"
              >
                <i class="bi bi-cash-stack"></i>
                <span>Customers</span>
                <i className="bi bi-chevron-down ms-auto"></i>
              </Link>
              <ul
                id="customers-nav"
                className="nav-content collapse "
                data-bs-parent="#sidebar-nav"
              > 
                <li>
                  <Link to="customers">
                    <i className="bi bi-circle"></i>
                    <span>Customers</span>
                  </Link>
                </li>
                
              </ul>
            </li>
            <li className="nav-item">
              <Link
                to="/payment"
                className="nav-link collapsed"
                data-bs-target="#payment-nav"
                data-bs-toggle="collapse"
              >
                <i class="bi bi-cash-stack"></i>
                <span>Payments</span>
                {/* <i className="bi bi-chevron-down ms-auto"></i> */}
              </Link>
              {/* <ul
                id="bottle-nav"
                className="nav-content collapse "
                data-bs-parent="#sidebar-nav"
              > 
                <li>
                  <Link to="bottle">
                    <i className="bi bi-circle"></i>
                    <span>Bottle Manufacturing</span>
                  </Link>
                </li>
                
              </ul> */}
            </li>
            <li className="nav-item">
              <Link
                to="/notice"
                className="nav-link collapsed"
                data-bs-target="#notice-nav"
                data-bs-toggle="collapse"
              >
               <i class="bi bi-bell"></i>
                <span>Notification</span>
                {/* <i className="bi bi-chevron-down ms-auto"></i> */}
              </Link>
              {/* <ul
                id="bottle-nav"
                className="nav-content collapse "
                data-bs-parent="#sidebar-nav"
              > 
                <li>
                  <Link to="bottle">
                    <i className="bi bi-circle"></i>
                    <span>Bottle Manufacturing</span>
                  </Link>
                </li>
                
              </ul> */}
            </li>
            <li className="nav-item">
              <Link
                to="/bottle"
                className="nav-link collapsed"
                data-bs-target="#bottle-nav"
                data-bs-toggle="collapse"
              >
                <i class="bi bi-calendar-check"></i>
                <span>Calendar</span>
                {/* <i className="bi bi-chevron-down ms-auto"></i> */}
              </Link>
              {/* <ul
                id="bottle-nav"
                className="nav-content collapse "
                data-bs-parent="#sidebar-nav"
              > 
                <li>
                  <Link to="bottle">
                    <i className="bi bi-circle"></i>
                    <span>Bottle Manufacturing</span>
                  </Link>
                </li>
                
              </ul> */}
            </li>
            <li className="nav-item">
              <Link
                to="/bottle"
                className="nav-link collapsed"
                data-bs-target="#bottle-nav"
                data-bs-toggle="collapse"
              >
               <i class="bi bi-clock-fill"></i>
                <span>Time Table</span>
                {/* <i className="bi bi-chevron-down ms-auto"></i> */}
              </Link>
              {/* <ul
                id="bottle-nav"
                className="nav-content collapse "
                data-bs-parent="#sidebar-nav"
              > 
                <li>
                  <Link to="bottle">
                    <i className="bi bi-circle"></i>
                    <span>Bottle Manufacturing</span>
                  </Link>
                </li>
                
              </ul> */}
            </li>
            <li className="nav-item">
              <Link
                to="/bottle"
                className="nav-link collapsed"
                data-bs-target="#bottle-nav"
                data-bs-toggle="collapse"
              >
                <i class="bi bi-people-fill"></i>
                <span>Users</span>
                {/* <i className="bi bi-chevron-down ms-auto"></i> */}
              </Link>
              {/* <ul
                id="bottle-nav"
                className="nav-content collapse "
                data-bs-parent="#sidebar-nav"
              > 
                <li>
                  <Link to="bottle">
                    <i className="bi bi-circle"></i>
                    <span>Bottle Manufacturing</span>
                  </Link>
                </li>
                
              </ul> */}
            </li>

            <li className="nav-item">
              <Link
                to="/bottle"
                className="nav-link collapsed"
                data-bs-target="#bottle-nav"
                data-bs-toggle="collapse"
              >
               <i class="bi bi-flag"></i>
                <span>Report</span>
                {/* <i className="bi bi-chevron-down ms-auto"></i> */}
              </Link>
              {/* <ul
                id="bottle-nav"
                className="nav-content collapse "
                data-bs-parent="#sidebar-nav"
              > 
                <li>
                  <Link to="bottle">
                    <i className="bi bi-circle"></i>
                    <span>Bottle Manufacturing</span>
                  </Link>
                </li>
                
              </ul> */}
            </li>

            <li className="nav-item">
              <Link
                to="/bottle"
                className="nav-link collapsed"
                data-bs-target="#bottle-nav"
                data-bs-toggle="collapse"
              >
                <i class="bi bi-person-rolodex"></i>
                <span>Contact Us</span>
                {/* <i className="bi bi-chevron-down ms-auto"></i> */}
              </Link>
              {/* <ul
                id="bottle-nav"
                className="nav-content collapse "
                data-bs-parent="#sidebar-nav"
              > 
                <li>
                  <Link to="bottle">
                    <i className="bi bi-circle"></i>
                    <span>Bottle Manufacturing</span>
                  </Link>
                </li>
                
              </ul> */}
            </li>
            <li className="nav-item">
              <Link
                to="/bottle"
                className="nav-link collapsed"
                data-bs-target="#bottle-nav"
                data-bs-toggle="collapse"
              >
                <i class="bi bi-gear-fill"></i>
                <span>Settings</span>
                {/* <i className="bi bi-chevron-down ms-auto"></i> */}
              </Link>
              {/* <ul
                id="bottle-nav"
                className="nav-content collapse "
                data-bs-parent="#sidebar-nav"
              > 
                <li>
                  <Link to="bottle">
                    <i className="bi bi-circle"></i>
                    <span>Bottle Manufacturing</span>
                  </Link>
                </li>
                
              </ul> */}
            </li>
            {/* <li className="nav-heading">Pages</li>

          <li className="nav-item">
            <a className="nav-link collapsed" href="users-profile.html">
              <i className="bi bi-person"></i>
              <span>Profile</span>
            </a>
          </li>

          <li className="nav-item">
            <a className="nav-link collapsed" href="pages-faq.html">
              <i className="bi bi-question-circle"></i>
              <span>F.A.Q</span>
            </a>
          </li>

          <li className="nav-item">
            <a className="nav-link collapsed" href="pages-contact.html">
              <i className="bi bi-envelope"></i>
              <span>Contact</span>
            </a>
          </li>

          <li className="nav-item">
            <a className="nav-link collapsed" href="pages-register.html">
              <i className="bi bi-card-list"></i>
              <span>Register</span>
            </a>
          </li>

          <li className="nav-item">
            <a className="nav-link collapsed" href="pages-login.html">
              <i className="bi bi-box-arrow-in-right"></i>
              <span>Login</span>
            </a>
          </li>

          <li className="nav-item">
            <a className="nav-link collapsed" href="pages-error-404.html">
              <i className="bi bi-dash-circle"></i>
              <span>Error 404</span>
            </a>
          </li>

          <li className="nav-item">
            <a className="nav-link collapsed" href="pages-blank.html">
              <i className="bi bi-file-earmark"></i>
              <span>Blank</span>
            </a>
          </li> */}
          </ul>
        </nav>
      </aside>
      <Outlet />
    </div>
  );
}

import React, { useState, useEffect } from 'react';
import {
  NavLink,
  NavItem,
  Nav,
  Row,
  Col,
  Form,
} from 'react-bootstrap';
import { TabContent, TabPane } from 'reactstrap';
import classnames from 'classnames';
import Moment from 'moment';
import axios from 'axios';
import ReactPaginate from 'react-paginate';
import "react-datepicker/dist/react-datepicker.css";


export default function EmploymentManagement() {
  const [activeTab, setActiveTab] = useState(1);

  const [expenditureType, setExpenditureType] = useState([]);
  const [expenditureTypeId, setExpenditureTypeId] = useState('');
  const [expenditureTypeName, setExpenditureTypeName] = useState('');
  const [description, setDescription] = useState('');
 
  const [selectedPhoto, setSelectedPhoto] = useState(null);
  const [pageSize, setPageSize] = useState(10);
  const [onEdit, setOnEdit] = useState(false);
  const [pageNumber, setPageNumber] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [value, setValue] = useState(0);
  const [searchTerm, setSearchTerm] = useState('');

  const [expenditureTypeErr, setExpenditureTypeErr] = useState(false);
  const [descErr, setDescErr] = useState(false);

  function firstHandler(e) {
    let item = e.target.value;
    if (item.length <= 0) {
      setExpenditureTypeErr(true);
    } else {
      setExpenditureTypeErr(false);
    }
    setExpenditureTypeName(item);
    // setLastName(item);

    console.log(e.target.value);
  }

  function descHandler(e) {
    let item = e.target.value;
    if (item.length > 30 || item.length <= 0) {
      // console.log(setDesErr)
      setDescErr(true);
    } else {
      setDescErr(false);
    }
    setDescription(item);
    // setAddress(item);

    console.log(e.target.value);
  }
  const handleSubmit = (e, val) => {
    let first = expenditureTypeName.trim();
    let desc = description.trim();

    if (first.length > 0) {
        setExpenditureTypeErr(false);
      if (desc.length > 0) {
        setExpenditureTypeErr(false);
        setDescErr(false);
        if (val == 0) {
          postData(e);
        } else {
          userData(e);
        }
      } else {
        setDescErr(true);
      }
    } else {
      setExpenditureTypeErr(true);
    }
  };

  const BarStyling = {
    width: '20rem',
    background: '#F2F1F9',
    border: '#0d6efd',
    padding: '0.5rem',
    marginBottom: 10,
  };
  const user = JSON.parse(sessionStorage.getItem('user'));
  const headers = {
    'Content-type': 'application/json',
    Authorization: 'Bearer ' + user.accessToken,
  };
  let userId = user.userId;
  const baseUrl = 'https://executivetracking.cloudjiffy.net/WaterPlantWeb';
  const ImageUrl =
    'https://executivetracking.cloudjiffy.net/WaterPlantWeb/file/downloadFile/?filePath=';

  const refreshPage = () => {
    window.location.reload(false);
  };

  const postData = () => {
    axios({
      method: 'post',
      url: `${baseUrl}/expenditureType/v1/createExpenditureType`,
      headers,
      data: {
        createdBy: {
          userId,
        },
        description,
        expenditureTypeId,
        expenditureTypeName,   
      }
    })
      .then(function (res) {
        if (res.data.responseCode === 201) {
          alert(res.data.message);
          window.location.reload(false);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
      setExpenditureTypeName('');
    setDescription('');  
    setExpenditureTypeId('');
    setActiveTab(1);
    NewData();
  };

  const NewData = async () => {
    await axios({
      method: 'get',
      url: `${baseUrl}/expenditureType/v1/getAllExpenditureTypeByPagination/{pageNumber}/{pageSize}?pageNumber=${pageNumber}&pageSize=${pageSize}`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        console.log(data);
        setPageCount(data.totalPages);
        setExpenditureType(data.content);
      })

      .catch(function (error) {
        console.log(error);
      });
  };
  
  const handlePageClick = (data) => {
    setPageNumber(data.selected);
    NewData(pageNumber);
  };
  const handleChange = (e) => {
    setPageSize(e.target.value);
    setValue(e.target.value);
    NewData(pageSize);
  };

  useEffect(() => {
    NewData();
  }, [
    description,
    expenditureTypeId,
    expenditureTypeName, 
    pageSize,
    pageNumber,
  ]);

  const handleRemove = (expenditureTypeId) => {
    axios({
      method: 'delete',
      url: `${baseUrl}/expenditureType/v1/deleteExpenditureTypeById/${expenditureTypeId}`,
      headers,
    })
      .then((res) => {
        if (res.data.responseCode === 200) {
          alert(res.data.message);
          window.location.reload(false);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
        NewData();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const UpdateData = (expenditureTypeId) => {
    axios({
      method: 'get',
      url: `${baseUrl}/expenditureType/v1/getExpenditureTypeByExpenditureTypeId/{expenditureTypeId}?expenditureTypeId=${expenditureTypeId}`,
      headers,
      data: {
        createdBy: {
          userId,
        },
        description,
        expenditureTypeId,
        expenditureTypeName,   
      }
    })
      .then(function (res) {
        let data = res.data;
        // console.log(data.responseCode ===201)
        if (data.responseCode === 201) {
          alert(data.message);
          window.location.reload(false);
        } else if (data.responseCode === 400) {
          alert(data.errorMessage);
        }
        setExpenditureTypeName(res.data.expenditureTypeName);
        setDescription(res.data.description);
        setExpenditureTypeId(expenditureTypeId);
        setOnEdit(true);
        setActiveTab(2);
        // console.log(setFirstName(res.data.firstName))
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const userData = (e) => {
    axios({
      method: 'put',
      url: `${baseUrl}/expenditureType/v1/updateExpenditureType`,
      headers,
      data: {
        createdBy: {
          userId,
        },
        description,
        expenditureTypeId,
        expenditureTypeName,   
      }
    })
      .then(function (res) {
        let data = res.data;
        // console.log(data.responseCode ===201)
        if (data.responseCode === 201) {
          alert(data.message);
          window.location.reload(false);
        } else if (data.responseCode === 400) {
          alert(data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
      setExpenditureTypeName('');
    setDescription('');  
    setExpenditureTypeId('');
    setOnEdit(false);
    setActiveTab(1);
    NewData();
  };

  
  return (
    <>
      <main id="main" class="main">
        <div class="pagetitle">
          <h1>Expenditure Type </h1>
          <nav>
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="/">Home</a>
              </li>
              <li class="breadcrumb-item active">Expenditure Type</li>
            </ol>
          </nav>
        </div>
        <div>
          <hr />
        </div>

        <div className="container-fluid">
          <div className="d-flex justify-content-between align-items-center ">
            <Nav
              tabs
              className="page-header-tab"
              variant="pills"
              style={{ marginBottom: 30 }}
            >
              <Nav.Item>
                <Nav.Link
                  className={classnames({ active: activeTab === 1 })}
                  onClick={() => setActiveTab(1)}
                >
                  List View
                </Nav.Link>
              </Nav.Item>
              <NavItem>
                {onEdit ? (
                  <NavLink
                    className={classnames({ active: activeTab === 2 })}
                    onClick={() => setActiveTab(2)}
                  >
                    Edit
                  </NavLink>
                ) : (
                  <NavLink
                    className={classnames({ active: activeTab === 2 })}
                    onClick={() => setActiveTab(2)}
                  >
                    Add
                  </NavLink>
                )}
              </NavItem>
            </Nav>
          </div>
        </div>

        <div class="container-fluid">
          <div class="row justify-content-between">
            <div class="col-4">
              <select
                value={value}
                onChange={handleChange}
                style={{ border: 'none', color: 'bg-primary' }}
              >
                <option value="10">10 / Pages</option>
                <option value="25">25 / Pages</option>
                <option value="50">50 / Pages</option>
                <option value="100">100 / Pages</option>
              </select>
            </div>
            <div class="col-4">
              <div class="input-group">
                <input
                  style={BarStyling}
                  type="text"
                  placeholder="Search"
                  className="prompt"
                  // value={searchTerm}
                  onChange={(e) => setSearchTerm(e.target.value)}
                />
              </div>
            </div>
          </div>
        </div>

        <div className="container-fluid">
          <TabContent activeTab={activeTab}>
            <TabPane tabId={1} className={classnames(['fade show'])}>
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-body ">
                    <table class="table table-hover bg-gradient-info text-black">
                      <thead class="table-section">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Expenditure Type Name</th>
                          <th scope="col">Description</th>
                          <th className="text-center" scope="col">
                            Created By
                          </th>
                          <th className="text-center" scope="col">
                            Updated By
                          </th>
                          <th scope="col">Inserted Date</th>
                          <th scope="col">Updated Date</th>
                          <th scope="col">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        {expenditureType
                          .filter((val) => {
                            if (searchTerm == '') {
                              return val;
                            } else if (
                              val.expenditureTypeName
                                .toLowerCase()
                                .includes(searchTerm.toLowerCase())
                            ) {
                              return val;
                            }
                          })
                          .map((data, index) => {
                            return (
                              <tr key={data.expenditureTypeId}>
                                <th scope="row">{index + 1}</th>
                                <td>{data.expenditureTypeName}</td>
                                <td>{data.description}</td>
                                <td>{data.createdBy.userName}</td>
                                <td>{data.updatedBy.userName}</td>
                                <td>
                                  {Moment(data.insertedDate).format(
                                    'DD-MM-YYYY'
                                  )}
                                </td>
                                <td>
                                  {Moment(data.updatedDate).format(
                                    'DD-MM-YYYY'
                                  )}
                                </td>
                                <td>
                                  <button
                                    type="button"
                                    className="btn btn-icon btn-sm"
                                    title="Edit"
                                    // onClick={console.log(UpdateData)}
                                    onClick={(e) =>
                                      UpdateData(data.expenditureTypeId, e)
                                    }
                                  >
                                    <i className="fa fa-edit"></i>
                                  </button>
                                  <button
                                    type="button"
                                    className="btn btn-icon btn-sm js-sweetalert"
                                    title="Delete"
                                    data-type="confirm"
                                    onClick={(e) =>
                                      handleRemove(data.expenditureTypeId, e)
                                    }
                                  >
                                    <i className="fas fa-trash-alt text-danger"></i>
                                  </button>
                                </td>
                              </tr>
                            );
                          })}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div>
                <ReactPaginate
                  previousLabel="Previous"
                  nextLabel="Next"
                  breakLabel={'...'}
                  pageCount={pageCount}
                  marginPagesDisplayed={2}
                  pageRangeDisplayed={2}
                  onPageChange={handlePageClick}
                  containerClassName={'pagination justify-content-center'}
                  pageClassName={'page-item'}
                  pageLinkClassName={'page-link'}
                  previousClassName={'page-item'}
                  previousLinkClassName={'page-link'}
                  nextClassName={'page-item'}
                  nextLinkClassName={'page-link'}
                  breakClassName={'page-item'}
                  breakLinkClassName={'page-link'}
                  activeClassName={'active'}
                />
              </div>
            </TabPane>
            <TabPane tabId={2} className={classnames(['fade show'])}>
              <div className="col-lg-12 col-md-12">
                <div class="card">
                  <div class="card-body">
                    <div className="card-header table-section">
                      {/* <h3 className="card-title ">Add Category</h3> */}
                      {onEdit ? (
                        <h3 className="card-title">Update Expenditure Type</h3>
                      ) : (
                        <h3 className="card-title">Add Expenditure Type </h3>
                      )}
                    </div>
                    <Form noValidate>
                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="6"
                          controlId="validationCustom01"
                        >
                          <Form.Label>Expenditure Type Name</Form.Label>
                          <Form.Control
                            required
                            type="text"
                            value={expenditureTypeName}
                            className="form-control"
                            placeholder="Enter Expenditure Type Name"
                            onChange={(e) => setExpenditureTypeName(e.target.value)}
                            // onChange={firstHandler}
                          />
                          {expenditureTypeErr ? (
                            <span style={{ color: 'red', marginLeft: 20 }}>
                              Expenditure Type is required!!
                            </span>
                          ) : null}
                        </Form.Group> 
                      </Row>

                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="12"
                          controlId="validationCustom03"
                        >
                          <Form.Label>Description</Form.Label>
                          <Form.Control
                            as="textarea"
                            rows={3}
                            value={description}
                            className="form-control no-resize"
                            placeholder="Add description"
                            onChange={(e) => setDescription(e.target.value)}
                            // onChange={descHandler}
                          />
                          {descErr ? (
                            <span style={{ color: 'red', marginLeft: 20 }}>
                              Description is required!!
                            </span>
                          ) : null}
                        </Form.Group>
                      </Row>
                     
                     
                    </Form>
                    <div className="col-sm-12">
                      {onEdit ? (
                        <button
                          onClick={(e) => handleSubmit(e.target.value, 1)}
                          // onClick={userData}
                          style={{ marginRight: 20 }}
                          type="submit"
                          className="mr-1 btn btn-primary"
                        >
                          Update
                        </button>
                      ) : (
                        <button
                          onClick={(e) => handleSubmit(e.target.value, 0)}
                          // onClick={postData}
                          style={{ marginRight: 20 }}
                          type="submit"
                          className="mr-1 btn btn-primary"
                        >
                          Submit
                        </button>
                      )}

                      <button
                        type="submit"
                        className="btn btn-outline-secondary btn-default"
                      >
                        Cancel
                      </button>
                    </div>
                  </div>
                </div>
                <div></div>
              </div>
            </TabPane>
          </TabContent>
        </div>
      </main>
    </>
  );
}

import React, { useState, useEffect } from "react";
import {
  NavLink,
  NavItem,
  Nav,
  Row,
  Col,
  Form,
  InputGroup,
} from "react-bootstrap";
import { TabContent, TabPane } from "reactstrap";
import classnames from "classnames";
import Moment from "moment";
import axios from "axios";
import ReactPaginate from "react-paginate";

export default function Product() {
  const [activeTab, setActiveTab] = useState(1);

  const [product, setProduct] = useState([]);
  const [productId, setProductId] = useState("");
  const [productName, setProductName] = useState("");
  const [batchNo, setBatchNo] = useState("");
  const [productBarcode, setProductBarcode] = useState("");
  const [productMrp, setProductMrp] = useState("");
  const [sellingcost, setSellingcost] = useState("");
  const [discount, setDiscount] = useState("");
  const [cgst, setCgst] = useState("");
  const [gst, setGst] = useState("");
  const [sgst, setSgst] = useState("");
  const [specification, setSpecification] = useState("");

  const [category, setCategory] = useState([]);
  const [catvalue, setCatValue] = useState("");
  const [categoryId, setCategoryId] = useState("");
  const [categoryName, setCategoryName] = useState("");

  const [pageSize, setPageSize] = useState(10);
  const [onEdit, setOnEdit] = useState(false);
  const [pageNumber, setPageNumber] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [value, setValue] = useState(0);
  const [searchTerm, setSearchTerm] = useState("");

  const [productErr, setProductErr] = useState(false);
  const [desErr, setDesErr] = useState(false);

  function userHandler(e) {
    let item = e.target.value;
    if (item.length <= 0) {
      setProductErr(true)
    } else {
      setProductErr(false)
    }
    setProductName(item);

    console.log(e.target.value)
  }

  function DesHandler(e) {
    let item = e.target.value;
    if (item.length > 30 || item.length <= 0) {
      setDesErr(true)
    } else {
      setDesErr(false)
    }
    setSpecification(item);

    console.log(e.target.value)
  }
  const handleSubmit = (e, val) => {
    let prod = productName.trim();
    let desc = specification.trim();
    console.log(val);
    if (prod.length > 0) {
      setProductErr(false);
      if (desc.length > 0) {
        setProductErr(false);
        setDesErr(false);
        if (val == 0) {
          postData(e);
        } else {
          userData(e);
        }
      } else {
        setDesErr(true);
      }
    } else {
      setProductErr(true);
    }
  };

  const BarStyling = {
    width: "20rem",
    background: "#F2F1F9",
    border: "#0d6efd",
    padding: "0.5rem",
    marginBottom: 10,
  };

  const user = JSON.parse(sessionStorage.getItem("user"));

  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };
  let userId = user.userId;

  const data = {
      batchNo,
      cgst,
      createdBy: {
        userId,
      },
      discount,
      gst,
      productBarcode,
      productCategoryDto: {
        categoryId,
        categoryName,
      },
      productId,
      productMrp,
      productName,
      sellingcost,
      sgst,
      specification,
    }
  
  const baseUrl = "https://executivetracking.cloudjiffy.net/WaterPlantWeb";

  const postData = (e) => {
    axios({
      method: "post",
      url: `${baseUrl}/product/v1/createProduct`,
      headers,
      data,
    })
      .then(function (res) {
        let data = res.data;
        console.log(data);
        if (data.responseCode === 201) {
          alert(data.message);
          window.location.reload(false)
        } else if (data.responseCode === 400) {
          alert(data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
      
      setProductName("");
      setBatchNo("");
      setProductBarcode("");
      setProductMrp("");
      setSellingcost("");
      setDiscount("");
      setCgst("");
      setGst("");
      setSgst("");
      setSpecification("");
      setCategoryId("");
      setCategoryName("");
      setActiveTab(1);
      NewData();
  };

  const CategoryData = () => {
    axios({
      method: "get",
      url: `${baseUrl}/category/v1/queryAllCategory`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setCategory(data);
      })

      .catch(function (error) {
        console.log(error);
      });
  }

  const handleTypeChange= (e) => {
    setCategoryId(e.target.value);
    setCatValue(e.target.value);
    console.log(categoryId);
  }

  const NewData = async () => {
    await axios({
      method: "get",
      url: `${baseUrl}/product/v1/getAllProductsByPagination/{pageNumber}/{pageSize}?pageNumber=${pageNumber}&pageSize=${pageSize}`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        // console.log("++==", data.content);
        setPageCount(data.totalPages);
        setProduct(data.content);
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  

  const handlePageClick = (data) => {
    setPageNumber(data.selected);
    NewData(pageNumber);
  };
  const handleChange = (e) => {
    setPageSize(e.target.value);
    setValue(e.target.value);
    NewData(pageSize);
  };

  const UpdateData = (productId) => {
    axios({
      method: "get",
      url: `${baseUrl}/product/v1/getProductByProductId/{productId}?productId=${productId}`,
      headers,
      
    }).then(function (res) {
      let data = res.data;
      console.log("data==", data);
      if (data.responseCode === 201) {
        alert(data.message);
        window.location.reload(false)
      } else if (data.responseCode === 400) {
        alert(data.errorMessage);
      }
      setProductName(data.productName);
      setProductId(productId);
      setBatchNo(data.batchNo);
      setProductBarcode(data.productBarcode);
      setSpecification(data.specification);
      setCgst(data.cgst);
      setDiscount(data.discount);
      setGst(data.gst);
      setSgst(data.sgst);
      setProductMrp(data.productMrp);
      setSellingcost(data.sellingcost);
      setCategoryId(data.categoryId);
      setCategoryName(data.categoryName);
      setOnEdit(true);
      setActiveTab(2);
    });
    // .catch(function (err) {
    //   console.log(err);
    // });
  };

  const userData = (e) => {
    console.log("userdata==",data)
    axios({
      method: "put",
      url: `${baseUrl}/product/v1/updateProduct`,
      headers,
      data,
    })
      .then(function (res) {
        let data = res.data;
        if (data.responseCode === 201) {
          alert(data.message);
          window.location.reload(false)
        } else if (data.responseCode === 400) {
          alert(data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
      setProductName("");
      setBatchNo("");
      setProductBarcode("");
      setProductMrp("");
      setSellingcost("");
      setDiscount("");
      setCgst("");
      setGst("");
      setSgst("");
      setSpecification("");
      setCategoryId("");
      setCategoryName("");
      setOnEdit(false);
      setActiveTab(1);
      NewData("");
  };
  const handleRemove = (productId, e) => {
    axios({
      method: "delete",
      url: `${baseUrl}/product/v1/deleteProductById/${productId}`,
      headers,
    })
      .then(function (res) {
        let data = res.data;
        if (data.responseCode === 200) {
          alert(data.message);
          window.location.reload(false)
        } else if (data.responseCode === 400) {
          alert(data.errorMessage);
        }
        NewData();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const totalDiscountChange = (e) =>{
    setDiscount(e);
    let TotalPrice = productMrp-(productMrp*e/100).toFixed(2)
    let TotalGst = (18);
    let TotalCGST= (18/2);
    setGst(TotalGst);
    setCgst(TotalCGST);
    setSgst(TotalCGST);
    setProductMrp(TotalPrice);
    setSellingcost(TotalPrice);
  }

  
  useEffect(() => {
    NewData();
    CategoryData();
  }, [
    batchNo,
    cgst,
    discount,
    gst,
    productBarcode,
    categoryId,
    categoryName,
    productId,
    productMrp,
    productName,
    sellingcost,
    sgst,
    specification,
    pageSize,
    pageNumber,
  ]);

  return (
    <>
      <main id="main" class="main">
        <div class="pagetitle">
          <h1>Product</h1>
          <nav>
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="/">Home</a>
              </li>

              <li class="breadcrumb-item active">Product</li>
            </ol>
          </nav>
        </div>
        <div>
          <hr />
        </div>

        <div className="container-fluid">
          <div className="d-flex justify-content-between align-items-center ">
            <Nav
              tabs
              className="page-header-tab"
              variant="pills"
              style={{ marginBottom: 30 }}
              
            >
              <Nav.Item>
                <Nav.Link
                  className={classnames({ active: activeTab === 1 })}
                  onClick={() => setActiveTab(1)}
                >
                  List View
                </Nav.Link>
              </Nav.Item>
              <NavItem>
                {onEdit ? (
                  <NavLink
                    className={classnames({ active: activeTab === 2 })}
                    onClick={() => setActiveTab(2)}
                  >
                    Edit
                  </NavLink>
                ) : (
                  <NavLink
                    className={classnames({ active: activeTab === 2 })}
                    onClick={() => setActiveTab(2)}
                  >
                    Add 
                  </NavLink>
                  
                )}
              </NavItem>
            </Nav>
          </div>
        </div>

        <div class="container-fluid">
          <div class="row justify-content-between">
            <div class="col-4">
              <select
                value={value}
                onChange={handleChange}
                style={{ border: "none", color: "bg-primary" }}
              >
                <option value="10">10 / Pages</option>
                <option value="25">25 / Pages</option>
                <option value="50">50 / Pages</option>
                <option value="100">100 / Pages</option>
              </select>
            </div>
            <div class="col-4">
              <div class="input-group">
                <input
                  style={BarStyling}
                  type="text"
                  placeholder="Search"
                  className="prompt"
                  // value={searchTerm}
                  onChange={(e) => setSearchTerm(e.target.value)}
                />
              </div>
            </div>
          </div>
        </div>

        <div className="container-fluid">
          <TabContent activeTab={activeTab}>
            <TabPane tabId={1} className={classnames(["fade show"])}>
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-body ">
                    <table class="table table-hover bg-gradient-info text-black">
                      <thead class="table-section">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Product Name</th>
                          <th scope="col">Batch Number</th>
                          <td scope="col">Product Barcode</td>
                          <th scope="col">Product Mrp ( <i class="fa-solid fa-indian-rupee-sign"></i> )</th>
                          <th scope="col">Discount</th>
                          <th scope="col">GST</th>
                          <th scope="col">CGST</th>
                          <th scope="col">SGST</th>
                          <th scope="col">Selling Cost (<i class="fa-solid fa-indian-rupee-sign"></i>)</th>
                          <th scope="col">Specification</th>
                          <th className="text-center" scope="col">
                            Created By
                          </th>
                          <th className="text-center" scope="col">
                            Updated By
                          </th>
                          <th scope="col">Inserted Date</th>
                          <th scope="col">Updated Date</th>
                          <th scope="col">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        {product
                          .filter((val) => {
                            if (searchTerm == "") {
                              return val;
                            } else if (
                              val.productName
                                .toLowerCase()
                                .includes(searchTerm.toLowerCase())
                            ) {
                              return val;
                            }
                          })
                          .map((data, index) => {
                            return (
                              <tr key={data.productId}>
                                <th scope="row">{index + 1}</th>
                                <td>{data.productName}</td>
                                <td>{data.batchNo}</td>
                                <td>{data.productBarcode}</td>
                                <td><i class="fa-solid fa-indian-rupee-sign"></i>. {data.productMrp}</td>
                                <td>{data.discount}%</td>
                                <td>{data.gst}%</td>
                                <td>{data.cgst}%</td>
                                <td>{data.sgst}%</td>
                                <td><i class="fa-solid fa-indian-rupee-sign"></i>. {data.sellingcost}</td>
                                <td>{data.specification}</td>  
                                <td>{data.createdBy.userName}</td>
                                <td>{data.updatedBy.userName}</td>

                                <td>
                                  {Moment(data.insertedDate).format(
                                    "DD-MM-YYYY"
                                  )}
                                </td>
                                <td>
                                  {Moment(data.updatedDate).format(
                                    "DD-MM-YYYY"
                                  )}
                                </td>
                                <td>
                                  <button
                                    type="button"
                                    className="btn btn-icon btn-sm"
                                    title="Edit"
                                    onClick={(e) =>
                                      UpdateData(data.productId, e)
                                    }

                                  >
                                    <i className="fa fa-edit"></i>
                                  </button>
                                  <button
                                    type="button"
                                    className="btn btn-icon btn-sm js-sweetalert"
                                    title="Delete"
                                    data-type="confirm"
                                    onClick={(e) =>
                                      handleRemove(data.productId,  e)
                                    }
                                  >
                                    <i className="fas fa-trash-alt text-danger"></i>
                                  </button>
                                </td>
                              </tr>
                            );
                          })}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div>
                <ReactPaginate
                  previousLabel="Previous"
                  nextLabel="Next"
                  breakLabel={"..."}
                  pageCount={pageCount}
                  marginPagesDisplayed={2}
                  pageRangeDisplayed={2}
                  onPageChange={handlePageClick}
                  containerClassName={"pagination justify-content-center"}
                  pageClassName={"page-item"}
                  pageLinkClassName={"page-link"}
                  previousClassName={"page-item"}
                  previousLinkClassName={"page-link"}
                  nextClassName={"page-item"}
                  nextLinkClassName={"page-link"}
                  breakClassName={"page-item"}
                  breakLinkClassName={"page-link"}
                  activeClassName={"active"}
                />
              </div>
            </TabPane>
            <TabPane tabId={2} className={classnames(["fade show"])}>
              <div className="col-lg-12 col-md-12">
                <div class="card">
                  <div class="card-body">
                    <div className="card-header table-section">
                      {onEdit ? (
                        <h3 className="card-title">Update Product</h3>
                      ) : (
                        <h3 className="card-title">Add Product</h3>
                      )}
                    </div>
                    <Form noValidate>
                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="4"
                          controlId="validationCustom01"
                        >
                          <Form.Label>Product Name</Form.Label>
                          <Form.Control
                            required
                            type="text"
                            placeholder="Add Product Name"
                            value={productName}
                            // onChange={(e) => setProductName(e.target.value)}
                            onChange={userHandler}
                          />
                          {productErr ? <span style={{ color: "red", marginLeft: 20 }}>Product Name is required!!</span> : null}
                        </Form.Group>
                        <Form.Group
                          as={Col}
                          md="4"
                          controlId="validationCustom02"
                        >
                          <Form.Label>Batch Number</Form.Label>
                          <Form.Control
                            required
                            type="number"
                            value={batchNo}
                            placeholder="Add Batch No"
                            onChange={(e) => setBatchNo(e.target.value)}
                          />
                        </Form.Group>
                        <Form.Group
                          as={Col}
                          md="4"
                          controlId="validationCustomUsername"
                        >
                          <Form.Label>Barcode</Form.Label>
                          <InputGroup hasValidation>
                            <Form.Control
                              value={productBarcode}
                              className="form-control"
                              placeholder="Add Product Barcode"
                              onChange={(e) => setProductBarcode(e.target.value)}

                              required
                            />
                          </InputGroup>
                        </Form.Group>
                      </Row>
                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="4"
                          controlId="validationCustom01"
                        >
                          <Form.Label>Product MRP</Form.Label>
                          <Form.Control
                            required
                            type="number"
                            value={productMrp}
                            className="form-control"
                            placeholder="Add Product Mrp"
                            onChange={(e) => setProductMrp(e.target.value)}
                            // onChange={totalDiscountChange}
                          />
                        </Form.Group>
                        <Form.Group
                          as={Col}
                          md="4"
                          controlId="validationCustom02"
                        >
                          <Form.Label>Selling Cost </Form.Label>
                          <Form.Control
                            required
                            value={sellingcost}
                            type="number"
                            className="form-control"
                            placeholder="Add Product SellingCost"
                            onChange={(e) => setSellingcost(e.target.value)}
                          />
                        </Form.Group>
                        <Form.Group
                          as={Col}
                          md="4"
                          controlId="validationCustomUsername"
                        >
                          <Form.Label>Discount</Form.Label>
                          <InputGroup hasValidation>
                            <Form.Control
                              value={discount}
                              type="number"
                              className="form-control"
                              placeholder="Add Product Discount"
                              // onChange={(e) => setDiscount(e.target.value)}
                              onChange={(e)=>totalDiscountChange(e.target.value)}
                              required
                            />
                          </InputGroup>
                        </Form.Group>
                      </Row>
                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="4"
                          controlId="validationCustom01"
                        >
                          <Form.Label>GST</Form.Label>
                          <Form.Control
                            required
                            type="number"
                            value={gst}
                            className="form-control"
                            placeholder="Add Product Gst"
                            onChange={(e) => setGst(e.target.value)}
                          />
                        </Form.Group>
                        <Form.Group
                          as={Col}
                          md="4"
                          controlId="validationCustom02"
                        >
                          <Form.Label>CGST </Form.Label>
                          <Form.Control
                            required
                            value={cgst}
                            type="number"
                            className="form-control"
                            placeholder="Add Product Cgst"
                            onChange={(e) => setCgst(e.target.value)}
                          />
                        </Form.Group>
                        <Form.Group
                          as={Col}
                          md="4"
                          controlId="validationCustomUsername"
                        >
                          <Form.Label>SGST</Form.Label>
                          <InputGroup hasValidation>
                            <Form.Control
                              value={sgst}
                              type="number"
                              className="form-control"
                              placeholder="Add Product Sgst"
                              onChange={(e) => setSgst(e.target.value)}
                              required
                            />
                          </InputGroup>
                        </Form.Group>

                        <Form.Group as={Col}  md="4" controlId="formGridState">
                          <Form.Label>Category</Form.Label>
                          <Form.Select catvalue={value}
                           onChange={handleTypeChange}>
                            <option>Select a Category</option>
                            {category.map((data) => {
                            return (
                              <option value={data.categoryId}>
                                {data.categoryName}
                              </option>
                            );
                          })}
                          </Form.Select>
                        </Form.Group>
                        
                      </Row>

                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="12"
                          controlId="validationCustom03"
                        >
                          <Form.Label>Specification</Form.Label>
                          <Form.Control
                            as="textarea"
                            rows={3}
                            value={specification}
                            className="form-control no-resize"
                            placeholder="Add Specification"
                            // onChange={(e) => setSpecification(e.target.value)}
                            onChange={DesHandler}
                            required 
                            />
                          {desErr ? <span style={{ color: "red" }}>Specification is required!!</span> : null}
                        </Form.Group>
                      </Row>
                    </Form>


                    <div className="col-sm-12">
                      {onEdit ? (
                        <button
                          // onClick={userData}
                          onClick={(e) => handleSubmit(e.target.value, 1)}
                          style={{ marginRight: 20 }}
                          type="submit"
                          className="mr-1 btn btn-primary"
                        >
                          Update
                        </button>
                      ) : (
                        <button
                          onClick={(e) => handleSubmit(e.target.value, 0)}
                          // onClick={postData}
                          style={{ marginRight: 20 }}
                          type="submit"
                          className="mr-1 btn btn-primary"
                        >
                          Submit
                        </button>
                      )}

                      <button
                        type="submit"
                        className="btn btn-outline-secondary btn-default"
                      >
                        Cancel
                      </button>
                    </div>
                  </div>
                  <div></div>
                </div>
              </div>
            </TabPane>
          </TabContent>
        </div>
      </main>
    </>
  );
}

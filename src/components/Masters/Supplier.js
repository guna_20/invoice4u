import React, { useState, useEffect } from "react";
import {
  NavLink,
  NavItem,
  Nav,
  DropdownButton,
  DropdownItem,
  Dropdown,
  Row,
  Col,
  FormGroup,
  Form,
  FormLabel,
  FormControl,
  InputGroup,
  Button,
} from "react-bootstrap";
import { TabContent, TabPane } from "reactstrap";
import Pagination from "react-bootstrap/Pagination";
import classnames from "classnames";
import Moment from "moment";
import axios from "axios";
import ReactPaginate from "react-paginate";
import "react-phone-number-input/style.css";
import PhoneInput from "react-phone-number-input";

export default function Supplier() {
  const [activeTab, setActiveTab] = useState(1);
  const [supplierName, setSupplierName] = useState("");
  const [supplier, setSupplier] = useState([]);
  const [supplierId, setSupplierId] = useState([]);
  const [address, setAddress] = useState("");
  const [bankAcNo, setBankAcNo] = useState("");
  const [gstNo, setGstNo] = useState("");
  const [ifsc, setIsfc] = useState("");
  const [mobileNumber, setMobileNumber] = useState("");
  const [description, setDescription] = useState("");
  const [pageSize, setPageSize] = useState(10);
  const [onEdit, setOnEdit] = useState(false);
  const [pageNumber, setPageNumber] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [value, setValue] = useState(0);
  const [searchTerm, setSearchTerm] = useState("");

  const [supplierErr, setSupplierErr] = useState(false);
  const [desErr, setDesErr] = useState(false);

  function userHandler(e) {
    let item = e.target.value;
    if (item.length <= 0) {
      setSupplierErr(true)
    } else {
      setSupplierErr(false)
    }
    setSupplierName(item);

    console.log(e.target.value)
  }

  function DesHandler(e) {
    let item = e.target.value;
    if (item.length > 30 || item.length <= 0) {
      // console.log(setDesErr)
      setDesErr(true)
    } else {
      setDesErr(false)
    }
    setDescription(item);

    console.log(e.target.value)
  }
  const handleSubmit = (e, val) => {
    let supply = supplierName.trim();
    let desc = description.trim();
  
    if (supply.length > 0) {
      setSupplierErr(false);
      if (desc.length > 0) {
        setSupplierErr(false);
        setDesErr(false);
        if (val == 0) {
          postData(e);
        } else {
          userData(e);
        }
      } else {
        setDesErr(true);
      }
    } else {
      setSupplierErr(true);
    }
  };

  const BarStyling = {
    width: "20rem",
    background: "#F2F1F9",
    border: "#0d6efd",
    padding: "0.5rem",
    marginBottom: 10,
  };
  const user = JSON.parse(sessionStorage.getItem("user"));
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };

  let userId = user.userId;

  const data = {
    address,
    bankAcNo,
    createdBy: {
      userId,
    },
    description,
    gstNo,
    ifsc,
    mobileNumber,
    supplierName,
  };

  const baseUrl = "https://executivetracking.cloudjiffy.net/WaterPlantWeb";

  const postData = (e) => {
    // e.preventDefault();
    axios({
      method: "post",
      url: `${baseUrl}/supplier/v1/createMaterialSupplier`,
      headers,
      data,
    })
      .then(function (res) {
        let data = res.data;
        if (data.responseCode === 201) {
          alert(data.message);
          window.location.reload(false)
        } else if (data.responseCode === 400) {
          alert(data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setSupplierName("");
    setDescription("");
    setAddress("");
    setBankAcNo("");
    setGstNo("");
    setIsfc("");
    setMobileNumber("");
    setActiveTab(1);
    NewData();
  };

  const NewData = async (currentPage) => {
    await axios({
      method: "get",
      url: `${baseUrl}/supplier/v1/getAllSuppliersByPagination/{pageNumber}/{pageSize}?pageNumber=${pageNumber}&pageSize=${pageSize}`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        // console.log(res.data.content);
        let data = res.data;
        setPageCount(data.totalPages);
        setSupplier(data.content);
      })

      .catch(function (error) {
        console.log(error);
      });
  };
  useEffect(() => {
    NewData();
  }, [
    address,
    bankAcNo,
    description,
    gstNo,
    ifsc,
    mobileNumber,
    supplierName,
    pageSize,
    pageNumber,
  ]);

  const handlePageClick = (data) => {
    // console.log(setPageNumber)
    setPageNumber(data.selected);
    NewData(pageNumber);
  };
  const handleChange = (e) => {
    setPageSize(e.target.value);
    setValue(e.target.value);
    NewData(pageSize);
  };

  const UpdateData = (supplierId, e) => {
    axios({
      method: "get",
      url: `${baseUrl}/supplier/v1/getMaterialSupplierBySupplierId/{supplierId}?supplierId=${supplierId}`,
      headers,
      data: {
        address,
        bankAcNo,
        updatedBy: {
          userId,
        },
        description,
        gstNo,
        ifsc,
        mobileNumber,
        supplierId,
        supplierName,
      },
    }).then(function (res) {
      let data = res.data;
      if (data.responseCode === 201) {
        alert(data.message);
        window.location.reload(false)
      } else if (data.responseCode === 400) {
        alert(data.errorMessage);
      }
      setSupplierName(data.supplierName);
      setDescription(data.description);
      setAddress(data.address);
      setBankAcNo(data.bankAcNo);
      setGstNo(data.gstNo);
      setIsfc(data.ifsc);
      setMobileNumber(data.mobileNumber);
      setOnEdit(true);
      setActiveTab(2);
      setSupplierId(data.supplierId);
      // console.log(res.data.data.supplierName);
    });
    // .catch(function (err) {
    //   console.log(err);
    // });
  };

  const userData = (e) => {
    e.preventDefault();
    axios({
      method: "put",
      url: `${baseUrl}/supplier/v1/updateMaterialSupplier`,
      headers,
      data: {
        address,
        bankAcNo,
        updatedBy: {
          userId,
        },
        description,
        gstNo,
        ifsc,
        mobileNumber,
        supplierId,
        supplierName,
      },
    })
      .then(function (res) {
        let data = res.data;
        if (data.responseCode === 201) {
          alert(data.message);
          window.location.reload(false)
        } else if (data.responseCode === 400) {
          alert(data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setSupplierName("");
    setDescription("");
    setAddress("");
    setBankAcNo("");
    setGstNo("");
    setIsfc("");
    setMobileNumber("");
    setOnEdit(false);
    setActiveTab(1);
    NewData("");
    // console.log(supplierName)
  };

  const handleRemove = (supplierId, e) => {
    axios({
      method: "delete",
      url: `${baseUrl}/supplier/v1/deleteSupplierBySupplierId/${supplierId}`,
      headers,
    })
      .then(function (res) {
        let data = res.data;
        // console.log(data.responseCode)
        if (data.responseCode === 200) {
          alert(data.message);
          window.location.reload(false)
        } else if (data.responseCode === 400) {
          alert(data.errorMessage);
        }
        NewData();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <>
      <main id="main" class="main">
        <div class="pagetitle">
          <h1>Material Supplier</h1>
          <nav>
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="/">Home</a>
              </li>

              <li class="breadcrumb-item active">Material Supplier</li>
            </ol>
          </nav>
        </div>
        <div>
          <hr />
        </div>

        <div className="container-fluid">
          <div className="d-flex justify-content-between align-items-center ">
            <Nav
              tabs
              className="page-header-tab"
              variant="pills"
              style={{ marginBottom: 30 }}
            >
              <Nav.Item>
                <Nav.Link
                  className={classnames({ active: activeTab === 1 })}
                  onClick={() => setActiveTab(1)}
                >
                  List View
                </Nav.Link>
              </Nav.Item>
              <NavItem>
                {onEdit ? (
                  <NavLink
                    className={classnames({ active: activeTab === 2 })}
                    onClick={() => setActiveTab(2)}
                  >
                    Edit
                  </NavLink>
                ) : (
                  <NavLink
                    className={classnames({ active: activeTab === 2 })}
                    onClick={() => setActiveTab(2)}
                  >
                    Add
                  </NavLink>
                )}
              </NavItem>
            </Nav>
          </div>
        </div>

        <div class="container-fluid">
          <div class="row justify-content-between">
            <div class="col-4">
              <select
                value={value}
                onChange={handleChange}
                style={{ border: "none", color: "bg-primary" }}
              >
                <option value="10">10 / Pages</option>
                <option value="25">25 / Pages</option>
                <option value="50">50 / Pages</option>
                <option value="100">100 / Pages</option>
              </select>
            </div>
            <div class="col-4">
              <div class="input-group">
                <input
                  style={BarStyling}
                  type="text"
                  placeholder="Search"
                  className="prompt"
                  // value={searchTerm}
                  onChange={(e) => setSearchTerm(e.target.value)}
                />
              </div>
            </div>
          </div>
        </div>

        <div className="container-fluid">
          <TabContent activeTab={activeTab}>
            <TabPane tabId={1} className={classnames(["fade show"])}>
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-body ">
                    <table class="table table-hover bg-gradient-info text-black">
                      <thead class="table-section">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Supplier Name</th>
                          <th scope="col">MobileNumber</th>
                          <th scope="col">Bank Account Number</th>
                          <th scope="col">GST NUmber</th>
                          <th scope="col">IFSC Code</th>
                          <th scope="col">Description</th>
                          <th scope="col">Address</th>
                          <th className="text-center" scope="col">
                            Created By
                          </th>
                          <th className="text-center" scope="col">
                            Updated By
                          </th>
                          <th scope="col">Inserted Date</th>
                          <th scope="col">Updated Date</th>
                          <th scope="col">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        {supplier
                          .filter((val) => {
                            if (searchTerm == "") {
                              return val;
                            } else if (
                              val.supplierName
                                .toLowerCase()
                                .includes(searchTerm.toLowerCase())
                            ) {
                              return val;
                            }
                          })
                          .map((data, index) => {
                            return (
                              <tr key={data.supplierId}>
                                <th scope="row">{index + 1}</th>
                                <td>{data.supplierName}</td>
                                <td>{data.mobileNumber}</td>
                                <td>{data.bankAcNo}</td>
                                <td>{data.gstNo}</td>
                                <td>{data.ifsc}</td>
                                <td>{data.description}</td>
                                <td>{data.address}</td>
                                <td>{data.createdBy.userName}</td>
                                <td>{data.updatedBy.userName}</td>
                                <td>
                                  {Moment(data.insertedDate).format(
                                    "DD-MM-YYYY"
                                  )}
                                </td>
                                <td>
                                  {Moment(data.updatedDate).format(
                                    "DD-MM-YYYY"
                                  )}
                                </td>
                                <td>
                                  <button
                                    type="button"
                                    className="btn btn-icon btn-sm"
                                    title="Edit"
                                    onClick={(e) =>
                                      UpdateData(data.supplierId, e)
                                    }
                                  >
                                    <i className="fa fa-edit"></i>
                                  </button>
                                  <button
                                    type="button"
                                    className="btn btn-icon btn-sm js-sweetalert"
                                    title="Delete"
                                    data-type="confirm"
                                    onClick={(e) =>
                                      handleRemove(data.supplierId, e)
                                    }
                                  >
                                    <i className="fas fa-trash-alt text-danger"></i>
                                  </button>
                                </td>
                              </tr>
                            );
                          })}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div>
                <ReactPaginate
                  previousLabel="Previous"
                  nextLabel="Next"
                  breakLabel={"..."}
                  pageCount={pageCount}
                  marginPagesDisplayed={2}
                  pageRangeDisplayed={2}
                  onPageChange={handlePageClick}
                  containerClassName={"pagination justify-content-center"}
                  pageClassName={"page-item"}
                  pageLinkClassName={"page-link"}
                  previousClassName={"page-item"}
                  previousLinkClassName={"page-link"}
                  nextClassName={"page-item"}
                  nextLinkClassName={"page-link"}
                  breakClassName={"page-item"}
                  breakLinkClassName={"page-link"}
                  activeClassName={"active"}
                />
              </div>
            </TabPane>
            <TabPane tabId={2} className={classnames(["fade show"])}>
              <div className="col-lg-12 col-md-12">
                <div class="card">
                  <div class="card-body">
                    <div className="card-header table-section">
                      {onEdit ? (
                        <h3 className="card-title">Update Material Supplier</h3>
                      ) : (
                        <h3 className="card-title">Add Material Supplier</h3>
                      )}
                    </div>
                    <Form noValidate>
                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="6"
                          controlId="validationCustom01"
                        >
                          <Form.Label>Material Supplier</Form.Label>
                          <Form.Control
                            required
                            value={supplierName}
                          className="form-control"
                          placeholder="Supplier Name"
                          // onChange={(e) => setSupplierName(e.target.value)}
                          onChange={userHandler}
                          />
                          {supplierErr ? <span style={{ color: "red", marginLeft: 20 }}>Material Supplier Name is required!!</span> : null}  
                        </Form.Group>
                        <Form.Group
                          as={Col}
                          md="3"
                          controlId="validationCustom03"
                        >
                          <Form.Label>Phone Number</Form.Label>
                          <PhoneInput
                            placeholder="Enter phone number"
                            value={mobileNumber}
                            onChange={setMobileNumber}
                          />
                        </Form.Group>
                        
                      </Row>
                      <Row>
                        <Form.Group
                          as={Col}
                          md="4"
                          controlId="validationCustom02"
                        >
                          <Form.Label>Bank Account Number</Form.Label>
                          <Form.Control
                            required
                            value={bankAcNo}
                            type="tel"
                            pattern="[0-9]*"
                            className="form-control"
                            placeholder="Supplier Bank Account No"
                            onChange={(e) => setBankAcNo(e.target.value)}
                          />
                        </Form.Group>
                        <Form.Group
                          as={Col}
                          md="4"
                          controlId="validationCustomUsername"
                        >
                          <Form.Label>GST Number</Form.Label>
                          <InputGroup hasValidation>
                            <Form.Control
                               value={gstNo}
                                type="number"
                               className="form-control"
                               placeholder="GST Number"
                               onChange={(e) => setGstNo(e.target.value)}
                             
                              required
                            />
                          </InputGroup>
                        </Form.Group>
                        <Form.Group
                          as={Col}
                          md="4"
                          controlId="validationCustomUsername"
                        >
                          <Form.Label>IFSC Code </Form.Label>
                          <InputGroup hasValidation>
                            <Form.Control
                              value={ifsc}
                              type="text"
                              className="form-control"
                              placeholder="IFSC Code"
                              onChange={(e) => setIsfc(e.target.value)}

                              required
                            />
                          </InputGroup>
                        </Form.Group>

                      </Row>
                        
                      
                      
                      <Row classname="mb-3">
                        <Form.Group
                          as={Col}
                          md="12"
                          controlId="validationCustom03"
                        >
                          <Form.Label>Address</Form.Label>
                          <Form.Control
                            as="textarea"
                            rows={3}
                            value={address}
                            className="form-control no-resize"
                            placeholder="Add Address"
                            onChange={(e) => setAddress(e.target.value)}
                            required
                          />
                        </Form.Group>
                        
                      </Row>
                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="12"
                          controlId="validationCustom03"
                        >
                          <Form.Label>Description</Form.Label>
                          <Form.Control
                           as="textarea"
                           rows={3}
                            value={description}
                            className="form-control no-resize"
                            placeholder="Add description"
                            // onChange={(e) => setDescription(e.target.value)}
                            onChange={DesHandler}
                            required
                          />
                          {desErr ? <span style={{ color: "red" }}>Description is required!!</span> : null}
                        </Form.Group>  
                      </Row>
                    </Form>
                    <div className="col-sm-12">
                      {onEdit ? (
                        <button
                          // onClick={userData}
                          onClick={(e) => handleSubmit(e.target.value, 1)}
                          style={{ marginRight: 20 }}
                          type="submit"
                          className="mr-1 btn btn-primary"
                        >
                          Update
                        </button>
                      ) : (
                        <button
                          // onClick={postData}
                          onClick={(e) => handleSubmit(e.target.value, 0)}
                          style={{ marginRight: 20 }}
                          type="submit"
                          className="mr-1 btn btn-primary"
                        >
                          Submit
                        </button>
                      )}

                      <button
                        type="submit"
                        className="btn btn-outline-secondary btn-default"
                      >
                        Cancel
                      </button>
                    </div>
                  </div>
                </div>
                <div>
                  
                  
                </div>
              </div>
            </TabPane>
          </TabContent>
        </div>
      </main>
    </>
  );
}

import React, { useState, useEffect } from "react";
import { NavLink, NavItem, Nav, Form, InputGroup } from "react-bootstrap";
import { TabContent, TabPane } from "reactstrap";
import classnames from "classnames";
import Moment from "moment";
import axios from "axios";
import ReactPaginate from "react-paginate";
import Swal from "sweetalert2";

export default function Category() {
  const [activeTab, setActiveTab] = useState(1);
  const [category, setCategory] = useState([]);
  const [categoryId, setCategoryId] = useState("");
  const [categoryName, setCategoryName] = useState("");
  const [description, setDescription] = useState("");
  const [pageSize, setPageSize] = useState(10);
  const [onEdit, setOnEdit] = useState(false);
  const [pageNumber, setPageNumber] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [value, setValue] = useState(0);
  const [searchTerm, setSearchTerm] = useState("");
  const [validated, setValidated] = useState(false);

  const [categoryErr, setCategoryErr] = useState(false);
  const [desErr, setDesErr] = useState(false);

  function userHandler(e) {
    let item = e.target.value;
    if (item.length <= 0) {
      setCategoryErr(true);
    } else {
      setCategoryErr(false);
    }
    setCategoryName(item);

    console.log(e.target.value);
  }

  function DesHandler(e) {
    let item = e.target.value;
    if (item.length > 30 || item.length <= 0) {
      // console.log(setDesErr)
      setDesErr(true);
    } else {
      setDesErr(false);
    }
    setDescription(item);

    console.log(e.target.value);
  }
  const handleSubmit = (e, val) => {
    let cato = categoryName.trim();
    let desc = description.trim();
    console.log(val);
    if (cato.length > 0) {
      setCategoryErr(false);
      if (desc.length > 0) {
        setCategoryErr(false);
        setDesErr(false);
        if (val == 0) {
          postData(e);
        } else {
          userData(e);
        }
      } else {
        setDesErr(true);
      }
    } else {
      setCategoryErr(true);
    }
  };

  const BarStyling = {
    width: "20rem",
    background: "#F2F1F9",
    border: "#0d6efd",
    padding: "0.5rem",
    marginBottom: 10,
  };
  const user = JSON.parse(sessionStorage.getItem("user"));

  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };
  let userId = user.userId;

  const data = {
    categoryName,
    createdBy: {
      userId,
    },
    description,
  };

  const baseUrl = "https://executivetracking.cloudjiffy.net/WaterPlantWeb";

  const postData = (e) => {
    axios({
      method: "post",
      url: `${baseUrl}/category/v1/createCategory`,
      headers,
      data,
    })
      .then(function (res) {
        let data = res.data;

        if (data.responseCode === 201) {
          alert(data.message);
          window.location.reload(false);
        } else if (data.responseCode === 400) {
          alert(data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setDescription("");
    setCategoryName("");
    setActiveTab(1);
    NewData();
  };

  const NewData = async () => {
    await axios({
      method: "get",
      url: `${baseUrl}/category/v1/getAllCategoryByPagination/{pageNumber}/{pageSize}?pageNumber=${pageNumber}&pageSize=${pageSize}`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setPageCount(data.totalPages);
        setCategory(data.content);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => {
    NewData();
  }, [categoryName, description, pageSize, pageNumber]);

  const handlePageClick = (data) => {
    setPageNumber(data.selected);
    NewData(pageNumber);
  };
  const handleChange = (e) => {
    setPageSize(e.target.value);
    setValue(e.target.value);
    NewData(pageSize);
  };

  const UpdateData = (categoryId) => {
    axios({
      method: "get",
      url: `${baseUrl}/category/v1/getCategoryByCategoryId/{categoryId}?categoryId=${categoryId}`,
      headers,
      data: {
        categoryId,
        categoryName,
        description,
        updatedBy: {
          userId,
        },
      },
    })
      .then(function (res) {
        let data = res.data;
        setCategoryName(data.categoryName);
        setCategoryId(data.categoryId);
        setDescription(data.description);
        setOnEdit(true);
        setActiveTab(2);
      })
      .catch(function (err) {
        console.log(err);
      });
  };

  const userData = (e) => {
    axios({
      method: "put",
      url: `${baseUrl}/category/v1/updateCategory`,
      headers,
      data: {
        updatedBy: {
          userId,
        },
        categoryId,
        description,
        categoryName,
      },
    })
      .then(function (res) {
        let data = res.data;
        if (data.responseCode === 201) {
          Swal.fire({
            title: "Success ",
            type: "success update",
            text: alert(data.message),
          });
          // alert(data.message);
          // window.location.reload(false)
        } else if (data.responseCode === 400) {
          Swal.fire({
            title: "Success ",
            type: "success update",
            text: data.errorMessage,
          });
          // alert(data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setCategoryName("");
    setCategoryId("");
    setDescription("");
    setOnEdit(false);
    setActiveTab(1);
    NewData("");
  };
  const handleRemove = (categoryId, e) => {
    axios({
      method: "delete",
      url: `${baseUrl}/category/v1/deleteCategoryById/${categoryId}`,
      headers,
    })
      .then(function (res) {
        let data = res.data;
        // console.log(data.responseCode)
        // if (data.responseCode === 200) {
        //   alert(data.message);
        //   window.location.reload(false)
        // } else if (data.responseCode === 400) {
        //   alert(data.errorMessage);
        // }
        Swal.fire({
          title: "Success",
          type: "success",
          text: "Your have Successfully deleted.",
        });
        NewData();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <>
      <main id="main" class="main">
        <div class="pagetitle">
          <h1>Category</h1>
          <nav>
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="/">Home</a>
              </li>
              <li class="breadcrumb-item active">Category</li>
            </ol>
          </nav>
        </div>
        <div>
          <hr />
        </div>

        <div className="container-fluid">
          <div className="d-flex justify-content-between align-items-center ">
            <Nav
              tabs
              className="page-header-tab"
              variant="pills"
              style={{ marginBottom: 30 }}
            >
              <Nav.Item>
                <Nav.Link
                  className={classnames({ active: activeTab === 1 })}
                  onClick={() => setActiveTab(1)}
                >
                  List View
                </Nav.Link>
              </Nav.Item>
              <NavItem>
                {onEdit ? (
                  <NavLink
                    className={classnames({ active: activeTab === 2 })}
                    onClick={() => setActiveTab(2)}
                  >
                    Edit
                  </NavLink>
                ) : (
                  <NavLink
                    className={classnames({ active: activeTab === 2 })}
                    onClick={() => setActiveTab(2)}
                  >
                    Add
                  </NavLink>
                )}
              </NavItem>
            </Nav>
          </div>
        </div>

        <div class="container-fluid">
          <div class="row justify-content-between">
            <div class="col-4">
              <select
                value={value}
                onChange={handleChange}
                style={{ border: "none", color: "bg-primary" }}
              >
                <option value="10">10 / Pages</option>
                <option value="25">25 / Pages</option>
                <option value="50">50 / Pages</option>
                <option value="100">100 / Pages</option>
              </select>
            </div>

            <div class="col-4">
              <div class="input-group">
                <input
                  style={BarStyling}
                  type="text"
                  placeholder="Search"
                  className="prompt"
                  onChange={(e) => setSearchTerm(e.target.value)}
                />
              </div>
            </div>
          </div>
        </div>

        <div className="container-fluid">
          <TabContent activeTab={activeTab}>
            <TabPane tabId={1} className={classnames(["fade show"])}>
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-body ">
                    <table class="table table-hover bg-gradient-info text-black">
                      <thead class="table-section">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Category Name</th>
                          <th scope="col">Category Description</th>
                          <th scope="col">Created By</th>
                          <th scope="col">Updated By</th>
                          <th scope="col">Inserted Date</th>
                          <th scope="col">Updated Date</th>
                          <th scope="col">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        {category
                          .filter((val) => {
                            if (searchTerm == "") {
                              return val;
                            } else if (
                              val.categoryName
                                .toLowerCase()
                                .includes(searchTerm.toLowerCase())
                            ) {
                              return val;
                            }
                          })
                          .map((data, index) => {
                            return (
                              <tr key={data.categoryId}>
                                <th scope="row">{index + 1}</th>
                                <td>{data.categoryName}</td>
                                <td>{data.description}</td>
                                <td>{data.createdBy.userName}</td>
                                <td>{data.updatedBy.userName}</td>
                                <td>
                                  {Moment(data.insertedDate).format(
                                    "DD-MM-YYYY"
                                  )}
                                </td>
                                <td>
                                  {Moment(data.updatedDate).format(
                                    "DD-MM-YYYY"
                                  )}
                                </td>
                                <td>
                                  <button
                                    type="button"
                                    className="btn btn-icon btn-sm"
                                    title="Edit"
                                    onClick={(e) =>
                                      UpdateData(data.categoryId, e)
                                    }
                                  >
                                    <i class="fas fa-edit text-primary"></i>
                                  </button>
                                  <button
                                    type="button"
                                    className="btn btn-icon btn-sm js-sweetalert"
                                    title="Delete"
                                    data-type="confirm"
                                    onClick={(e) =>
                                      handleRemove(data.categoryId, e)
                                    }
                                  >
                                    <i className="fas fa-trash-alt text-danger"></i>
                                  </button>
                                </td>
                              </tr>
                            );
                          })}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div>
                <ReactPaginate
                  previousLabel="Previous"
                  nextLabel="Next"
                  breakLabel={"..."}
                  pageCount={pageCount}
                  marginPagesDisplayed={2}
                  pageRangeDisplayed={2}
                  onPageChange={handlePageClick}
                  containerClassName={"pagination justify-content-center"}
                  pageClassName={"page-item"}
                  pageLinkClassName={"page-link"}
                  previousClassName={"page-item"}
                  previousLinkClassName={"page-link"}
                  nextClassName={"page-item"}
                  nextLinkClassName={"page-link"}
                  breakClassName={"page-item"}
                  breakLinkClassName={"page-link"}
                  activeClassName={"active"}
                />
              </div>
            </TabPane>
            <TabPane tabId={2} className={classnames(["fade show"])}>
              <div className="col-lg-12 col-md-12">
                <div class="card">
                  <div class="card-body">
                    <div className="card-header table-section">
                      {onEdit ? (
                        <h3 className="card-title">Update Category</h3>
                      ) : (
                        <h3 className="card-title">Add Category</h3>
                      )}
                    </div>
                    <Form style={{ marginbottom: 8 }}>
                      <Form.Group
                        className="mb-4 col-sm-4"
                        controlId="validationCustom01"
                      >
                        <Form.Label>Category Name</Form.Label>
                        <InputGroup>
                          <Form.Control
                            type="text"
                            value={categoryName}
                            placeholder="Enter Category Name"
                            //  onChange={(e) => setCategoryName(e.target.value)}
                            onChange={userHandler}
                            required
                          />
                          {categoryErr ? (
                            <span style={{ color: "red", marginLeft: 20 }}>
                              Category Name is required!!
                            </span>
                          ) : null}
                        </InputGroup>
                      </Form.Group>

                      <Form.Group
                        className="mb-3"
                        controlId="validationCustom02"
                      >
                        <Form.Label>Description</Form.Label>
                        <Form.Control
                          as="textarea"
                          rows={3}
                          type="text"
                          value={description}
                          placeholder="Enter Description"
                          // onChange={(e) => setDescription(e.target.value)}
                          onChange={DesHandler}
                          required
                        />
                        {desErr ? (
                          <span style={{ color: "red" }}>
                            Short Description is required!!
                          </span>
                        ) : null}
                      </Form.Group>
                    </Form>
                    <div className="col-sm-12">
                      {onEdit ? (
                        <button
                          onClick={(e) => handleSubmit(e.target.value, 1)}
                          style={{ marginRight: 20 }}
                          type="submit"
                          className="mr-1 btn btn-primary"
                        >
                          Update
                        </button>
                      ) : (
                        <button
                          onClick={(e) => handleSubmit(e.target.value, 0)}
                          // onClick={postData}
                          style={{ marginRight: 20 }}
                          type="submit"
                          className="mr-1 btn btn-primary"
                        >
                          Submit
                        </button>
                      )}

                      <button
                        type="submit"
                        className="btn btn-outline-secondary btn-default"
                      >
                        Cancel
                      </button>
                    </div>
                  </div>
                  <div></div>
                </div>
              </div>
            </TabPane>
          </TabContent>
        </div>
      </main>
    </>
  );
}

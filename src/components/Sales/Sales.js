import React, {useState} from "react";
import {
  NavLink,
  NavItem,
  Nav,
  DropdownButton,
  DropdownItem,
  Dropdown,
  Row,
  Col,
  FormGroup,
  Form,
  FormLabel,
  FormControl,
  InputGroup,
  Button,
} from "react-bootstrap";
import "react-phone-number-input/style.css";
import PhoneInput from "react-phone-number-input";
import { TabContent, TabPane } from "reactstrap";
import ReactPaginate from "react-paginate";


export default function Sales() {
  const [sales,SetSales] = useState([])
  return (
    <>
      <main id="main" class="main">
        <div className="pagetitle">
          <h1>Sales</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <a href="/">Home</a>
              </li>
              <li className="breadcrumb-item active">Sales </li>
            </ol>
          </nav>
        </div>
        <div className="col-lg-12 col-md-12">
          <Form noValidate>
            <Row className="mb-3">
              <Form.Group as={Col} md="4" controlId="validationCustom01">
                <Form.Label>Customer </Form.Label>
                <Form.Control required type="text" placeholder="Add Customer" />
              </Form.Group>
              <Form.Group as={Col} md="2" controlId="validationCustom02" style={{marginTop:22}}  >
                <Form.Label></Form.Label>
                <Button style={{backgroundColor:"info"}}>Get Customer</Button>
                
              </Form.Group>
              <Form.Group as={Col} md="3" controlId="validationCustomUsername">
                <Form.Label>Phone Number</Form.Label>
                <PhoneInput placeholder="Enter phone number" />
              </Form.Group>

              <Form.Group as={Col} md="3" controlId="formGridState">
                <Form.Label>Payment Mode</Form.Label>
                <Form.Select>
                  <option>Cash</option>
                  <option>Cash..</option>
                </Form.Select>
              </Form.Group>
            </Row>

            <Row className="mb-3">
              <Form.Group as={Col} md="4" controlId="validationCustom01">
                <Form.Label>Search Product By Name</Form.Label>
                <Form.Control
                  required
                  type="text"
                  className="form-control"
                  placeholder="Search"
                />
              </Form.Group>
              <Form.Group as={Col} md="4" controlId="validationCustom02">
                <Form.Label>Search Product By Code </Form.Label>
                <Form.Control
                  required
                  className="form-control"
                  placeholder="Search"
                />
              </Form.Group>
              <Form.Group as={Col} md="4" controlId="validationCustomUsername">
                <Form.Label>Search Product By Barcode</Form.Label>
                <InputGroup hasValidation>
                  <Form.Control
                    className="form-control"
                    placeholder="Search"
                    required
                  />
                </InputGroup>
              </Form.Group>
            </Row>
          </Form>
          <TabPane  >
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-body ">
                    <table class="table table-header bg-info text-black ">
                      <thead class="table-section">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Product Name</th>
                          <th scope="col">Qty</th>
                          <th scope="col">MRP</th>
                          <th scope="col">Discount</th>
                          <th scope="col">ProdTax/TaxAmount</th>
                          <th scope="col">Sub Total</th>
                          <th scope="col">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        {sales
                          .map((data, index) => {
                            return (
                              <tr key={data.sales}>
                                {/* <th scope="row">{index + 1}</th>
                                <td>{data.supplierName}</td>
                                <td>{data.mobileNumber}</td>
                                <td>{data.bankAcNo}</td>
                                <td>{data.gstNo}</td>
                                <td>{data.ifsc}</td>
                                <td>{data.description}</td>
                                <td>{data.address}</td>
                                <td>{data.createdBy.userName}</td>
                                <td>{data.updatedBy.userName}</td>
                                <td>
                                  {Moment(data.insertedDate).format(
                                    "DD-MM-YYYY"
                                  )}
                                </td>
                                <td>
                                  {Moment(data.updatedDate).format(
                                    "DD-MM-YYYY"
                                  )}
                                </td> */}
                                <td>
                                  
                                  <button
                                    type="button"
                                    className="btn btn-icon btn-sm js-sweetalert"
                                    title="Delete"
                                    data-type="confirm"
                                    // onClick={(e) =>
                                    //   handleRemove(data.supplierId, e)
                                    // }
                                  >
                                    <i className="fas fa-trash-alt text-danger"></i>
                                  </button>
                                </td>
                              </tr>
                            );
                          })}
                      </tbody>
                    </table>
                  </div>
                </div>
                <Row>

               
                </Row>
              </div>
            </TabPane>
        </div>
      </main>
    </>
  );
}

import React from "react";

export default function SalesReturn() {
  return (
    <>
      <main id="main" className="main d-flex float-left">
        <div className="pagetitle">
          <h1>Sales Return</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <a href="">Sales</a>
              </li>
              <li className="breadcrumb-item active">Sales Return</li>
            </ol>
          </nav>
        </div>
      </main>
    </>
  );
}

import React, { useState, useEffect } from "react";
import { TabContent, TabPane } from "reactstrap";
import classnames from "classnames";
import Moment from "moment";
import axios from "axios";
import ReactPaginate from "react-paginate";

export default function WaterBottleStock() {
  const [activeTab, setActiveTab] = useState(1);

  const [waterbottlestock, setWaterbottlestock] = useState([]);
  const [wbstockId, setWbstockId] = useState("");
  const [totalCurrentWaterBottles, setTotalCurrentWaterBottles] = useState("");
  const [totalManufactureWaterBottles, setTotalManufactureWaterBottles] =
    useState("");
  const [totalSaleAmount, setTotalSaleAmount] = useState("");

  const [product, setProduct] = useState([]);
  const [productId, setProductId] = useState("");

  const [pageSize, setPageSize] = useState(10);
  const [onEdit, setOnEdit] = useState(false);
  const [pageNumber, setPageNumber] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [value, setValue] = useState(0);
  const [searchTerm, setSearchTerm] = useState("");
  const [validated, setValidated] = useState(false);

  const BarStyling = {
    width: "20rem",
    background: "#F2F1F9",
    border: "#0d6efd",
    padding: "0.5rem",
    marginBottom: 10,
  };
  const user = JSON.parse(sessionStorage.getItem("user"));

  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };
  let userId = user.userId;

  const baseUrl = "https://executivetracking.cloudjiffy.net/WaterPlantWeb";

  const NewData = async () => {
    await axios({
      method: "get",
      url: `${baseUrl}/waterbottlestock/v1/getAllWaterBottleStockByPagination/{pageNumber}/{pageSize}?pageNumber=${pageNumber}&pageSize=${pageSize}`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setPageCount(data.totalPages);
        setWaterbottlestock(data.content);
        console.log("datttaaa", data.content);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => {
    NewData();
  }, [
    wbstockId,
    totalCurrentWaterBottles,
    totalManufactureWaterBottles,
    totalSaleAmount,
    productId,
    pageSize,
    pageNumber,
  ]);

  const handlePageClick = (data) => {
    setPageNumber(data.selected);
    NewData(pageNumber);
  };
  const handleChange = (e) => {
    setPageSize(e.target.value);
    setValue(e.target.value);
    NewData(pageSize);
  };

  return (
    <>
      <main id="main" class="main">
        <div class="pagetitle">
          <h1>Water Bottle Stock</h1>
          <nav>
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="/">Home</a>
              </li>
              <li class="breadcrumb-item active">Water Bottle Stock</li>
            </ol>
          </nav>
        </div>
        <div>
          <hr />
        </div>

        <div class="container-fluid">
          <div class="row justify-content-between">
            <div class="col-4">
              <select
                value={value}
                onChange={handleChange}
                style={{ border: "none", color: "bg-primary" }}
              >
                <option value="10">10 / Pages</option>
                <option value="25">25 / Pages</option>
                <option value="50">50 / Pages</option>
                <option value="100">100 / Pages</option>
              </select>
            </div>

            <div class="col-4">
              <div class="input-group">
                <input
                  style={BarStyling}
                  type="text"
                  placeholder="Search"
                  className="prompt"
                  onChange={(e) => setSearchTerm(e.target.value)}
                />
              </div>
            </div>
          </div>
        </div>

        <div className="container-fluid">
          <TabContent activeTab={activeTab}>
            <TabPane tabId={1} className={classnames(["fade show"])}>
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-body ">
                    <table class="table table-hover bg-gradient-info text-black">
                      <thead class="table-section">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">totalCurrentWaterBottles</th>
                          <th scope="col">totalManufactureWaterBottles</th>
                          <td scope="col">totalSaleAmount</td>
                          {/* <th className="text-center" scope="col">
                            Created By
                          </th>
                          <th className="text-center" scope="col">
                            Updated By
                          </th> */}
                          <th scope="col">Inserted Date</th>
                          <th scope="col">Updated Date</th>
                        </tr>
                      </thead>
                      <tbody>
                        {waterbottlestock.map((data, index) => {
                          return (
                            <tr key={data.wbstockId}>
                              <th scope="row">{index + 1}</th>
                              <td>{data.totalCurrentWaterBottles}</td>
                              <td>{data.totalManufactureWaterBottles}</td>
                              <td>{data.totalSaleAmount}</td>
                              {/* <td>{data.createdBy.userName}</td>
                                <td>{data.updatedBy.userName}</td> */}
                              <td>
                                {Moment(data.insertedDate).format("DD-MM-YYYY")}
                              </td>
                              <td>
                                {Moment(data.updatedDate).format("DD-MM-YYYY")}
                              </td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div>
                <ReactPaginate
                  previousLabel="Previous"
                  nextLabel="Next"
                  breakLabel={"..."}
                  pageCount={pageCount}
                  marginPagesDisplayed={2}
                  pageRangeDisplayed={2}
                  onPageChange={handlePageClick}
                  containerClassName={"pagination justify-content-center"}
                  pageClassName={"page-item"}
                  pageLinkClassName={"page-link"}
                  previousClassName={"page-item"}
                  previousLinkClassName={"page-link"}
                  nextClassName={"page-item"}
                  nextLinkClassName={"page-link"}
                  breakClassName={"page-item"}
                  breakLinkClassName={"page-link"}
                  activeClassName={"active"}
                />
              </div>
            </TabPane>
          </TabContent>
        </div>
      </main>
    </>
  );
}

import React from "react";

export default function Inventory() {
  return (
    <>
      <main id="main" className="main d-flex float-left">
        <div className="pagetitle">
          <h1>Inventory</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <a href="">Inventory</a>
              </li>
              <li className="breadcrumb-item active">Inventory</li>
            </ol>
          </nav>
        </div>
      </main>
    </>
  );
}

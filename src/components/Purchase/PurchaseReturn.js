import React from "react";

export default function PurchaseReturn() {
  return (
    <>
      <main id="main" className="main d-flex float-left">
        <div className="pagetitle">
          <h1>Purchase</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <a href="">Home</a>
              </li>
              <li className="breadcrumb-item active">Purchase Return</li>
            </ol>
          </nav>
        </div>
      </main>
    </>
  );
}

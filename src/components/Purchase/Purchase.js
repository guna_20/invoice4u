import React, { useState, useEffect } from "react";
import {
  NavLink,
  NavItem,
  Nav,
  DropdownButton,
  DropdownItem,
  Dropdown,
  Row,
  Col,
  FormGroup,
  Form,
  FormLabel,
  FormControl,
  InputGroup,
  Button,
} from "react-bootstrap";
import axios from "axios";
import "react-phone-number-input/style.css";
import PhoneInput from "react-phone-number-input";
import { TabContent, TabPane } from "reactstrap";
import classnames from "classnames";
import Moment from "moment";
import ReactPaginate from "react-paginate";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import SelectSearch from "react-select-search";
import { useRef } from "react";

export default function Sales() {
  const [activeTab, setActiveTab] = useState(1);
  // Purchase
  const [purchase, setPurchase] = useState([]);
  const [purchageId, setPurchageId] = useState("");
  const [purchaseDate, setPurchaseDate] = useState("");
  const [invoiceNo, setInvoiceNo] = useState("");
  const [supplierInvoiceNo, setSupplierInvoiceNo] = useState("");
  const [fileName, setFileName] = useState("");
  const [filePath, setFilePath] = useState("");
  // const [grandtotalAmount, setGrandtotalAmount] = useState(0);
  const [shippingAmount, setShippingAmount] = useState(0);
  const [totalAmount, setTotalAmount] = useState(0);
  // const [totalMrp, setTotalMrp] = useState(0);
  const [totalQuantity, setTotalQuantity] = useState("");
  const [totalSavedAmount, setTotalSavedAmount] = useState("");
  const [remark, setRemark] = useState("");
  let grandtotalAmount = 0;
  let totalMrp=0;

  // Material Supplier
  const [supplier, setSupplier] = useState([]);
  const [supplierId, setSupplierId] = useState("");
  const [supplierName, setSupplierName] = useState("");
  const [mobileNumber, setMobileNumber] = useState("");
  const [gstNo, setGstNo] = useState("");
  const [supplierValue, setSupplierValue] = useState("");

  // Raw Material
  const [rawMaterial, setRawMaterial] = useState([]);
  const [rawmaterialId, setRawmaterialId] = useState("");
  const [rawMaterialName, setRawMaterialName] = useState("");

  const [purchaseLog, setPurchaseLog] = useState([]);
  const [purchaseLogId, setPurchaseLogId] = useState("");
  const [cgst, setCgst] = useState("");
  const [gst, setGst] = useState("");
  const [sgst, setSgst] = useState("");
  const [discount, setDiscount] = useState("");
  const [productMrp, setProductMrp] = useState("");
  const [productPurchasedCost, setProductPurchasedCost] = useState("");
  const [quantity, setQuantity] = useState("");
  const [subTotalAmount, setSubTotalAmount] = useState("");
  const [barcode, setBarcode] = useState("");

  const [selectedPhoto, setSelectedPhoto] = useState(null);
  const [pageSize, setPageSize] = useState(10);
  const [pageNumber, setPageNumber] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [value, setValue] = useState(0);
  const [value1, setValue1] = useState(0);
  const [searchTerm, setSearchTerm] = useState("");

  const user = JSON.parse(sessionStorage.getItem("user"));
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };
  let userId = user.userId;

  const baseUrl = "https://executivetracking.cloudjiffy.net/WaterPlantWeb";

  const data = {
    createdBy: {
      userId,
    },
    fileName,
    filePath,
    grandtotalAmount,
    invoiceNo,
    materialSupplierDtoList: {
      gstNo,
      mobileNumber,
      supplierId,
      supplierName,
    },
    purchageId,
    purchaseDate,
    rawMaterialLogDtoList: [
      {
        cgst,
        discount,
        gst,
        productMrp,
        productPurchasedCost,
        purchaseLogId,
        quantity,
        rawMaterial: {
          rawMaterialName,
          rawmaterialId,
        },
        rawmaterialId,
        sgst,
        subTotalAmount,
      },
    ],
    remark,
    shippingAmount,
    supplierInvoiceNo,
    totalAmount,
    totalMrp,
    totalQuantity,
    totalSavedAmount,
  };

  const postData = (e) => {
    // e.preventDefault();
    // console.log("post---", data);
    axios({
      method: "post",
      url: `${baseUrl}/purchase/v1/createRawMaterialPurchase`,
      headers,
      data,
    })
      .then(function (res) {
        let data = res.data;
        // NewData.then((resp) => {
        NewData();
        //   console.log("PURCHASE", data)
        // });
      })
      .catch(function (error) {
        console.log(error);
      });
    setPurchaseDate("");
    setInvoiceNo("");
    setSupplierInvoiceNo("");
    setFileName("");
    // setGrandtotalAmount("");
    setShippingAmount("");
    setTotalAmount("");
    // setTotalMrp("");
    setTotalQuantity("");
    setTotalSavedAmount("");
    setRemark("");

    setSupplierName("");
    setSupplierId("");
    setMobileNumber("");
    setGstNo("");

    setRawmaterialId("");
    setRawMaterialName("");

    setCgst("");
    setGst("");
    setSgst("");
    setDiscount("");
    setProductMrp("");
    setProductPurchasedCost("");
    setQuantity("");
    setSubTotalAmount("");
    // setActiveTab(1);
  };

  const NewData = async () => {
    await axios({
      method: "get",
      url: `${baseUrl}/purchase/v1/getAllRawMaterialPurchasesByPagination/{pageNumber}/{pageSize}?pageNumber=${pageNumber}&pageSize=${pageSize}`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setPageCount(data.totalPages);
        setPurchase(data.content);
        console.log("DATAAA==", data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  // const UpdateData = (purchageId, e) => {
  //   axios({
  //     method: 'get',
  //     url: `${baseUrl}/purchase/v1/getRawMaterialPurchaseByPurchageId/{purchageId}?purchageId=${purchageId}`,
  //     headers,
  //     data,
  //   })
  //     .then(function (res) {
  //       let data = res.data;
  //       console.log('update', data);
  //       setGstNo(e.target.value);
  //       setMobileNumber(e.target.value);
  //     })
  //     .catch(function (err) {
  //       console.log(err);
  //     });
  // };

  const SupplierData = () => {
    axios({
      method: "get",
      url: `${baseUrl}/supplier/v1/getAllSuppliers`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setSupplier(data);
        console.log("supplier", data);
      })

      .catch(function (error) {
        console.log(error);
      });
  };

  const handleTypeChange = (e) => {
    setMobileNumber(e.target.value);

    let SupGst = supplier.filter((data) => {
      return data.mobileNumber === e.target.value;
    });

    if (SupGst[0] !== undefined) {
      console.log(SupGst[0]);
      setGstNo(SupGst[0].gstNo);
      setMobileNumber(SupGst[0].mobileNumber);
      setSupplierId(SupGst[0].supplierId);
    }
  };

  const handleTypeChangeByRawMaterial = (e) => {
    setRawmaterialId(e.target.value);
    setRawMaterialName(e.target.value);
  };
  // const handleTypeChangeByRawMaterial = (e) => {
  //   setBarcode(e.target.value);

  //   let RawBarcode = rawMaterial.filter((data)=> {
  //     return data.barcode === e.target.value;
  //   });
  //   if(RawBarcode[0] !== undefined) {

  //   }
  // }

  const RawMaterialData = () => {
    axios({
      method: "get",
      url: `${baseUrl}/rawmaterial/v1/getAllRawMaterials`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        setRawMaterial(data);
        console.log("raw===", data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const RawMaterialName = (RName) => {
    axios({
      method: "get",
      url: `${baseUrl}/rawmaterial/v1/getRawMaterialByRawMaterialName/{rawMaterialName}?rawMaterialName=${RName}`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data[0];
        setRawmaterialId(data.rawmaterialId);
        setRawMaterialName(data.rawMaterialName);
        setGst(data.gst);
        setCgst(data.cgst);
        setSgst(data.sgst);
        setDiscount(data.discount);
        setProductMrp(data.rawMaterialMRP);
        setProductPurchasedCost(data.rawMaterialCost);
        setQuantity(data.quantity);
        setBarcode(data.barcode);
        getTotalAmount();
       
        console.log("rawname==", data.rawMaterialMRP);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const getTotalAmount = () => {
    purchase.map((data) => {
      // grandtotalAmount = data.totalAmount + grandtotalAmount;
      totalMrp=data.productMrp + totalMrp;
      let totalAmount = data.rawMaterialLogDtoList[0].productMrp * data.rawMaterialLogDtoList[0].quantity;
      setSubTotalAmount(totalAmount);
      console.log("sub", data.rawMaterialLogDtoList[0].productMrp * data.rawMaterialLogDtoList[0].quantity);
      setProductMrp(totalMrp);
    });
  };
  
  // const total = () => {
  //   subTotalAmount = productMrp * quantity;
  //   setSubTotalAmount("");
  // }

  // const RawMaterialBarcode = () => {
  //   axios({
  //     method: "get",
  //     url:`${baseUrl}/rawmaterial/v1/getRawMaterialByBarcode/{barcode}?barcode=${barcode}`,
  //     headers,
  //     body:JSON.stringify(),
  //   })
  //   .then((res) => {
  //     let data = res.data;
  //     setRawMater
  //   })
  // }

  const searchInput = useRef();
  const options = [
    {
      type: "group",
      name: "Atlanta",
      items: [
        { name: "Workshop One", value: "1" },
        { name: "Workshop Two", value: "2" },
      ],
    },
    {
      type: "group",
      name: "Charleston",
      items: [
        { name: "Workshop Three", value: "3" },
        { name: "Workshop Four", value: "4" },
        { name: "Workshop Five", value: "5" },
      ],
    },
    {
      type: "group",
      name: "Inactive",
      items: [{ name: "Inactive Workshop", value: "100" }],
    },
  ];

  const handleOptionChange = (...args) => {
    searchInput.current.querySelector("input").value = "";
    console.log("ARGS:", args);
    console.log("CHANGE:");
  };

  const handleFilter = (items) => {
    return (searchValue) => {
      if (searchValue.length === 0) {
        return options;
      }
      const updatedItems = items.map((list) => {
        const newItems = list.items.filter((item) => {
          return item.name.toLowerCase().includes(searchValue.toLowerCase());
        });
        return { ...list, items: newItems };
      });
      return updatedItems;
    };
  };

  const handlePageClick = (data) => {
    setPageNumber(data.selected);
    NewData(pageNumber);
  };

  const handlePageChange = (e) => {
    setPageSize(e.target.value);
    setValue(e.target.value);
    NewData(pageSize);
  };

  useEffect(() => {
    // grandtotalAmount = 0;
    totalMrp=0;
    NewData();
    SupplierData();
    RawMaterialData();
    getTotalAmount();
  }, [
    fileName,
    filePath,
    grandtotalAmount,
    invoiceNo,
    gstNo,
    mobileNumber,
    supplierId,
    supplierName,
    purchageId,
    purchaseDate,
    cgst,
    discount,
    gst,
    productMrp,
    productPurchasedCost,
    purchaseLogId,
    quantity,
    rawMaterialName,
    rawmaterialId,
    rawmaterialId,
    sgst,
    subTotalAmount,
    remark,
    shippingAmount,
    supplierInvoiceNo,
    totalAmount,
    totalMrp,
    totalQuantity,
    totalSavedAmount,
    pageSize,
    pageNumber,
  ]);

  const onFileChange = (e) => {
    // setSelectedPhoto(e.target.files[0]);
    setSelectedPhoto(e.target.files[0]);
    if (!selectedPhoto) {
      // alert("image is required");
      return false;
    }
    if (!selectedPhoto.name.match(/\.(jpg|png)$/)) {
      alert("select valid image (jpg or png)");
      return false;
    }
  };

  const onFileUpload = (e) => {
    e.preventDefault();
    const data = new FormData();
    data.append("file", selectedPhoto);
    axios({
      method: "post",
      url: `https://executivetracking.cloudjiffy.net/WaterPlantWeb/file/uploadFile`,
      headers: {
        "content-type": "multipart/form-data",
        Authorization: "Bearer " + user.accessToken,
      },
      data,
    })
      .then(function (res) {
        console.log(res.data);
        setFileName(res.data.fileName);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <>
      <main id="main" class="main">
        <div className="pagetitle">
          <h1>Purchase</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <a href="/">Home</a>
              </li>
              <li className="breadcrumb-item active">Purchase</li>
            </ol>
          </nav>
        </div>
        <TabContent activeTab={activeTab}>
          <div className="col-lg-12 col-md-12">
            <Form noValidate>
              <Row className="mb-3">
                <Form.Group as={Col} md="4" controlId="validationCustom01">
                  <Form.Label>Supplier Invoice No. </Form.Label>
                  <Form.Control
                    required
                    type="number"
                    placeholder="Add Number"
                    value={supplierInvoiceNo}
                    onChange={(e) => setSupplierInvoiceNo(e.target.value)}
                  />
                </Form.Group>

                <Form.Group as={Col} md="3" controlId="validationCustom01">
                  <Form.Label>Purchase Date</Form.Label>
                  <InputGroup.Text id="inputGroupPrepend">
                    <i class="bi bi-calendar-check"></i>
                    <DatePicker
                      selected={purchaseDate}
                      placeholder="Choose Purchase Date"
                      onChange={(date) => setPurchaseDate(date)}
                    />
                  </InputGroup.Text>
                </Form.Group>

                <Form.Group as={Col} md="3" controlId="validationCustom03">
                  <Form.Label>File</Form.Label>
                  <Form.Control
                    type="file"
                    required
                    name="file"
                    className="form-control"
                    placeholder="Upload Photo"
                    onChange={(e) => onFileChange(e)}
                  />
                </Form.Group>

                <Form.Group
                  as={Col}
                  md="2"
                  controlId="validationCustom02"
                  style={{ marginTop: 25 }}
                >
                  <Form.Label></Form.Label>
                  <Button
                    style={{ backgroundColor: "info" }}
                    onClick={onFileUpload}
                  >
                    Upload
                  </Button>
                </Form.Group>
              </Row>

              <Row>
                <Form.Group as={Col} md="4" controlId="formGridState">
                  <Form.Label>Supplier</Form.Label>
                  <Form.Select
                    supplierValue={value}
                    onChange={handleTypeChange}
                  >
                    <option>Select a Supplier</option>
                    {supplier.map((data) => {
                      return (
                        <option value={data.mobileNumber}>
                          {data.supplierName}
                          {/* {data.gstNo}
                          {data.mobileNumber} */}
                        </option>
                      );
                    })}
                  </Form.Select>
                </Form.Group>

                <Form.Group as={Col} md="4" controlId="formGridState">
                  <Form.Label>GST Number</Form.Label>
                  {/* {supplier.map((data,index)=>{ */}
                  <Form.Control
                    className="cursor"
                    // onChange={handleTypeChange}
                    value={gstNo}
                  ></Form.Control>
                </Form.Group>

                <Form.Group as={Col} md="4" controlId="formGridState">
                  <Form.Label>Mobile Number</Form.Label>
                  <Form.Control
                    className="cursor"
                    // onChange={handleTypeChange}
                    value={mobileNumber}
                  ></Form.Control>
                </Form.Group>
              </Row>

              <Row className="mb-3">
                <Form.Group as={Col} md="4" controlId="validationCustom01">
                  <Form.Label>Search Raw Material By Name</Form.Label>
                  <Form.Select
                    supplierValue={value}
                    onChange={(e) => RawMaterialName(e.target.value)}
                  >
                    <option>Select Raw Material By Name</option>
                    {rawMaterial.map((data) => {
                      return (
                        <option value={data.rawMaterialName}>
                          {data.rawMaterialName}
                        </option>
                      );
                    })}
                  </Form.Select>
                </Form.Group>

                <Form.Group as={Col} md="4" controlId="formGridState">
                  <Form.Label>Search Raw Material By Barcode</Form.Label>
                  <Form.Control value={barcode}></Form.Control>
                </Form.Group>

                {/* <Form.Group as={Col} md="4" controlId="validationCustom01">
                  <Form.Label>Search Raw Material By Code</Form.Label>
                  <Form.Select
                    supplierValue={value}
                    onChange={handleTypeChangeByRawMaterial}
                  >
                    <option>Select Raw Material By Code</option>
                    {/* <option value={}>{barcode}</option>; 
                  </Form.Select>
                </Form.Group> */}

                {/* <Form.Group as={Col} md="4" controlId="validationCustom01">
                  <Form.Label>Search Raw Material By Name</Form.Label>
                  <SelectSearch
                    ref={searchInput}
                    options={options}
                    filterOptions={handleFilter}
                    value=""
                    name="Workshop"
                    placeholder="Choose a workshop"
                    search
                    onChange={handleOptionChange}
                  />
                </Form.Group> */}

                {/* <Form.Group as={Col} md="4" controlId="validationCustom02">
                  <Form.Label>Search Raw Material By Barcode </Form.Label>
                 <Form.Select
                    supplierValue={value}
                    onChange={handleTypeChangeByRawMaterial}
                  >
                    <option>Select Raw Material By Barcode</option>
                    {rawMaterial.map((data) => {
                      return (
                        <option value={data.rawmaterialId}>
                          {data.rawmaterial}
                        </option>
                      );
                    })}
                  </Form.Select>
                </Form.Group> */}
                {/* <Form.Group
                  as={Col}
                  md="4"
                  controlId="validationCustomUsername"
                >
                  <Form.Label>Search Raw Material By Code</Form.Label>
                  <InputGroup hasValidation>
                    <Form.Control
                      className="form-control"
                      placeholder="Search"
                      required
                    />
                  </InputGroup>
                </Form.Group> */}
              </Row>
            </Form>

            <div>
              <br />
            </div>
            <TabPane>
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-body ">
                    <table class="table table-header bg-info text-black ">
                      <thead class="table-section">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Product Name</th>
                          <th scope="col">Quantity</th>
                          <th scope="col">Product MRP</th>
                          <th scope="col">Product Purchased Cost</th>
                          <th scope="col">Discount</th>
                          <th scope="col">Gst</th>
                          <th scope="col">Cgst</th>
                          <th scope="col">Sgst</th>
                          <th scope="col">Sub Total Amount</th>
                        </tr>
                      </thead>
                      <tfoot style={{ backgroundColor: "white" }}>
                        {/* {purchase.map((data,index)=>{
                          return( */}
                        <tr>
                          <td></td>
                          <td>Shipping Amount: </td>
                          <td>Qty:</td>
                          <td>MRP:{totalMrp} </td>
                          <td>
                            Cost: {data.rawMaterialCost + productPurchasedCost}
                          </td>
                          <td>Discount:</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td>
                            Grand Total Amount:
                            {totalAmount}
                          </td>

                          <td>
                            <button
                              onClick={postData}
                              style={{ marginRight: 20 }}
                              type="submit"
                              className="mr-1 btn btn-primary"
                            >
                              Submit
                            </button>
                          </td>
                        </tr>
                        {/* )
                        })} */}
                      </tfoot>
                      <tbody>
                        {/* {rawMaterial.map((data, index) => {
                          return ( */}
                        {purchase.map((data, index) => {
                          // grandtotalAmount=data.totalAmount+grandtotalAmount;
                          return (
                            <tr
                              key={data.purchaseId}
                              style={{ backgroundColor: "white" }}
                            >
                              <th scope="row">{index + 1}</th>
                              <td scope="row">
                                {data.rawMaterialLogDtoList[0].rawMaterialDto.rawMaterialName}
                              </td>
                              <td>
                                {data.rawMaterialLogDtoList[0].quantity}
                              </td>
                              <td>
                                {data.rawMaterialLogDtoList[0].productMrp}
                                {/* {console.log("mrp",data.rawMaterialLogDtoList[0].productMrp)} */}
                              </td>
                              <td>
                                {data.rawMaterialLogDtoList[0].productPurchasedCost}
                              </td>
                              <td>{data.rawMaterialLogDtoList[0].discount}%</td>
                              <td>{data.rawMaterialLogDtoList[0].gst}%</td>
                              <td>{data.rawMaterialLogDtoList[0].cgst}%</td>
                              <td>{data.rawMaterialLogDtoList[0].sgst}%</td>
                              <td>{subTotalAmount}</td>
                              <td>
                                <button
                                  type="button"
                                  className="btn btn-icon btn-sm js-sweetalert"
                                  title="Delete"
                                  data-type="confirm"
                                  // onClick={(e) =>
                                  //   handleRemove(data.supplierId, e)
                                  // }
                                ></button>
                              </td>
                            </tr>
                          );
                        })}
                        <tr
                          key={data.rawmaterialId}
                          style={{ backgroundColor: "white" }}
                        >
                          <th scope="row"></th>
                          <td scope="row">{rawMaterialName}</td>
                          <td>
                            <input 
                            style={{width:50}}
                            required 
                            type="number" 
                            value={quantity}
                            onChange={(e)=>setQuantity(e.target.value)}/>
                          </td>
                          <td>{productMrp}</td>
                          <td>{productPurchasedCost}</td>
                          <td>{discount}</td>
                          <td>{gst}</td>
                          <td>{cgst}</td>
                          <td>{sgst}</td>
                          <td>{totalAmount}</td>
                          <td>
                            <button
                              type="button"
                              className="btn btn-icon btn-sm js-sweetalert"
                              title="Delete"
                              data-type="confirm"
                              // onClick={(e) =>
                              //   handleRemove(data.supplierId, e)
                              // }
                            >
                              {/* <i className="fas fa-trash-alt text-danger"></i> */}
                            </button>
                          </td>
                        </tr>
                        {/* );
                       })}  */}
                      </tbody>
                    </table>
                    <div>
                      <ReactPaginate
                        previousLabel="Previous"
                        nextLabel="Next"
                        breakLabel={"..."}
                        pageCount={pageCount}
                        marginPagesDisplayed={2}
                        pageRangeDisplayed={2}
                        onPageChange={handlePageClick}
                        containerClassName={"pagination justify-content-center"}
                        pageClassName={"page-item"}
                        pageLinkClassName={"page-link"}
                        previousClassName={"page-item"}
                        previousLinkClassName={"page-link"}
                        nextClassName={"page-item"}
                        nextLinkClassName={"page-link"}
                        breakClassName={"page-item"}
                        breakLinkClassName={"page-link"}
                        activeClassName={"active"}
                      />
                    </div>
                    {/* <tfoot style={{ backgroundColor: 'white' }}>
                      {rawMaterial.map((data, index) => {
                        return (
                          <tr>
                            {console.log('++', totalMrp)}
                            <td></td> 
                            <td>{data.shippingAmount}</td>
                            <td>{data.totalQuantity}</td>
                            <td>{data.rawMaterialMRP + totalMrp}</td>
                            <td>{data.totalAmount}</td>
                            <td>{data.discount}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>{data.grandtotalAmount}</td>
                          </tr>
                        );
                      })}
                    </tfoot> */}
                  </div>
                </div>
              </div>
            </TabPane>
          </div>
        </TabContent>
      </main>
    </>
  );
}

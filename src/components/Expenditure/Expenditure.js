import React, { useState, useEffect } from "react";
import {
  NavLink,
  NavItem,
  Nav,
  Row,
  Col,
  Form,
  InputGroup,
  Button,
} from "react-bootstrap";
import { TabContent, TabPane } from "reactstrap";
import classnames from "classnames";
import Moment from "moment";
import axios from "axios";
import ReactPaginate from "react-paginate";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

export default function EmploymentManagement() {
  const [activeTab, setActiveTab] = useState(1);

  const [expenditure, setExpenditure] = useState([]);
  const [expenditureId, setExpenditureId] = useState("");
  const [expenditureDate, setExpenditureDate] = useState("");
  const [expenditureHeader, setExpenditureHeader] = useState("");
  const [description, setDescription] = useState("");
  const [expenditureAmount, setExpenditureAmount] = useState("");
  const [fileName, setFileName] = useState("");
  const [filePath, setFilePath] = useState("");

  const [selectedPhoto, setSelectedPhoto] = useState(null);
  const [pageSize, setPageSize] = useState(10);
  const [onEdit, setOnEdit] = useState(false);
  const [pageNumber, setPageNumber] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [value, setValue] = useState(0);
  const [searchTerm, setSearchTerm] = useState("");

  const [expenditureErr, setExpenditureErr] = useState(false);
  const [descErr, setDescErr] = useState(false);

  function firstHandler(e) {
    let item = e.target.value;
    if (item.length <= 0) {
      setExpenditureErr(true);
    } else {
      setExpenditureErr(false);
    }
    setExpenditureHeader(item);
    // setLastName(item);

    console.log(e.target.value);
  }

  function descHandler(e) {
    let item = e.target.value;
    if (item.length > 30 || item.length <= 0) {
      // console.log(setDesErr)
      setDescErr(true);
    } else {
      setDescErr(false);
    }
    setDescription(item);
    // setAddress(item);

    console.log(e.target.value);
  }
  const handleSubmit = (e, val) => {
    let first = expenditureHeader.trim();
    let desc = description.trim();

    if (first.length > 0) {
      setExpenditureErr(false);
      if (desc.length > 0) {
        setExpenditureErr(false);
        setDescErr(false);
        if (val == 0) {
          postData(e);
        }
      } else {
        setDescErr(true);
      }
    } else {
      setExpenditureErr(true);
    }
  };

  const BarStyling = {
    width: "20rem",
    background: "#F2F1F9",
    border: "#0d6efd",
    padding: "0.5rem",
    marginBottom: 10,
  };
  const user = JSON.parse(sessionStorage.getItem("user"));
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };
  let userId = user.userId;
  const baseUrl = "https://executivetracking.cloudjiffy.net/WaterPlantWeb";
  const ImageUrl =
    "https://executivetracking.cloudjiffy.net/WaterPlantWeb/file/downloadFile/?filePath=";

  const FileUrl =
    "https://executivetracking.cloudjiffy.net/WaterPlantWeb/file/downloadFile/?filePath=";

  const postData = () => {
    axios({
      method: "post",
      url: `${baseUrl}/expenditure/v1/createExpenditure`,
      headers,
      data: {
        createdBy: {
          userId,
        },
        description,
        expenditureAmount,
        expenditureDate,
        expenditureHeader,
        expenditureId,
        fileName,
      },
    })
      .then(function (res) {
        if (res.data.responseCode === 201) {
          alert(res.data.message);
          window.location.reload(false);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    setExpenditureHeader("");
    setExpenditureAmount("");
    setExpenditureDate("");
    setDescription("");
    setFileName("");
    setExpenditureId("");
    setActiveTab(1);
    NewData();
  };

  const NewData = async () => {
    await axios({
      method: "get",
      url: `${baseUrl}/expenditure/v1/getAllExpenditureByPagination/{pageNumber}/{pageSize}?pageNumber=${pageNumber}&pageSize=${pageSize}`,
      headers,
      body: JSON.stringify(),
    })
      .then((res) => {
        let data = res.data;
        console.log(data);
        setPageCount(data.totalPages);
        setExpenditure(data.content);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const handlePageClick = (data) => {
    setPageNumber(data.selected);
    NewData(pageNumber);
  };
  const handleChange = (e) => {
    setPageSize(e.target.value);
    setValue(e.target.value);
    NewData(pageSize);
  };

  useEffect(() => {
    NewData();
  }, [
    description,
    expenditureAmount,
    expenditureDate,
    expenditureHeader,
    expenditureId,
    fileName,
    filePath,
    pageSize,
    pageNumber,
  ]);

  const UpdateData = (expenditureId) => {
    axios({
      method: "get",
      url: `${baseUrl}/expenditure/v1/getExpenditureByExpenditureId/{expenditureId}?expenditureId=${expenditureId}`,
      headers,
      data: {
        createdBy: {
          userId,
        },
        description,
        expenditureAmount,
        expenditureDate,
        expenditureHeader,
        expenditureId,
        fileName,
      },
    })
      .then(function (res) {
        let data = res.data;
        if (data.responseCode === 201) {
          alert(data.message);
          window.location.reload(false);
        } else if (data.responseCode === 400) {
          alert(data.errorMessage);
        }
        setExpenditureHeader(res.data.expenditureHeader);
        setExpenditureAmount(res.data.expenditureAmount);
        setExpenditureDate(res.data.expenditureDate);
        setDescription(res.data.description);
        setExpenditureId(expenditureId);
        setFileName(res.data.fileName);
        setOnEdit(true);
        setActiveTab(2);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const onFileChange = (e) => {
    setSelectedPhoto(e.target.files[0]);
    if (!selectedPhoto) {
      // alert("image is required");
      return false;
    }
    if (!selectedPhoto.name.match(/\.(jpg|png)$/)) {
      alert("select valid image (jpg or png)");
      return false;
    }
  };

  const onFileUpload = (e) => {
    e.preventDefault();
    const data = new FormData();
    data.append("file", selectedPhoto);

    axios({
      method: "post",
      url: `https://executivetracking.cloudjiffy.net/WaterPlantWeb/file/uploadFile`,
      headers: {
        "content-type": "multipart/form-data",
        Authorization: "Bearer " + user.accessToken,
      },
      data,
    })
      .then(function (res) {
        setFileName(res.data.fileName);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <>
      <main id="main" class="main">
        <div class="pagetitle">
          <h1>Expenditure</h1>
          <nav>
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="/">Home</a>
              </li>
              <li class="breadcrumb-item active">Expenditure</li>
            </ol>
          </nav>
        </div>
        <div>
          <hr />
        </div>

        <div className="container-fluid">
          <div className="d-flex justify-content-between align-items-center ">
            <Nav
              tabs
              className="page-header-tab"
              variant="pills"
              style={{ marginBottom: 30 }}
            >
              <Nav.Item>
                <Nav.Link
                  className={classnames({ active: activeTab === 1 })}
                  onClick={() => setActiveTab(1)}
                >
                  List View
                </Nav.Link>
              </Nav.Item>
              <NavItem>
                {onEdit ? (
                  <NavLink
                    className={classnames({ active: activeTab === 2 })}
                    onClick={() => setActiveTab(2)}
                  >
                    Edit
                  </NavLink>
                ) : (
                  <NavLink
                    className={classnames({ active: activeTab === 2 })}
                    onClick={() => setActiveTab(2)}
                  >
                    Add
                  </NavLink>
                )}
              </NavItem>
            </Nav>
          </div>
        </div>

        <div class="container-fluid">
          <div class="row justify-content-between">
            <div class="col-4">
              <select
                value={value}
                onChange={handleChange}
                style={{ border: "none", color: "bg-primary" }}
              >
                <option value="10">10 / Pages</option>
                <option value="25">25 / Pages</option>
                <option value="50">50 / Pages</option>
                <option value="100">100 / Pages</option>
              </select>
            </div>
            <div class="col-4">
              <div class="input-group">
                <input
                  style={BarStyling}
                  type="text"
                  placeholder="Search"
                  className="prompt"
                  onChange={(e) => setSearchTerm(e.target.value)}
                />
              </div>
            </div>
          </div>
        </div>

        <div className="container-fluid">
          <TabContent activeTab={activeTab}>
            <TabPane tabId={1} className={classnames(["fade show"])}>
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-body ">
                    <table class="table table-hover bg-gradient-info text-black">
                      <thead class="table-section">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Expenditure Header</th>
                          <th scope="col">Expenditure Amount</th>
                          <th scope="col">Expenditure Date</th>
                          <th scope="col">Description</th>
                          <th scope="col">Image</th>
                          <th scope="col">File</th>
                          {/* <th className="text-center" scope="col">
                            Created By
                          </th>
                          <th className="text-center" scope="col">
                            Updated By
                          </th> */}
                          <th scope="col">Inserted Date</th>
                          <th scope="col">Updated Date</th>
                          {/* <th scope="col">Actions</th> */}
                        </tr>
                      </thead>
                      <tbody>
                        {expenditure
                          .filter((val) => {
                            if (searchTerm == "") {
                              return val;
                            } else if (
                              val.expenditureHeader
                                .toLowerCase()
                                .includes(searchTerm.toLowerCase())
                            ) {
                              return val;
                            }
                          })
                          .map((data, index) => {
                            return (
                              <tr key={data.expenditureId}>
                                <th scope="row">{index + 1}</th>
                                <td>{data.expenditureHeader}</td>
                                <td>{data.expenditureAmount}</td>
                                <td>
                                  {Moment(data.expenditureDate).format(
                                    "DD-MM-YYYY"
                                  )}
                                </td>
                                <td>{data.description}</td>
                                <td>
                                  <img
                                    src={ImageUrl + data.filePath}
                                    alt={data.fileName}
                                    style={{ width: 50, height: 50 }}
                                  />
                                </td>
                                <td>
                                  <h6
                                    onClick={(e) =>
                                      window.open(FileUrl + data.filePath)
                                    }
                                  >
                                    <i class="fa-solid fa-file-pdf"></i>{" "}
                                    {data.fileName}
                                  </h6>
                                </td>
                                {/* <td>{data.createdBy.userName}</td>
                                <td>{data.updatedBy.userName}</td> */}
                                <td>
                                  {Moment(data.insertedDate).format(
                                    "DD-MM-YYYY"
                                  )}
                                </td>
                                <td>
                                  {Moment(data.updatedDate).format(
                                    "DD-MM-YYYY"
                                  )}
                                </td>
                              </tr>
                            );
                          })}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div>
                <ReactPaginate
                  previousLabel="Previous"
                  nextLabel="Next"
                  breakLabel={"..."}
                  pageCount={pageCount}
                  marginPagesDisplayed={2}
                  pageRangeDisplayed={2}
                  onPageChange={handlePageClick}
                  containerClassName={"pagination justify-content-center"}
                  pageClassName={"page-item"}
                  pageLinkClassName={"page-link"}
                  previousClassName={"page-item"}
                  previousLinkClassName={"page-link"}
                  nextClassName={"page-item"}
                  nextLinkClassName={"page-link"}
                  breakClassName={"page-item"}
                  breakLinkClassName={"page-link"}
                  activeClassName={"active"}
                />
              </div>
            </TabPane>
            <TabPane tabId={2} className={classnames(["fade show"])}>
              <div className="col-lg-12 col-md-12">
                <div class="card">
                  <div class="card-body">
                    <div className="card-header table-section">
                      {onEdit ? (
                        <h3 className="card-title">Update Expenditure</h3>
                      ) : (
                        <h3 className="card-title">Add Expenditure</h3>
                      )}
                    </div>
                    <Form noValidate>
                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="6"
                          controlId="validationCustom01"
                        >
                          <Form.Label>Expenditure Header</Form.Label>
                          <Form.Control
                            required
                            type="text"
                            value={expenditureHeader}
                            className="form-control"
                            placeholder="Enter Expenditure Header"
                            // onChange={(e) => setExpenditureHeader(e.target.value)}
                            onChange={firstHandler}
                          />
                          {expenditureErr ? (
                            <span style={{ color: "red", marginLeft: 20 }}>
                              Expenditure Header is required!!
                            </span>
                          ) : null}
                        </Form.Group>

                        <Form.Group
                          as={Col}
                          md="6"
                          controlId="validationCustom01"
                        >
                          <Form.Label>Expenditure Amount</Form.Label>
                          <Form.Control
                            required
                            type="text"
                            value={expenditureAmount}
                            className="form-control"
                            placeholder="Enter Expenditure Amount"
                            onChange={(e) =>
                              setExpenditureAmount(e.target.value)
                            }
                            // onChange={firstHandler}
                          />
                        </Form.Group>
                      </Row>
                      <Row>
                        <Form.Group
                          as={Col}
                          md="4"
                          controlId="validationCustom01"
                        >
                          <Form.Label>Expenditure Date</Form.Label>
                          <InputGroup.Text id="inputGroupPrepend">
                            <i class="bi bi-calendar-check"></i>
                            <DatePicker
                              selected={expenditureDate}
                              placeholder="Enter Expenditure Date"
                              onChange={(date) => setExpenditureDate(date)}
                            />
                          </InputGroup.Text>
                        </Form.Group>
                      </Row>

                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="12"
                          controlId="validationCustom03"
                        >
                          <Form.Label>Description</Form.Label>
                          <Form.Control
                            as="textarea"
                            rows={3}
                            value={description}
                            className="form-control no-resize"
                            placeholder="Add description"
                            // onChange={(e) => setDescription(e.target.value)}
                            onChange={descHandler}
                          />
                          {descErr ? (
                            <span style={{ color: "red", marginLeft: 20 }}>
                              Description is required!!
                            </span>
                          ) : null}
                        </Form.Group>
                      </Row>

                      <Row className="mb-3">
                        <Form.Group
                          as={Col}
                          md="6"
                          controlId="validationCustom03"
                        >
                          <Form.Label>File</Form.Label>
                          <Form.Control
                            type="file"
                            required
                            name="file"
                            className="form-control"
                            placeholder="Upload Photo"
                            onChange={(e) => onFileChange(e)}
                          />
                        </Form.Group>
                        <div className="col-sm-6">
                          <Button
                            style={{ marginbottom: 20 }}
                            className="mr-1 p-2 btn btn-primary"
                            onClick={onFileUpload}
                          >
                            Upload
                          </Button>
                        </div>
                      </Row>
                    </Form>
                    <div className="col-sm-12">
                      <button
                        onClick={(e) => handleSubmit(e.target.value, 0)}
                        // onClick={postData}
                        style={{ marginRight: 20 }}
                        type="submit"
                        className="mr-1 btn btn-primary"
                      >
                        Submit
                      </button>

                      <button
                        type="submit"
                        className="btn btn-outline-secondary btn-default"
                      >
                        Cancel
                      </button>
                    </div>
                  </div>
                </div>
                <div></div>
              </div>
            </TabPane>
          </TabContent>
        </div>
      </main>
    </>
  );
}

import React from "react";

export default function Dashboard() {
  return (
    <>
      <main id="main" class="main">
        <div class="pagetitle">
          <h1>Dashboard</h1>
          <nav>
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="/">Home</a>
              </li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </nav>
        </div>

        <div class="wrapper">
          <section class="columns section dashboard">
            <div class="column">
              <div class="card info-card sales-card">
                <div class="card-body">
                  <h5 class="card-title">Category</h5>

                  <div class="d-flex align-items-center">
                    <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                      <i class="bi bi-cart"></i>
                    </div>
                    <div class="ps-3">
                      <h6>14</h6>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="column">
              <div class="card info-card revenue-card">
                <div class="card-body">
                  <h5 class="card-title">Product</h5>

                  <div class="d-flex align-items-center">
                    <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                      <i class="fab fa-product-hunt"></i>
                    </div>
                    <div class="ps-3">
                      <h6>6</h6>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="column">
              <div class="card info-card sales-card">
                <div class="card-body">
                  <h5 class="card-title">Raw Material</h5>

                  <div class="d-flex align-items-center">
                    <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                      <i class="fas fa-industry"></i>
                    </div>
                    <div class="ps-3">
                      <h6>7</h6>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="column">
              <div class="card info-card revenue-card">
                <div class="card-body">
                  <h5 class="card-title">User Profile</h5>

                  <div class="d-flex align-items-center">
                    <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                      <i class="fas fa-users"></i>
                    </div>
                    <div class="ps-3">
                      <h6>2</h6>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </main>
    </>
  );
}
